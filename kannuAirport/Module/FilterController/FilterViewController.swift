//
//  FilterViewController.swift
//  kannuAirport
//
//  Created by Arpana on 25/05/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit

class FilterViewController: UIViewController {

    @IBOutlet weak var lblTypeOfFlight: UILabel!
   
//    @IBOutlet weak var switchTypeOfFlight: UISegmentedControl!
//
//    @IBOutlet weak var lblFlightNo: UILabel!
//
//    @IBOutlet weak var txtFieldFlightNo: UITextField!
//
//    @IBOutlet weak var lblFrom: UILabel!
//
//    @IBOutlet weak var txtFieldFrom: UITextField!
//
//    @IBOutlet weak var pickerView: UIPickerView!
    
    @IBOutlet weak var customNavigationBarForDrawer: CustomNavigationBarForDrawer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
         kAppDelegate.filterText = ""
        
        // Do any additional setup after loading the view.
    }
//    /UIColor(red: 1, green: 0, blue: 0, alpha: 1)
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
//        txtFieldFrom.rightViewMode = .always
//        txtFieldFrom.AddImage(direction: .Right, imageName:"dropdown" , Frame:   CGRect(origin: CGPoint(x: 0,y :0 ), size: CGSize(width: 30, height: 30)), backgroundColor: UIColor.clear)
//
//        switchTypeOfFlight.setTitleTextAttributes([
//            NSAttributedStringKey.font :  UIFont(name: "Karla-Regular", size: 16.0)!,
//            NSAttributedStringKey.foregroundColor: UIColor.black
//            ], for: .normal)
//
//        switchTypeOfFlight.setTitleTextAttributes([
//            NSAttributedStringKey.font :  UIFont(name: "Karla-Regular", size: 16.0)!,
//            NSAttributedStringKey.foregroundColor: UIColor.white
//            ], for: .selected)
//
        customNavigationBarForDrawer.titleLabel.text = "Fillters"
        customNavigationBarForDrawer.senderController = self
        customNavigationBarForDrawer.leftSideMenuButtonItem.tag = ButtonTag.BACK.rawValue
        customNavigationBarForDrawer.leftSideMenuButtonItem.setImage(#imageLiteral(resourceName: "back"), for: .normal)

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        if let touch = touches.first {
//            let position = touch.location(in: view)
//            print(position)
////        }
//        txtFieldFrom.resignFirstResponder()
//        self.pickerView.isHidden = true
    }
    
    deinit {
        print("\(FilterViewController.self) is released ")
        
    }
    
    @IBAction func applyFilter(_ sender : Any){
        
        switch (sender as? UIButton)?.tag
            
        {
        case 0:
            kAppDelegate.filterText = "Domestic"
        case 1:
              kAppDelegate.filterText = "International"
        case 2:
            kAppDelegate.filterText = "Both"

        default: print("wrong button tapped"
        )
    }
        self.navigationController?.popViewController(animated: true)

}
}
/*
extension FilterViewController: UITextFieldDelegate {
    
     func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        return true
    }
    
     func textFieldDidBeginEditing(_ textField: UITextField){
        
        if(textField == txtFieldFrom){
            pickerView.isHidden = false
        }
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if(textField == txtFieldFrom){
            
            pickerView.isHidden = true
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtFieldFrom{
            return false
        }
        return true
    }
 
    
}
extension FilterViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
      
    }
    
        
    }

*/
