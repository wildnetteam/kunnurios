//
//  InformationAsTabularViewController.swift
//  kannuAirport
//
//  Created by Arpana on 25/09/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit
import SwiftLoader
import SkeletonView
private let heightForHeader  :CGFloat  = 110.0

class InformationAsTabularViewController: UIViewController {

    @IBOutlet weak var customNavigationBarForDrawer: CustomNavigationBarForDrawer!

    var nodeIDToGetDetails :String?
    var nodeTitle :String?
    var isFromSideController: Bool = true
    var isTypeContentView: Bool = false

        var tableTimer : Timer!
        var inc : Int!
        private let refreshControl = UIRefreshControl()
        
        @IBOutlet weak var shadowView: UIView!
        @IBOutlet weak var tableView: UITableView!
        
        var newsArray : News?
        var ArrMutable : NSMutableArray?
        var nodeIdToFetchDetails :String?
        var isNewsToDisplay = true // This variable is maintained to keep tract of different view to be shown
        //if true consider news api, if false consider notification api
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
        override func viewDidLoad() {
            
            super.viewDidLoad()
            ArrMutable = NSMutableArray()
            setUPUI()
            inc = 0
            
            newsArray?.removeAll()
            ArrMutable?.removeAllObjects()
           
            
        }
    
        func setUPUI(){
            
            customNavigationBarForDrawer.senderController = self
            customNavigationBarForDrawer.leftSideMenuButtonItem.tag = isFromSideController == true ? ButtonTag.MENU.rawValue :ButtonTag.BACK.rawValue
            customNavigationBarForDrawer.leftSideMenuButtonItem.setImage (isFromSideController == true ? #imageLiteral(resourceName: "3bar") : #imageLiteral(resourceName: "back"), for: .normal)
            
            self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
            shadowView.layer.masksToBounds = false
            shadowView.layer.shadowOpacity = 0.4 ;
            shadowView.layer.shadowColor = UIColor.black.cgColor
            shadowView.layer.shadowOffset = CGSize(width: 0.0, height: 4.0)
            tableView.refreshControl = refreshControl
            // Configure Refresh Control
            refreshControl.addTarget(self, action: #selector(refreshNotificationData(_:)), for: .valueChanged)
        }
        
        @objc private func refreshNotificationData(_ sender: Any) {
            // Fetch Weather Data
            getNotificationDetails()
        }
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
            customNavigationBarForDrawer.titleLabel.text = nodeTitle
            newsArray?.removeAll()
            tableView.reloadData()
            getNotificationDetails()
            
        }
        
        
        @objc func performTableUpdates(_ timer : Timer){
            
            let ip : IndexPath = IndexPath(row: inc, section: 0)
            
            guard let tempNewsArray = newsArray else {
                
                return
            }
            if(tempNewsArray.count == 0){
                return
            }
            ArrMutable?.add(tempNewsArray[inc])
            tableView.beginUpdates()
            
            tableView.insertRows(at: [ip], with: UITableViewRowAnimation.fade)
            
            tableView.endUpdates()
            
            if(inc == ((tempNewsArray.count) - 1)){
                tableTimer.invalidate()
            }
            else{
                inc = inc + 1
            }
        }
        
    
        func getNotificationDetails()  {
            
            // SwiftLoader.show(animated: true)
            if tableView != nil {
                self.tableView.showAnimatedSkeleton()
            }
            APICall .callTabularInformationAPI([ : ], header: ["Authorization" : AuthorizationToken ], URLPath: nodeIDToGetDetails!) { (sucess, result, error) in
                
                DispatchQueue.main.async {
                    
                    weak var weakSelf = self
                    
                    SwiftLoader.hide()
                    
                    if (error != nil){
                        
                        self.showAlertWithAction(with: "", message: error?.localizedDescription)
                        
                    }
                    if sucess {
                        
                        do {
                            let jsonDecoder = JSONDecoder()
                            
                            weakSelf?.newsArray = try  jsonDecoder.decode(News.self, from: result!) as News
                            weakSelf?.tableView.reloadData()
                            
                        } catch  {
                            
                            weakSelf?.showAlertWithAction(with: "", message: AlertMessages.MODELERRORMESSAGE)
                            
                        }
                    }
                        
                    else{
                        //Alert not sucessfull
                        weakSelf?.showAlertWithAction(with: "Failed", message: "Some error occured")
                        
                    }
                    if (weakSelf?.refreshControl.isRefreshing)!  {
                        
                        weakSelf?.refreshControl.endRefreshing()
                    }
                    if weakSelf?.tableView != nil {
                        weakSelf?.tableView.hideSkeleton()
                    }
                }
            }
            
        }
        deinit {
            print(" \(String(describing: newsArray)) is NewsArray deallocated")
        }
        
    }
    
    extension InformationAsTabularViewController : UITableViewDelegate, UITableViewDataSource , SkeletonTableViewDataSource, UIViewControllerTransitioningDelegate{
        func collectionSkeletonView(_ skeletonView: UITableView, cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
            return "NewsOrTenderCellTableViewCellID"
        }
        
        
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section:
            Int) -> Int
        {
            if let array = newsArray {
                
                return array.count
                
            }
            return 0
        }
        
        
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
        {
            // Allocates a Table View Cell
            let cell =
                tableView.dequeueReusableCell(withIdentifier: "NewsOrTenderCellTableViewCellID",
                                              for: indexPath) as! NewsOrTenderCellTableViewCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            
            let layer = UIView(frame: CGRect(x: 0, y: heightForHeader - 3, width: cell.contentView.frame.width, height: 1))
            
            layer.layer.borderWidth = 1
            layer.layer.borderColor = UIColor(red:0.89, green:0.92, blue:0.93, alpha:1).cgColor
            cell.contentView.addSubview(layer)
            
            if let newsObj  = self.newsArray?[indexPath.row] {
                
                cell.lblTitle.text = newsObj.title
                cell.lblDate.text =  newsObj.created //newsObj.dateFromString(dt: newsObj.created)
                
            }
            cell.accessoryType = .disclosureIndicator
            return cell
        }
        
        func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath){
            let delay : CGFloat =     1.0
            UIView.beginAnimations("transalation", context: nil)
            UIView.animate(withDuration: 5.0, delay:TimeInterval(delay), options: UIViewAnimationOptions.repeat, animations: {}, completion: nil)
            UIView.commitAnimations()
            
        }
        
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            
            return heightForHeader
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
        {
            
            if let newsObj  = self.newsArray?[indexPath.row] {
                
                if isTypeContentView == true {
                    
                    //implement node/uri concept
                  NavigationManager.moveToInformationViewControllerIfNotExists(kAppDelegate.menuNavController! ,node: .NOTAPPLIED ,nodeIden:newsObj.nid!, nodeName: newsObj.title! , isSideMenuIconToAppear: false)
                    
                }else {
                    if newsObj.fieldUploadPDF != "" , newsObj.fieldUploadPDF != nil {
                        //  if newsObj.uri != "" , newsObj.uri != nil{
                        //Open PDF Viewer
                        NavigationManager.moveToPDFViewControllerIfNotExists(kAppDelegate.menuNavController! ,node: .NOTAPPLIED ,pdfURL:newsObj.fieldUploadPDF!, nodeName: newsObj.title! )
                        
                    }else{
                        
                        if let urlToVisit =  newsObj.viewNode {
                            
                            var  nodeUriID : Array? = (newsObj.nid!.components(separatedBy: "/"))
                            
                            if (nodeUriID?.count)! > 1{
                                
                                if let number : String = nodeUriID![1] {
                                    
                                    NavigationManager.moveToInformationViewControllerIfNotExists(kAppDelegate.menuNavController!, node: .NOTAPPLIED, nodeIden: number, nodeName: nodeTitle!, isSideMenuIconToAppear: false)
                                }else{
                                    
                                }
                            }else{
                                NavigationManager.moveToWebInformationDisplayViewControllerIfNotExists(kAppDelegate.menuNavController!, webURL: urlToVisit)
                            }
                        }
                    }
                }
            }
        }
        

    

}
