//
//  PDFViewController.swift
//  kannuAirport
//
//  Created by Arpana on 26/06/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit
import PDFKit
import SwiftLoader
@objcMembers

class PDFViewController: UIViewController {

        var pdfView: PDFView!
        @IBOutlet weak var pdfBaseView: UIView!
        var PDFUrl:String?
        var nodeTitle :String?

        @IBOutlet weak var customNavigationBarForDrawer: CustomNavigationBarForDrawer!

        override func viewDidLoad() {
            
            super.viewDidLoad()
            
            setUPUI()
 
            // load our example PDF and make it display immediately
//            if let url = Bundle.main.url(forResource: "File", withExtension: "pdf"){
//                pdfView.document = PDFDocument(url: url)
//            }
            
          
//            let pdfDoc =  PDFDocument.init(url: URL(string:"http://111.93.53.59/kialnew/sites/default/files/2018-06/UniformNIQ.pdf")!)
//
//                  pdfView.document =  pdfDoc
        }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        SwiftLoader.show(animated: true)
        DispatchQueue.main.async {
            weak var weakSelf = self
            weakSelf?.pdfView = PDFView()
             weakSelf?.pdfView.translatesAutoresizingMaskIntoConstraints = false
           //  weakSelf?.pdfView.backgroundColor = UIColor.lightGray
            weakSelf?.pdfBaseView.addSubview( (weakSelf?.pdfView)!)
            // make it take up the full screen
            weakSelf?.pdfView.leadingAnchor.constraint(equalTo:  (weakSelf?.pdfBaseView.leadingAnchor)!).isActive = true
            weakSelf?.pdfView.trailingAnchor.constraint(equalTo:  (weakSelf?.pdfBaseView.trailingAnchor)!).isActive = true
            weakSelf?.pdfView.topAnchor.constraint(equalTo:  (weakSelf?.pdfBaseView.topAnchor)!).isActive = true
            weakSelf?.pdfView.bottomAnchor.constraint(equalTo:  (weakSelf?.pdfBaseView.bottomAnchor)!).isActive = true
             weakSelf?.pdfView.displayMode = .singlePageContinuous
             weakSelf?.pdfView.autoScales = true
            
            SwiftLoader.hide()
            if let pdfLink = weakSelf?.PDFUrl{
                
                if let createdURL = URL(string:pdfLink)
                {
                    if let initializedURL = PDFDocument.init(url:createdURL){
                        weakSelf?.pdfView.document =  initializedURL
                    }else{
                        weakSelf?.showAlertWithAction(with: "Failed", message: "Could not initailized Url with given path")
                        
                    }
                }
            }else{
                
                self.showAlertWithAction(with: "Failed", message: "PDF Url is not correct")
            }
        }
        
        //   //  SwiftLoader.hide()
        // create and add the PDF view
      
 
        
    }
    func setUPUI()  {
        
        customNavigationBarForDrawer.titleLabel.text = nodeTitle
        customNavigationBarForDrawer.senderController = self
        customNavigationBarForDrawer.leftSideMenuButtonItem.tag = ButtonTag.BACK.rawValue
        customNavigationBarForDrawer.leftSideMenuButtonItem.setImage(#imageLiteral(resourceName: "back"), for: .normal)
    }
        // Go to Next Page
        func nextPage() {
            pdfView.goToNextPage(nil)
        }
        
        // Go to Previous Page
        func prevPage() {
            pdfView.goToPreviousPage(nil)
        }
        
        // Go to First Page
        func firstPage() {
            pdfView.goToFirstPage(nil)
        }
        
        // Go to Last Page
        func lastPage() {
            pdfView.goToLastPage(nil)
        }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
        
}


