//
//  NewsViewController.swift
//  kannuAirport
//
//  Created by Arpana on 23/05/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit
import SwiftLoader
import SDWebImage



class InformationViewController: UIViewController ,UIWebViewDelegate , UIScrollViewDelegate{
    
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var customNavigationBarForDrawer: CustomNavigationBarForDrawer!
    @IBOutlet weak var imgView: UIImageView!
    var nodeID: NodeType = .NOTAPPLIED
    var nodeIDToGetDetails :String?
    var nodeTitle :String?
    var isFromSideController: Bool = true
    @IBOutlet weak var headingLabel : UILabel!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUPUI()
    }
    
    override func viewDidLayoutSubviews() {
        
        self.webView.scrollView.contentSize = CGSize(width: self.webView.frame.size.width, height: webView.scrollView.contentSize.height)
        self.webView.updateConstraintsIfNeeded()
        
    }
    
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    // MARK:- WebView Delegate
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        print("request \(request)")
        let url = (request.url?.absoluteString)!
        if (url.contains("www.google.co.in") || url.contains("airindia") || url.contains("info@") || url.contains("goindigo") || url.contains("https://www.goair.in/") || url.contains("https://www.goindigo.in/web-check-in.html?linkNav=web-check-in_header") || url.contains("https://www.airindiaexpress.in/en/manage/manage-booking") || url.contains("https://www.goair.in/plan-my-trip/web-check-in/"))
        {
            webView.stopLoading()
            if let openUrl = request.url {
                UIApplication.shared.open(openUrl, options: [:], completionHandler: nil)
            }
            
            
           // Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            //startActivity(i);
        }
        
        if (url.contains("https://www.kannurairport.aero/")) {
//            String updatedUrl=url.replace("https://www.kannurairport.aero/","");
            let updatedUrl = url.replacingOccurrences(of: "https://www.kannurairport.aero/", with: "")
//
            if (!updatedUrl.contains("/")){
                SwiftLoader.hide()
                APICall.callTabularInformationAPI([:], header: ["Authorization" : AuthorizationToken ], URLPath: updatedUrl) { (success, responseData, error) in
                   
                    SwiftLoader.hide()
                    DispatchQueue.main.async {
                        
                        weak var weakSelf = self
                        
                        SwiftLoader.hide()
                        
                        if (error != nil){
                            
                            self.showAlertWithAction(with: "", message: error?.localizedDescription)
                        }
                        
                        if success {
                            do {
                            let jsonData = try JSONSerialization.jsonObject(with: responseData!, options: .mutableContainers)
                            
                            if let responseDict = (jsonData as? NSDictionary){
                                
                                if let bodyContent = responseDict.value(forKey: "body") as? NSArray {
                                    
                                    if bodyContent.count == 0 {
                                        return
                                    }
                                    if let details = bodyContent.object(at: 0) as? NSDictionary{
                                        
                                        weakSelf?.showOnWebView(text: details.value(forKey: "value") as! String)
                                    }
                                    
                                }else{
                                    
                                    weakSelf?.showAlertWithAction(with: "", message: AlertMessages.bodyError)
                                    
                                }
                                
                                }
                                
                            }catch {
                                
                            }
                                
                            
                                
                            
                        }
                            
                        else{
                            //Alert not sucessfull
                            weakSelf?.showAlertWithAction(with: "Failed", message: "Some error occured")
                            
                        }
                    
                    
                    }
                }
//
//                webServiceStringMenu(updatedUrl);
            }
        }
        
       
        
        
        
        return true
    }
    
     func webViewDidFinishLoad(_ webView: UIWebView) {
        
         SwiftLoader.hide()
          self.view.layoutIfNeeded()
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        SwiftLoader.show(animated: true)
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        SwiftLoader.hide()

    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.x > 0 {
            scrollView.contentOffset = CGPoint(x: 0, y: scrollView.contentOffset.y)
        }
        self.webView.scrollView.contentSize = CGSize(width: self.webView.frame.size.width, height: webView.frame.size.height)
        
        self.webView.updateConstraintsIfNeeded()
        self.view.layoutIfNeeded()
    }
    
    override func viewWillAppear(_ animated: Bool) {
    
        super.viewWillAppear(true)
        customNavigationBarForDrawer.titleLabel.text = nodeTitle
        headingLabel.text = nodeTitle
        getInformation()
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

    }
    func setUPUI()  {
        
        customNavigationBarForDrawer.titleLabel.text = nodeTitle
        customNavigationBarForDrawer.senderController = self
        customNavigationBarForDrawer.leftSideMenuButtonItem.tag = isFromSideController == true ? ButtonTag.MENU.rawValue :ButtonTag.BACK.rawValue
        customNavigationBarForDrawer.leftSideMenuButtonItem.setImage (isFromSideController == true ? #imageLiteral(resourceName: "3bar") : #imageLiteral(resourceName: "back"), for: .normal)
        self.webView.delegate = self
        self.webView.scrollView.showsHorizontalScrollIndicator = false
        
    }
    func getInformation()  {

        if let paramID = nodeIDToGetDetails {
            
            SwiftLoader.show(animated: true)

            APICall .callInformationAPI([ : ], header: ["Authorization" : AuthorizationToken ], nodeIDToGetInfo: nodeID, idToFetchDetails: paramID) { (sucess, result, error) in
                
                weak var weakSelf = self
                
                DispatchQueue.main.async {
                    
                    SwiftLoader.hide()
                    
                    if (error != nil){
                       
                        self.showAlertWithAction(with: "", message: error?.localizedDescription)

                    }
                    if sucess {
                        
                            
                            do {
                                let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                                
                                if let responseDict = (jsonData as? NSDictionary){
                                    
                                    if let bodyContent = responseDict.value(forKey: "body") as? NSArray {
                                        
                                            if bodyContent.count > 0 {
                                                if let details = bodyContent.object(at: 0) as? NSDictionary{
                                                
                                                weakSelf?.showOnWebView(text: details.value(forKey: "value") as! String)
                                            }
                                        }
                                        
                                    }else{
                                        
                                        weakSelf?.showAlertWithAction(with: "", message: AlertMessages.bodyError)

                                    }
                                    
                                    
                                    
                                    if let pdfContent = responseDict.value(forKey: "field_upload_pdf") as? NSArray {
                                        
                                        if pdfContent.count > 0 {
                                            if let details = pdfContent.object(at: 0) as? NSDictionary{
                                                
                                                print(details)
                                                if let pdfUrl = details.object(forKey: "url"){
                                                    let url = URL(string: pdfUrl as! String)
                                                    let urlReq = URLRequest(url: url!)
                                                    weakSelf?.webView.loadRequest(urlReq)
                                                }
                                            }
                                        
                                    }
                                    }
                                    
                                    
                                    
                                    
                                    
                                    if let imgContent = responseDict.value(forKey: "field_featured_image") as? NSArray {
                                        
                                        if imgContent.count == 0 {
                                            return
                                        }
                                        if let details = imgContent.object(at: 0) as? NSDictionary{
                                            
                                            //weakSelf?.showOnWebView(text: details.value(forKey: "url") as! String)
                                            if let imgUrl = details.value(forKey: "url") as? String{
                                                weakSelf?.imgView.sd_imageIndicator?.startAnimatingIndicator()
                                                weakSelf?.imgView.sd_setImage(with: URL(string:imgUrl), completed: { (image, error, type, url) in
                                                    if error == nil{
                                                        weakSelf?.imgView.image = image
                                                    }
                                                    weakSelf?.imgView.sd_imageIndicator?.stopAnimatingIndicator()
                                                })
                                            }else{
                                                weakSelf?.imgView.image = nil
                                            }
                                        }else{
                                             weakSelf?.showAlertWithAction(with: "", message: "No dictionary found for key field_featured_image")
                                        }
                                    }
//                                    else{
//                                       weakSelf?.showAlertWithAction(with: "", message: "No array found for key field_featured_image")
//                                        
//                                    }
                                }
                                
                                
//                            let jsonDecoder = JSONDecoder()
//
//                            let InfoObj = try  jsonDecoder.decode(Information.self, from: result!) as Information
//
//                            if  (InfoObj.body?.count)! > 0{
//
//                                if let text = (InfoObj.body?[0].value ){
//                                    weakSelf?.showOnWebView(text: text)
//
//                                }
//                            }else{
//                                //No content on this page
//                                weakSelf?.showOnWebView(text: "")
//                            }
                        } catch  {
                           

                        }
                    }
                        
                    else{
                        //Alert not sucessfull
                    }
                }
            }
        }
        
    }

    func showOnWebView(text: String)  {
       
        webView.loadHTMLString("<html><body> \(text)</body></html>", baseURL: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK:- IBActions
    @IBAction func backBtnTapped(sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    deinit {
        print(" \(webView)information class is deinitialized")
    }
}
