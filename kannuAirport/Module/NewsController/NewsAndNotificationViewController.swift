//
//  News&NotificationViewController.swift
//  kannuAirport
//
//  Created by Arpana on 01/06/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit

class NewsAndNotificationViewController: UIViewController  {
    
    @IBOutlet weak var pageView: UIView!
    var pageMenu : CAPSPageMenu?
    var newsArray : News?
    var ArrMutable : NSMutableArray?
    
    var isNewsToDisplay = true // This variable is maintained to keep tract of different view to be shown
    //if true consider news api, if false consider notification api
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AddPageMenu()

        
    }
    override func viewWillAppear(_ animated: Bool) {

    }
    
    func AddPageMenu(){
        // Array to keep track of controllers in page menu
        var controllerArray : [UIViewController] = []
        
        
        let newsListController : UIViewController = UIStoryboard.newsController()
        newsListController.title = ""
        
        controllerArray.append(newsListController)
        
        let notificationListController : UIViewController = UIStoryboard.notificationListViewController()
        notificationListController.title = ""
        controllerArray.append(notificationListController)
        
        let dictionary:NSDictionary = [
            CAPSPageMenuOptionScrollMenuBackgroundColor : UIColor.clear
            ,   CAPSPageMenuOptionViewBackgroundColor : UIColor.clear,CAPSPageMenuOptionMenuItemWidth : self.view.frame.size.width/2,CAPSPageMenuOptionMenuHeight: IS_IPHONE_X ?55 :60 ,CAPSPageMenuOptionCenterMenuItems: true ,CAPSPageMenuOptionMenuItemFont: UIFont(name: "Karla-Bold", size: 16.0)!
        ]
        
        let  frame = CGRect(x:0.0,y:54 , width:self.view.frame.size.width,height:self.view.frame.size.height - 60)
        
        pageMenu = CAPSPageMenu.init(viewControllers: controllerArray, frame: frame, options: dictionary as? [AnyHashable: Any] ?? [:])
        pageMenu!.delegate = self

        self.view.addSubview(pageMenu!.view)
        
    }
    
    override func viewDidLayoutSubviews() {
        
        var frame :CGRect
        if #available(iOS 11.0, *) {
            frame = CGRect(x:0.0,y: IS_IPHONE_X ? 50 + self.view.safeAreaInsets.top : 54, width:self.view.frame.size.width,height:  IS_IPHONE_X ? self.view.frame.size.height - ( self.view.safeAreaInsets.top) : self.view.frame.size.height )
        } else {
            // Fallback on earlier versions
            frame = CGRect(x:0.0,y:60 , width:self.view.frame.size.width,height:self.view.frame.size.height)
        }
        pageMenu?.view.frame = frame
        view.layoutIfNeeded()
    }
    
    deinit {
        print(" \(String(describing: newsArray)) is NewsArray deallocated")
    }
 
    // MARK:- IBActions
    @IBAction func backBtnTapped(sender:UIButton){
        kAppDelegate.drawerController.setDrawerState(.opened, animated: true)
    }
}
extension NewsAndNotificationViewController: CAPSPageMenuDelegate{
    
}
    

