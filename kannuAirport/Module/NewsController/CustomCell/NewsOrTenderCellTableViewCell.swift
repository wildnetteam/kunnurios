//
//  NewsOrTenderCellTableViewCell.swift
//  kannuAirport
//
//  Created by Arpana on 29/05/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit

class NewsOrTenderCellTableViewCell: UITableViewCell {

    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
