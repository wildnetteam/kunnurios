//
//  SpalshViewController.swift
//  kannuAirport
//
//  Created by Rajeev on 09/05/19.
//  Copyright © 2019 Arpana. All rights reserved.
//

import UIKit

class SpalshViewController: UIViewController {
    @IBOutlet weak var imageView:UIImageView?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        imageView?.startAnimating()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
