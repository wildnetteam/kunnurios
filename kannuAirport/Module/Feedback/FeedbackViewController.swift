//
//  FeedbackViewController.swift
//  
//
//  Created by Arpana on 06/06/18.
//

import UIKit
import SwiftLoader

class FeedbackViewController: UIViewController ,CountryViewDelegate  {
    
    @IBOutlet weak var customNavigationBarForDrawer: CustomNavigationBarForDrawer!
    var tempTxtField = UITextField()
    
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var mrTextField: UITextField!
    
    @IBOutlet weak var textFieldSelectCountry: UITextField!
    
    @IBOutlet weak var textFieldMobile: UITextField!
    
    @IBOutlet weak var textFieldEmail: UITextField!
    
    @IBOutlet weak var textFieldLocation: UITextField!
    
    @IBOutlet weak var textViewAddress: UITextView!
    
    @IBOutlet weak var textviewComments: UITextView!
    
    @IBOutlet weak var textFieldGravience: UITextField!
    
    var allCountryObject : Country?
    var terminalObject :   Terminal?
    var queryTypeObject :  QueryType?
    var nameTitle = ["Mr" , "Mrs"]
    var countryView:CountryView?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // MARK: - Get API response in backgroung
        
        DispatchQueue.global(qos: .background).async {
            print("In background")
            self.getTerminalDetails()
            
        }
        
        DispatchQueue.global(qos: .background).async {
            print("In background")
            self.getQueryTypes()
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        customNavigationBarForDrawer.titleLabel.text = "Feedback"
        customNavigationBarForDrawer.senderController = self
        customNavigationBarForDrawer.leftSideMenuButtonItem.tag = ButtonTag.MENU.rawValue
        customNavigationBarForDrawer.leftSideMenuButtonItem.setImage(#imageLiteral(resourceName: "3bar"), for: .normal)
        
        textViewAddress.placeholder = "Address (Optional...)"
        textViewAddress.textViewBorderColor(textViewAddress, color: UIColor(red:0.89, green:0.92, blue:0.93, alpha:1))
        
        textviewComments.placeholder = "Comments (Optional...)"
        textviewComments.textViewBorderColor(textviewComments, color: UIColor(red:0.89, green:0.92, blue:0.93, alpha:1))
        
        textFieldSelectCountry.rightViewMode = .always
        textFieldSelectCountry.AddImage(direction: .Right, imageName:"dropdown" , Frame:   CGRect(origin: CGPoint(x: 0,y :0 ), size: CGSize(width: 30, height: 30)), backgroundColor: UIColor.clear)
        
        textFieldGravience.rightViewMode = .always
        textFieldGravience.AddImage(direction: .Right, imageName:"dropdown" , Frame:   CGRect(origin: CGPoint(x: 0,y :0 ), size: CGSize(width: 30, height: 30)), backgroundColor: UIColor.clear)
        
        textFieldLocation.rightViewMode = .always
        textFieldLocation.AddImage(direction: .Right, imageName:"dropdown" , Frame:   CGRect(origin: CGPoint(x: 0,y :0 ), size: CGSize(width: 30, height: 30)), backgroundColor: UIColor.clear)
        
        mrTextField.rightViewMode = .always
        mrTextField.AddImage(direction: .Right, imageName:"dropdown" , Frame:   CGRect(origin: CGPoint(x: 0,y :0 ), size: CGSize(width: 30, height: 30)), backgroundColor: UIColor.clear)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    // MARK: - API to fetch Country, Location and Query Type
    
    
    func getCountryDetails()  {
        
        //   SwiftLoader.show(animated: true)
        
        APICall .callCountryAPI([ : ], header: ["Authorization" : AuthorizationToken ]) { (sucess, result, error) in
            DispatchQueue.main.async {
                
                //  SwiftLoader.hide()
                
                if (error != nil){
                    
                    self.showAlertWithAction(with: "", message: error?.localizedDescription)
                    
                }
                if sucess {
                    
                    do {
                        
                        let jsonDecoder = JSONDecoder()
                        self.allCountryObject = try  jsonDecoder.decode(Country.self, from: result!) as Country
                        print(self.allCountryObject?.data?.count)
                        
                    } catch  {
                        
                    }
                }
                else {
                    //Alert not sucessfull
                }
            }
        }
    }
    
    func getTerminalDetails()  {
        
        //SwiftLoader.show(animated: true)
        
        APICall .callTerminalTypeAPI([ : ], header: ["Authorization" : AuthorizationToken ]) { (sucess, result, error) in
            
            DispatchQueue.main.async {
                
                // SwiftLoader.hide()
                
                if (error != nil){
                    
                    self.showAlertWithAction(with: "", message: error?.localizedDescription)
                    
                }
                if sucess {
                    
                    do {
                        let jsonDecoder = JSONDecoder()
                        
                        self.terminalObject = try  jsonDecoder.decode(Terminal.self, from: result!) as Terminal
                        
                    } catch  {
                        
                    }
                }
                    
                else{
                    //Alert not sucessfull
                }
            }
        }
    }
    
    func getQueryTypes()  {
        
        //SwiftLoader.show(animated: true)
        
        APICall .callQueryTypeAPI([ : ], header: ["Authorization" : AuthorizationToken ]) { (sucess, result, error) in
            
            DispatchQueue.main.async {
                
                // SwiftLoader.hide()
                
                if (error != nil){
                    
                    self.showAlertWithAction(with: "", message: error?.localizedDescription)
                    
                }
                if sucess {
                    
                    do {
                        let jsonDecoder = JSONDecoder()
                        
                        self.queryTypeObject = try  jsonDecoder.decode(QueryType.self, from: result!) as QueryType
                        
                        
                    } catch  {
                        
                    }
                }
                    
                else{
                    //Alert not sucessfull
                }
            }
        }
    }
    
    
    @IBOutlet weak var textFieldName: UITextField!
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        textFieldGravience.resignFirstResponder()
        self.pickerView.isHidden = true
    }
    
    deinit {
        print("\(FeedbackViewController.self) is released ")
        
    }
    // MARK: - ToolBar button Delegates
    
    @objc func donePickerBtnTapped(sender : UIDatePicker ){
        
        if(tempTxtField.tag == 5){
            
            if let terminalOb = terminalObject {
                
                if (terminalOb.data?.count)! > 0 {
                    
                    textFieldLocation.text = terminalOb.data?[0].location
                }
            }
            else{
                textFieldLocation.text = tempTxtField.text
            }
            textFieldLocation.resignFirstResponder()
            
        }
        else if(tempTxtField.tag == 6){
            
            if tempTxtField.text == ""{
                
                if let queryOb = queryTypeObject {
                    
                    if (queryOb.data?.count)! > 0 {
                        
                        textFieldGravience.text = queryOb.data?[0].feedback
                    }
                }
            }else{
                textFieldGravience.text = tempTxtField.text
            }
            textFieldGravience.resignFirstResponder()
            
        }
        else if(tempTxtField.tag == 0){
            
            mrTextField.text = tempTxtField.text == "" ? nameTitle[0]:tempTxtField.text
            mrTextField.resignFirstResponder()
            
        }else{
            
            print("some other text field appeared")
        }
        
    }
    
    @objc func cancelPickerBtnTapped(sender:UIDatePicker){
        
        self.view.endEditing(true)
    }
    func showCountryView(type : Int){
        
        countryView = Bundle.main.loadNibNamed("CountryView", owner: self, options: nil)?[0] as? CountryView
        countryView?.delegate = self
        countryView?.registerNibs()
        countryView?.managerRadius()
        countryView?.tag = type
        kAppDelegate.window?.addSubview(countryView!)
        countryView?.alpha = 0
        countryView?.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
        UIView.animate(withDuration: 0.3, animations: {
            self.countryView?.alpha = 1
        } ,completion: { (done) in
            
            switch (self.countryView?.tag){
                
            case 1:   self.countryView?.lblTitle.text = "Select Country"
            self.countryView?.getCountryDetails()
                
                //            case 2: self.countryView?.lblTitle.text = "Select State"
                //            self.countryView?.getAllStates(region_id: self.txtCountry.tag )
                //
                //            case 3:  self.countryView?.lblTitle.text = "Select City"
            //            self.countryView?.getAllCities(region_id: self.txtState.tag)
            default: print("not correct tag")
                
            }
            
        } )
    }
    
    // MARK: - CountryViewDelegate
    
    func removeCountryView()  {
        
        self.countryView?.alpha = 1
        UIView.animate(withDuration: 0.3, animations: {
            self.countryView?.alpha = 0
        } ,completion: { (done) in
            self.countryView?.removeFromSuperview()
            self.countryView = nil
        } )
    }
    
    func getCountryCode(CountryName: String) {
        
        switch (self.countryView?.tag){
            
        case 1:  textFieldSelectCountry.text = CountryName
        default: print("not correct tag")
            
        }
        
    }
    @IBAction func submitBtnClicked(_ sender: Any) {
        if validateData () {
            submitFeedback()
        }
    }
    
    
    private func validateData() -> Bool {
        
        if textFieldName.text == nil || textFieldName.text == "" {
            textFieldName.becomeFirstResponder()

            self.showAlertWithAction(with: "", message: "Please enter Name")
            return false
            
            
        }
        if textFieldName.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            textFieldName.becomeFirstResponder()

            self.showAlertWithAction(with: "", message: "Please enter Name")

            return false
            
            
        }
        if textFieldEmail.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            textFieldEmail.becomeFirstResponder()

            self.showAlertWithAction(with: "", message: "Please enter Email")

            return false
            
        }
        if  (textFieldEmail.text?.trimmingCharacters(in: .whitespacesAndNewlines).count)! > 0 && !(textFieldEmail.text?.isValidEmail())! {
            textFieldEmail.becomeFirstResponder()

            self.showAlertWithAction(with: "", message: "Please enter valid Email")

            return false
        }
        if textFieldMobile.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            textFieldMobile.becomeFirstResponder()

            self.showAlertWithAction(with: "", message: "Please enter Mobile Number")

            return false
        }
        if (textFieldMobile.text?.trimmingCharacters(in: .whitespacesAndNewlines).count)! < 9 {
            textFieldMobile.becomeFirstResponder()
            
            self.showAlertWithAction(with: "", message: "Please enter atleast 10 digit Number")
            
            return false
        }
        
        if textFieldSelectCountry.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            textFieldSelectCountry.becomeFirstResponder()

            self.showAlertWithAction(with: "", message: "Please enter Country Name")
            return false
        }
        return true
        
        
    }
    
    //MARK:- Get banner images for advartise purpose
    func submitFeedback() {
        
        SwiftLoader.show(animated: true)
        
        
        let paramDict : [String : Any] = ["name": textFieldName.text ?? "" ,"email" : textFieldEmail.text ?? "" ,"phone" : textFieldMobile.text ?? "" ,  "location" : textFieldLocation.text ?? "" ,"country" : textFieldSelectCountry.text ?? "" ,"type_of_query" : textFieldGravience.text ?? "" ,"address" : textViewAddress.text ?? "" , "comment" : textviewComments.text ?? "" ,"form_id" : "feedback_form"]
        
        APICall.callSubmitFeedbbackAPI(paramDict, header: [:] )  { (sucess, result, error) in
            
            DispatchQueue.main.async {
                
                weak var weakSelf = self
                
                SwiftLoader.hide()
                
                if (error != nil){
                    
                    weakSelf?.showAlertWithAction(with: "", message: error?.localizedDescription)
                    return
                    
                }
                if sucess {
                    
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                        
                        if (jsonData is NSDictionary){
                            
                            if  let  resultDict = jsonData as? NSDictionary {
                                
                                if let successMsg = resultDict.value(forKey: "status") as? String {
                                    
                                    if successMsg == "success"{
                                        
                                        if let errorMsg = resultDict.value(forKey: "message") as? String {
                                            
                                            weakSelf?.showAlertWithAction(with: "", message: errorMsg )
                                        }else{
                                            weakSelf?.showAlertWithAction(with: "", message: "Data submitted sucessfully." )
                                            
                                        }
                                        weakSelf?.clearTextFields()
                                        
                                    }else{
                                        if let errorMsg = resultDict.value(forKey: "message") as? String {
                                            
                                            weakSelf?.showAlertWithAction(with: "", message: errorMsg )
                                        }else{
                                            weakSelf?.showAlertWithAction(with: "", message: "Data could not be submitted." )
                                            
                                        }
                                    }
                                }
                            }
                        }
                        
                        
                    } catch  {
                        
                        weakSelf?.showAlertWithAction(with: "", message: AlertMessages.MODELERRORMESSAGE)
                        
                    }
                }
                    
                else{
                    //Alert not sucessfull
                    weakSelf?.showAlertWithAction(with: "", message: AlertMessages.UnidetifiedErrorMessage)
                    
                }
            }
        }
    }
    
    func clearTextFields()   {
        
        textFieldSelectCountry.text = ""
        textFieldMobile.text = ""
        textFieldEmail.text = ""
        textFieldLocation.text = ""
        textViewAddress.text = ""
        textviewComments.text = ""
        textFieldGravience.text = ""
        textFieldName.text = ""
        mrTextField.text = ""
    }
}

//MARK: UITextViewDelegate

func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
    // Hide the keyboard.
    textView.resignFirstResponder()
    return true
}

// MARK: - UITextField Delegates

extension FeedbackViewController: UITextFieldDelegate {

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
    
        return true
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField){
        
      
            
            if textField == self.textFieldSelectCountry {
                textField.text = ""
                textField.tag = 0
                
                textField.resignFirstResponder()
                
                self.showCountryView(type: 1)
                
            }
            else  if(textField == self.textFieldGravience || textField == self.textFieldLocation || textField == self.mrTextField) {
                let picker = UIPickerView()
               // picker = UIPickerView(frame: CGRect(x:0, y:0, width:self.view.frame.width, height:200))
                picker.backgroundColor = ColorConstants.appBackGroundCOLOR()
                picker.showsSelectionIndicator = true
                picker.delegate = self
                picker.dataSource = self
                
                let toolBar = UIToolbar()
                toolBar.barStyle = UIBarStyle.default
                toolBar.isTranslucent = true
                toolBar.tintColor = UIColor.black
                toolBar.sizeToFit()
                
                let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(FeedbackViewController.donePickerBtnTapped))
                let monthTittle = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: self, action: nil)
                let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
                let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action:#selector(FeedbackViewController.cancelPickerBtnTapped))
                
                toolBar.setItems([cancelButton,spaceButton,monthTittle, spaceButton, doneButton], animated: false)
                toolBar.isUserInteractionEnabled = true
                
                textField.inputView = picker
                textField.inputAccessoryView = toolBar
                self.tempTxtField = textField
                picker.reloadAllComponents()
                
            
            }
       
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
      textField.resignFirstResponder()
//        if(textField == mrTextField && mrTextField.text == ""){
//            mrTextField.text = nameTitle[0]
//        }
//        if(textField == textFieldGravience && textFieldLocation.text = ""){
//
//            if let
//
//        }else if(textField == textFieldLocation && textFieldLocation.text = ""){
//
//            return terminalObject?.data?[row].location
//
//        }
//
       
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
       if(textField == self.textFieldGravience || textField == self.textFieldLocation || textField == self.mrTextField) {
            return false
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag = textField.tag + 1
        
        if let nextResponder = textField.superview?.viewWithTag(nextTag) {
            nextResponder.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        
        return true
    }
}
extension FeedbackViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if(tempTxtField == mrTextField){
            return nameTitle.count
        }
        if(tempTxtField == textFieldGravience){
            if let queryOb = queryTypeObject {
                return (queryOb.data?.count)!
            }else{
                
                getQueryTypes()
            }
        }else if(tempTxtField == textFieldLocation){
            
            if let locOb = terminalObject {
                return (locOb.data?.count)!
            }
            else{
                
                getTerminalDetails()
            }
        }
        
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(tempTxtField == mrTextField){
            return nameTitle[row]
        }
        if(tempTxtField == textFieldGravience){
            
            return queryTypeObject?.data?[row].feedback
            
        }else if(tempTxtField == textFieldLocation){
            
            return terminalObject?.data?[row].location
            
        }
        return "NA"
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if(tempTxtField == mrTextField){
            
            tempTxtField.text = nameTitle[row]
        }
        if(tempTxtField == textFieldGravience){
            
           tempTxtField.text =  queryTypeObject?.data?[row].feedback
            
        }else if(tempTxtField == textFieldLocation){
            
            tempTxtField.text =  terminalObject?.data?[row].location
            
        }
    }
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {

        let pickerLabel = UILabel()
        pickerLabel.textColor = UIColor.white
        pickerLabel.font = UIFont(name: "Karla-Bold", size: 16) // In this use your custom font
        pickerLabel.textAlignment = .center

        if(tempTxtField == mrTextField){

            pickerLabel.text = nameTitle[row]
        }
        else if(tempTxtField == textFieldGravience){

            pickerLabel.text =  queryTypeObject?.data?[row].feedback

        }else if(tempTxtField == textFieldLocation){

          pickerLabel.text =  terminalObject?.data?[row].location

        }
        return pickerLabel

    }
    
}


