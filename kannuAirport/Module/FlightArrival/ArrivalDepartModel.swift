//
// HomeModel.swift
//  LUX Application
//

import UIKit
/*
 Optional({
 data =     (
 {
 airline = "<null>";
 "arrival_departure" = Arrival;
 belt = "";
 counter = "";
 destination = "";
 eta = "14:30";
 etd = "";
 "flight_code" = dsad;
 "flight_date" = "2018-12-06";
 gate = "";
 id = 2;
 "last_updated" = "2006-12-18 09:12:02";
 origin = BOM;
 sta = "14:30";
 status = Scheduled;
 std = "";
 type = Domestic;
 via = "";
 },
 {
 airline = "<null>";
 "arrival_departure" = Arrival;
 belt = "";
 counter = "";
 destination = "";
 eta = "15:00";
 etd = "";
 "flight_code" = dsad;
 "flight_date" = "2018-12-06";
 gate = "";
 id = 3;
 "last_updated" = "2006-12-18 09:12:02";
 origin = MAA;
 sta = "15:00";
 status = Scheduled;
 std = "";
 type = Domestic;
 via = "";
 },
 {
 airline = "<null>";
 "arrival_departure" = Arrival;
 belt = "";
 counter = "";
 destination = "";
 eta = "15:15";
 etd = "";
 "flight_code" = "";
 "flight_date" = "2018-12-06";
 gate = "";
 id = 8;
 "last_updated" = "2006-12-18 10:12:01";
 origin = BLR;
 sta = "15:15";
 status = Scheduled;
 std = "";
 type = Domestic;
 via = "";
 },
 {
 airline = "<null>";
 "arrival_departure" = Arrival;
 belt = "";
 counter = "";
 destination = "";
 eta = "15:40";
 etd = "";
 "flight_code" = "";
 "flight_date" = "2018-12-06";
 gate = "";
 id = 9;
 "last_updated" = "2006-12-18 10:12:40";
 origin = DXB;
 sta = "15:40";
 status = Cancelled;
 std = "";
 type = International;
 via = "";
 },
 {
 airline = "<null>";
 "arrival_departure" = Arrival;
 belt = "";
 counter = "";
 destination = "";
 eta = "15:55";
 etd = "";
 "flight_code" = "";
 "flight_date" = "2018-12-06";
 gate = "";
 id = 10;
 "last_updated" = "2006-12-18 10:12:01";
 origin = DOH;
 sta = "15:55";
 status = Scheduled;
 std = "";
 type = International;
 via = "";
 },
 {
 airline = "<null>";
 "arrival_departure" = Arrival;
 belt = "";
 counter = "";
 destination = "";
 eta = "14:30";
 etd = "";
 "flight_code" = "";
 "flight_date" = "2018-12-06";
 gate = "";
 id = 16;
 "last_updated" = "2006-12-18 09:12:02";
 origin = BOM;
 sta = "14:30";
 status = Scheduled;
 std = "";
 type = Domestic;
 via = "";
 },
 {
 airline = "<null>";
 "arrival_departure" = Arrival;
 belt = "";
 counter = "";
 destination = "";
 eta = "15:00";
 etd = "";
 "flight_code" = "";
 "flight_date" = "2018-12-06";
 gate = "";
 id = 17;
 "last_updated" = "2006-12-18 09:12:02";
 origin = MAA;
 sta = "15:00";
 status = Scheduled;
 std = "";
 type = Domestic;
 via = "";
 },
 {
 airline = "<null>";
 "arrival_departure" = Arrival;
 belt = "";
 counter = "";
 destination = "";
 eta = "15:15";
 etd = "";
 "flight_code" = "";
 "flight_date" = "2018-12-06";
 gate = "";
 id = 22;
 "last_updated" = "2006-12-18 11:12:43";
 origin = BLR;
 sta = "15:15";
 status = Arrived;
 std = "";
 type = Domestic;
 via = "";
 },
 {
 airline = "<null>";
 "arrival_departure" = Arrival;
 belt = "";
 counter = "";
 destination = "";
 eta = "16:40";
 etd = "";
 "flight_code" = "";
 "flight_date" = "2018-12-06";
 gate = "";
 id = 23;
 "last_updated" = "2006-12-18 12:12:10";
 origin = DXB;
 sta = "15:40";
 status = Delayed;
 std = "";
 type = International;
 via = "";
 },
 {
 airline = "<null>";
 "arrival_departure" = Arrival;
 belt = "";
 counter = "";
 destination = "";
 eta = "15:55";
 etd = "";
 "flight_code" = "";
 "flight_date" = "2018-12-06";
 gate = "";
 id = 24;
 "last_updated" = "2006-12-18 10:12:01";
 origin = DOH;
 sta = "15:55";
 status = Scheduled;
 std = "";
 type = International;
 via = "";
 },
 {
 airline = "<null>";
 "arrival_departure" = Arrival;
 belt = "";
 counter = "";
 destination = "";
 eta = "14:30";
 etd = "";
 "flight_code" = "";
 "flight_date" = "2018-12-06";
 gate = "";
 id = 30;
 "last_updated" = "2006-12-18 09:12:02";
 origin = BOM;
 sta = "14:30";
 status = Scheduled;
 std = "";
 type = Domestic;
 via = "";
 },
 {
 airline = "<null>";
 "arrival_departure" = Arrival;
 belt = "";
 counter = "";
 destination = "";
 eta = "15:00";
 etd = "";
 "flight_code" = "";
 "flight_date" = "2018-12-06";
 gate = "";
 id = 31;
 "last_updated" = "2006-12-18 09:12:02";
 origin = MAA;
 sta = "15:00";
 status = Scheduled;
 std = "";
 type = Domestic;
 via = "";
 },
 {
 airline = "<null>";
 "arrival_departure" = Arrival;
 belt = "";
 counter = "";
 destination = "";
 eta = "15:15";
 etd = "";
 "flight_code" = "";
 "flight_date" = "2018-12-06";
 gate = "";
 id = 36;
 "last_updated" = "2006-12-18 11:12:43";
 origin = BLR;
 sta = "15:15";
 status = Arrived;
 std = "";
 type = Domestic;
 via = "";
 },
 
 message = "Records Found.";
 status = success;
 })
 */
class ArrivalDepartModel: NSObject {
    
    var arrival_departure : String?
    var belt : String?
    var counter :String?
    var destination : String?
    var eta : String?
    var etd :String?
    var flightCode:String?
    var flightDate:String?
    var flightId:String?
    var lastUpdated:String?
    var origin:String?
    var sta:String?
    var status:String?
    var std:String?
    var type:String?
    var via:String?
    var message:String?
    var statusMsg : String?
    var airline : String?
    var gate : String?
    class func initModel(res:[String : Any]) -> [ArrivalDepartModel]
    {
        var arrivalDepartData = [ArrivalDepartModel]()
        //        if let bookingData = res["bookingdata"]{
        //            let bookingDataDic = bookingData as! NSDictionary
        if let data = res["data"]
        {
            for dic in data as! [[String:Any]]
            {
                let model = ArrivalDepartModel()
                model.airline = dic["airline"] as? String
                model.arrival_departure = dic["arrival_departure"] as? String
                model.belt = dic["belt"] as? String
                model.counter = dic["counter"] as? String
                model.destination = dic["destination"] as? String
                model.eta = dic["eta"] as? String
                model.etd = dic["etd"] as? String
                model.flightCode = dic["flight_code"] as? String
                model.status = dic["status"] as? String
                model.flightDate = dic["flight_date"] as? String
                model.gate = dic["gate"] as? String
                model.flightId = dic["id"] as? String
                model.lastUpdated = dic["last_updated"] as? String
                model.origin = dic["origin"] as? String
                model.sta = dic["sta"] as? String
                model.statusMsg = res["status"] as? String
                model.type = dic["type"] as? String
                model.via = dic["via"] as? String
                model.std = dic["std"] as? String
                arrivalDepartData.append(model)
            }
        }
        return arrivalDepartData
        
    }
}
        


