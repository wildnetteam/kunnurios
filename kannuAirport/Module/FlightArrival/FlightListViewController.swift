//
//  FlightListViewController.swift
//  kannuAirport
//
//  Created by Arpana on 01/05/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit
import SideMenu
import SwiftLoader

class FlightListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var departureArry : [NSDictionary]?
    var arrivelArry : [NSDictionary]?
    @IBOutlet weak var imgView: UIView!

    var arrivalViewM : [ArrivalDepartModel] = []
    var tempArrivalViewM : [ArrivalDepartModel] = []
    
    let recordKey = "flight"//flightInfo"
    let dictionaryKeys = Set<String>(["Status", "Type", "FlightDate"])
    
    // a few variables to hold the results as we parse the XML
    
    var results: [[String: String]]?         // the whole array of dictionaries
    var currentDictionary: [String: String]? // the current dictionary
    var currentValue: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imgView.layer.masksToBounds = false
        imgView.layer.shadowOpacity = 0.4 ;
        imgView.layer.shadowColor = UIColor.black.cgColor
        imgView.layer.shadowOffset = CGSize(width: 0.0, height: 4.0)
        getArrivalList()
        
        // add
      tableView.register(UINib.init(nibName: "FlightArrivalTableViewCell", bundle: nil), forCellReuseIdentifier: "FlightArrivalTableViewCell")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getFlightLogo(forFlight:String)->String{
        if forFlight.contains("AI") {
            return "Air_india"
        }else if forFlight.contains("G8") {
            return "GoAir"
        }else if forFlight.contains("6E") {
            return "IndiGo"
        }else if forFlight.contains("IX") {
            return "Air_India_Express"
        }
        return ""
    }
    
    /*
    func serachWithFlightNumber(flightNumber:String){
        if flightNumber != ""{
        }else{
            var filteredArray = [ArrivalDepartModel]()
            for item in tempArrivalViewM {
                if (item.flightCode?.uppercased().contains(flightNumber.uppercased()) ?? <#default value#>) {
                    filteredArray.append(item)
                }
            }
            
            
            //            let newArray = self.arrivalViewM.filter({ (obj) -> Bool in
            //                obj.type == "\(kAppDelegate.filterText)"
            //            })
            //
            for item in filteredArray {
                print("Origin--\(item.origin!)--type:\(String(item.type!))")
            }
            
            
            //            let newArray = self.arrivalViewM.filter({ (obj) -> Bool in
            //                obj.type == "\(kAppDelegate.filterText)"
            //            })
            //
            //            print(newArray)
            
            self.arrivalViewM.removeAll()
            self.arrivalViewM = filteredArray
            
        }
        tableView.reloadData()
        }
       
    
    }
*/
    
    
    func getOriginArray()->Array<String>{
        var array = [String]()
        for item in tempArrivalViewM {
           array.append(item.origin!)
        }
      return array
    }
    
    func searchArrivalWithFlightNumber(flightNumber:String){
        
         if flightNumber !=  "" {
            
            
            var filteredArray = [ArrivalDepartModel]()
            for item in tempArrivalViewM {
                if (item.flightCode!.contains(flightNumber)) {
                    filteredArray.append(item)
                }
            }
            
            self.arrivalViewM.removeAll()
            self.arrivalViewM = filteredArray
            
         }else{
            self.arrivalViewM = tempArrivalViewM
        }
        tableView.reloadData()
    }
    
    func getArrivalList()  {
        
         self.arrivalViewM.removeAll()
        if tableView != nil {
            self.tableView.showAnimatedSkeleton()
        }
         SwiftLoader.show(animated: true)
        
        APICall .callFlightArrivalDepartAPI([ : ], header: ["Authorization" : AuthorizationToken ]) { (sucess, result, error) in
            DispatchQueue.main.async {
                
                weak var weakSelf = self
                
                SwiftLoader.hide()
                
                if (error != nil){
                    
                    self.showAlertWithAction(with: "", message: error?.localizedDescription)
                    
                }
                if sucess {
                    
                    do {
                        
                        
                        let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                        
                        if (jsonData is NSDictionary){
                            
                            let modelArray = ArrivalDepartModel.initModel(res: jsonData as! [String : Any])
                            
                            self.arrivalViewM = modelArray
                            self.tempArrivalViewM = self.arrivalViewM
                            
                            /*   if kAppDelegate.filterText !=  "" {
                             let newArray = modelArray.filter({ (obj) -> Bool in
                             obj.type == "\(kAppDelegate.filterText)"
                             })
                             
                             print(newArray)
                             self.arrivalViewM = newArray
                             
                             }else{
                             
                             self.arrivalViewM = modelArray
                             }*/
                            
                            
                        }
                        
                        weakSelf?.tableView.reloadData()
                        
                        
                    } catch  {
                        weakSelf?.showAlertWithAction(with: "", message: AlertMessages.MODELERRORMESSAGE)
                        
                    }
                }
                else {
                    //Alert not sucessfull
                    self.showAlertWithAction(with: "Failed", message: "Some error occured")
                    
                }
                //                if (weakSelf?.refreshControl.isRefreshing)!  {
                //
                //                    weakSelf?.refreshControl.endRefreshing()
                //                }
                if weakSelf?.tableView != nil {
                    weakSelf?.tableView.hideSkeleton()
                }
            }
        }
    }
    
    
    func  filterDataForOrigin (origin:String) {
        
       
        if origin !=  "" {
            
            
            var filteredArray = [ArrivalDepartModel]()
            for item in tempArrivalViewM {
                if (item.origin! == origin) {
                    filteredArray.append(item)
                }
            }
            
            self.arrivalViewM.removeAll()
            self.arrivalViewM = filteredArray
            
        }else{
            self.arrivalViewM = tempArrivalViewM
        }
        tableView.reloadData()
        
    }
    
    
    
    func  dataAfterAppliedFilter () {
        
        if kAppDelegate.filterText ==  "Both" {
            
            getArrivalList()
        }
        else if kAppDelegate.filterText !=  "" {
            
            
            var filteredArray = [ArrivalDepartModel]()
            for item in tempArrivalViewM {
                if (item.type == kAppDelegate.filterText ) {
                    filteredArray.append(item)
                }
            }
            
            
            //            let newArray = self.arrivalViewM.filter({ (obj) -> Bool in
            //                obj.type == "\(kAppDelegate.filterText)"
            //            })
            //
            for item in filteredArray {
                print("Origin--\(item.origin!)--type:\(String(item.type!))")
            }
            
            
//            let newArray = self.arrivalViewM.filter({ (obj) -> Bool in
//                obj.type == "\(kAppDelegate.filterText)"
//            })
//
//            print(newArray)
            
            self.arrivalViewM.removeAll()
            self.arrivalViewM = filteredArray
            
        }
        tableView.reloadData()
        
    }
    
    func getXMLDocFromURL(){
        
        if let url = URL.init(string: "http:/58.84.23.95:8080/flightData.xml")as? URL {
            let task = URLSession.shared.dataTask(with: url) { data, response, error in
                guard let data = data, error == nil else {
                    print(error ?? "Unknown error")
                    return
                }
                
                let parser = XMLParser(data: data)
                parser.delegate = self
                if parser.parse() {
                    print(self.results ?? "No results")
                }
            }
            task.resume()
            
        }
    }
}
extension FlightListViewController : UITableViewDelegate, UITableViewDataSource , UIViewControllerTransitioningDelegate{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section:
        Int) -> Int
    {
        if let array = self.arrivalViewM  as? [ArrivalDepartModel] {

            return array.count

        }
        return 0
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        // Allocates a Table View Cell
        let cell =
            tableView.dequeueReusableCell(withIdentifier: "arrivalDepartureCell",
                                          for: indexPath) as! ArrivalDepartureTableCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        if indexPath.row < arrivalViewM.count {

            if let model = self.arrivalViewM[indexPath.row] as? ArrivalDepartModel {
                print(model)
               let flightCode = model.flightCode
               let flightStatus = model.status
               let flightTime = model.eta
                let flighImg = getFlightLogo(forFlight: flightCode!)
                
//                if flighImg != ""{
//                  cell.flighImgView?.image = UIImage(named: flighImg)
//                }
                cell.flighImgView?.image = UIImage(named: flighImg)

                cell.flightNumberLbl?.text = flightCode
                if flightStatus == "Arrived"{
                    cell.fligthStatusLbl?.textColor = UIColor.yellow
                }else if flightStatus == "Delayed"{
                    cell.fligthStatusLbl?.textColor = UIColor.red
                }else if flightStatus == "Rescheduled"{
                    cell.fligthStatusLbl?.textColor = UIColor.yellow
                }else if flightStatus == "Expected"{
                    cell.fligthStatusLbl?.textColor = UIColor.green
                }else if flightStatus == "Scheduled"{
                    cell.fligthStatusLbl?.textColor = UIColor.white
                }else{
                    cell.fligthStatusLbl?.textColor = UIColor.white
                }
                
                
                
                cell.fligthStatusLbl?.text = flightStatus
                cell.fromLablCode?.text = model.origin
                cell.toLablCode?.text = "CNN"
                cell.toLablFull?.text = "Kannur"
                if model.belt == ""{
                    cell.flightGateLbl?.text = "Belt NA"
                }else{
                    cell.flightGateLbl?.text = model.belt
                    
                }
                
                cell.fligthDateLbl?.text = model.flightDate
                cell.flightTime2Lbl?.text = "ETA \(flightTime!)"
               // cell.flighTime2Lbl?.text = "ETA: \(flightTime!)"
                cell.flighTimeLbl?.text = "STA \(model.sta!)"
               // cell.setData(arriavlModel: model)
            }
        }
//        let layer = UIView(frame: CGRect(x: 0, y: heightForHeader - 3, width: cell.contentView.frame.width, height: 1))
//
//        layer.layer.borderWidth = 1
//        layer.layer.borderColor = UIColor(red:0.89, green:0.92, blue:0.93, alpha:1).cgColor
//        cell.contentView.addSubview(layer)
//
//        if let newsObj  = self.ArrMutable?[indexPath.row] as? CurrentNews {
//
//            cell.lblTitle.text = newsObj.title
//            cell.lblDate.text =  newsObj.created //newsObj.dateFromString(dt: newsObj.created)
//
//        }
        //cell.accessoryType = .disclosureIndicator
        return cell
    }
    
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath){
//        let delay : CGFloat =     1.0
//        UIView.beginAnimations("transalation", context: nil)
//        UIView.animate(withDuration: 5.0, delay:TimeInterval(delay), options: UIViewAnimationOptions.repeat, animations: {}, completion: nil)
//        UIView.commitAnimations()
//
//    }
    
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//
//        return heightForHeader
//    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
       
    }
    

    
}

extension FlightListViewController: XMLParserDelegate {
    
    // initialize results structure
    
    func parserDidStartDocument(_ parser: XMLParser) {
        results = []
    }
    
    // start element
    //
    // - If we're starting a "record" create the dictionary that will hold the results
    // - If we're starting one of our dictionary keys, initialize `currentValue` (otherwise leave `nil`)
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        if elementName == recordKey {
            currentDictionary = [:]
        } else if dictionaryKeys.contains(elementName) {
            currentValue = ""
        }
    }
    
    // found characters
    //
    // - If this is an element we care about, append those characters.
    // - If `currentValue` still `nil`, then do nothing.
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        currentValue? += string
    }
    
    // end element
    //
    // - If we're at the end of the whole dictionary, then save that dictionary in our array
    // - If we're at the end of an element that belongs in the dictionary, then save that value in the dictionary
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if elementName == recordKey {
            results!.append(currentDictionary!)
            currentDictionary = nil
        } else if dictionaryKeys.contains(elementName) {
            currentDictionary![elementName] = currentValue
            currentValue = nil
        }
    }
    
    // Just in case, if there's an error, report it. (We don't want to fly blind here.)
    
    func parser(_ parser: XMLParser, parseErrorOccurred parseError: Error) {
        print(parseError)
        
        currentValue = nil
        currentDictionary = nil
        results = nil
    }
    
}

