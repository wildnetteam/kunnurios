//
//  HomeViewModel.swift
//  LUX Application
//

import UIKit


class ArrivalDepartViewModel: NSObject {
    
    private var tripsModel: ArrivalDepartModel
    
    
    var arrivalDeparture : String?{
        return tripsModel.arrival_departure
    }
    var belt : String?{
        return tripsModel.belt
    }
    var counter:String?{
        return tripsModel.counter
    }
    var destination : String?{
        return tripsModel.destination
    }
    
    var eta : String?{
        return tripsModel.eta
    }
    
    var flightCode : String?{
        return tripsModel.flightCode
    }
    var flightDate : String?{
        return tripsModel.flightDate
    }
    var flightId : String?{
        return tripsModel.flightId
    }
    var lastUpdated : String?{
        return tripsModel.lastUpdated
    }
    var origin : String?{
        return tripsModel.origin
    }
    var sta : String?{
        return tripsModel.sta
    }
    var status : String?{
        return tripsModel.status
    }
    var std : String?{
        return tripsModel.std
    }
    var type : String?{
        return tripsModel.type
    }
    var via : String?{
        return tripsModel.via
    }
    var message : String?{
        return tripsModel.message
    }
    var statusMsg : String?{
        return tripsModel.statusMsg
    }
    var airline : String?{
        return tripsModel.airline
    }
    var gate : String?{
        return tripsModel.gate
    }
    
    init(model: ArrivalDepartModel) {
        self.tripsModel = model
    }
}
