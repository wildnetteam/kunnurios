//
//  FlightArrivalTableViewCell.swift
//  kannuAirport
//
//  Created by Rajeev on 07/05/19.
//  Copyright © 2019 Arpana. All rights reserved.
//

import UIKit

class ArrivalDepartureTableCell: UITableViewCell {
    
    @IBOutlet weak var flightNumberLbl:UILabel?
    
    @IBOutlet weak var flightGateLbl:UILabel?
    @IBOutlet weak var fromLablCode:UILabel?
    @IBOutlet weak var fromLablFull:UILabel?
    
    @IBOutlet weak var flighImgView:UIImageView?
    @IBOutlet weak var flighTimeLbl:UILabel?
    
    @IBOutlet weak var toLablCode:UILabel?
    @IBOutlet weak var toLablFull:UILabel?
    
    @IBOutlet weak var fligthStatusLbl:UILabel?
    
    @IBOutlet weak var fligthDateLbl:UILabel?
    
    @IBOutlet weak var flightTime2Lbl:UILabel?
    
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
