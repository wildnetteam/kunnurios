//
//  DetailController.swift
//  kannuAirport
//
//  Created by Rajeev on 17/05/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit


class DetailController: UIViewController {
    
    @IBOutlet weak var pageMenuView: UIView!
    
    var flightArrivalController: FlightListViewController = UIStoryboard.FlightListController()
    
    let flightDepartureController : FlightDepartureController = UIStoryboard.flightDepartureController()

    @IBOutlet weak var topViewHeight:NSLayoutConstraint?
     @IBOutlet weak var searchBar:UISearchBar?
    @IBOutlet weak var searchView:UIView?
     @IBOutlet weak var originDestBtn:UIButton?
    
    
    var pageMenu : CAPSPageMenu?
    
    var SelectedPageIndex : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar?.isHidden = true
        searchBar?.delegate = self
        searchBar?.placeholder = "Flight Number"
        
         searchView?.isHidden = true
        originDestBtn?.layer.masksToBounds = true
        originDestBtn?.layer.cornerRadius = 5
        topViewHeight?.constant = 114
        AddPageMenu(yPosition: 55)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        fileterApplicable()
    
    }
    
    func removePageMenu(){
    pageMenu?.delegate = nil
    pageMenu?.view.removeFromSuperview()
    pageMenu = nil
    }
    
    @IBAction func originDestBtn(_ sender:UIButton){
        let regularFont = UIFont.systemFont(ofSize: 16)
        let boldFont = UIFont.boldSystemFont(ofSize: 16)
        let blueColor =  UIColor.blue
        
        
    var title = ""
        if SelectedPageIndex == 0 {
            title = "Select Origin"
        }else{
            title = "Select Destination"
        }
        
        
        let blueAppearance = YBTextPickerAppearanceManager.init(
            pickerTitle         : title,
            titleFont           : boldFont,
            titleTextColor      : .black,
            titleBackground     : .clear,
            searchBarFont       : regularFont,
            searchBarPlaceholder: title,
            closeButtonTitle    : "Cancel",
            closeButtonColor    : .darkGray,
            closeButtonFont     : regularFont,
            doneButtonTitle     : "Done",
            doneButtonColor     : blueColor,
            doneButtonFont      : boldFont,
            checkMarkPosition   : .Right,
            itemCheckedImage    : UIImage(named:"blue_ic_checked"),
            itemUncheckedImage  : UIImage(),
            itemColor           : .black,
            itemFont            : regularFont
        )
        
        
        
        let originDeptArray = SelectedPageIndex == 0 ? flightArrivalController.getOriginArray() : flightDepartureController.getDepartureArray()
        let picker = YBTextPicker.init(with: originDeptArray, appearance: blueAppearance,
                                       onCompletion: { (selectedIndexes, selectedValues) in
                                        if selectedValues.count > 0{
                                            self.originDestBtn?.setTitle(selectedValues[0], for: .normal)
                                        }
                                        
                                        if  self.SelectedPageIndex == 0 {
                                            self.flightArrivalController.filterDataForOrigin(origin: selectedValues[0])
                                        }else{
                                            self.flightDepartureController.filterDataForDestination(destination: selectedValues[0])
                                        }
        },
                                       onCancel: {
                                        print("Cancelled")
        }
        )
        
       
        picker.allowMultipleSelection = true
        
        picker.show(withAnimation: .Fade)
    }
    
    func fileterApplicable() {
        
        if kAppDelegate.filterText != "" {
            
            //filteris applied, so fetch data accordingly
            
            switch SelectedPageIndex {
                
            case 0:
                //refersh page at 0 index
                flightArrivalController.dataAfterAppliedFilter()
                
            case 1:
                flightDepartureController.dataAfterAppliedFilter()
                
            default:
                print("wrong index")
            }
        }
    }
    
    func setLabelText(){
        
        
        if SelectedPageIndex == 0 {
            originDestBtn?.setTitle("Select Origin", for: .normal)
        }else{
            originDestBtn?.setTitle("Select Dest.", for: .normal)
            
        }
    }
    
    @IBAction func searchButtonClicked(_ sender: UIButton){
        sender.isSelected = !sender.isSelected
        
        if sender.isSelected {
            searchBar?.isHidden = false
            searchView?.isHidden = false
            setLabelText()
            topViewHeight?.constant = 170
            
//            let  frame = CGRect(x:0.0,y:110 , width:self.view.frame.size.width,height:self.view.frame.size.height)
//
//            pageMenu?.view.frame =  frame
//            self.view.layoutIfNeeded()
            
            removePageMenu()
            AddPageMenu(yPosition: 115)
        }else{
            searchBar?.isHidden = true
            searchView?.isHidden = true
            topViewHeight?.constant = 114
            
//            let  frame = CGRect(x:0.0,y:64 , width:self.view.frame.size.width,height:self.view.frame.size.height)
//
//            pageMenu?.view.frame =  frame
//            self.view.layoutIfNeeded()
            removePageMenu()
            AddPageMenu(yPosition: 55)
        }
    }
    
    @IBAction func filterButtonClicked(_ sender: Any) {
        
        NavigationManager.moveToFilterControllerIfNotExists(self.navigationController!)
    }
    
    func AddPageMenu(yPosition:Int){
        
        // Array to keep track of controllers in page menu
        var controllerArray : [UIViewController] = []
     //   let flightArrivalController : UIViewController = UIStoryboard.FlightListController()
        flightArrivalController.title = ""
        
        controllerArray.append(flightArrivalController)
        
       // let flightDepartureController : UIViewController = UIStoryboard.flightDepartureController()
        flightDepartureController.title = ""
        controllerArray.append(flightDepartureController)
       
        let dictionary:NSDictionary = [
            CAPSPageMenuOptionScrollMenuBackgroundColor : UIColor.clear
            ,   CAPSPageMenuOptionViewBackgroundColor : UIColor.clear,CAPSPageMenuOptionMenuItemWidth : self.view.frame.size.width/2,CAPSPageMenuOptionMenuHeight: IS_IPHONE_X ? 15:20 ,CAPSPageMenuOptionCenterMenuItems: true ,CAPSPageMenuOptionMenuItemFont: UIFont(name: "Karla-Bold", size: 16.0)!
        ]
        
       
        
      
       
        let  frame = CGRect(x:0.0,y:0 , width:self.view.frame.size.width,height:self.view.frame.size.height)
        print(yPosition)
        pageMenu = CAPSPageMenu.init(viewControllers: controllerArray, frame: frame, options: dictionary as? [AnyHashable: Any] ?? [:])
        pageMenu!.delegate = self
       // pageMenu?.view.backgroundColor = UIColor.gray
        self.view.layoutIfNeeded()
        
        pageMenu?.selectionIndicatorColor = UIColor.gray
        pageMenu?.bottomMenuHairlineColor = UIColor.green
        pageMenu?.menuItemSeparatorColor = UIColor.blue
        
        
        self.pageMenuView.addSubview(pageMenu!.view)
    }
    
    override func viewDidLayoutSubviews() {
        
        var frame :CGRect
        if #available(iOS 11.0, *) {
            frame = CGRect(x:0.0,y: IS_IPHONE_X ? 50 + self.view.safeAreaInsets.top : 54, width:self.view.frame.size.width,height:self.view.frame.size.height)
        } else {
            // Fallback on earlier versions
            frame = CGRect(x:0.0,y:60 , width:self.view.frame.size.width,height:self.view.frame.size.height)
        }
        //pageMenu?.view.frame = frame
        view.layoutIfNeeded()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK:- IBActions
    
    
    @IBAction func arrivalButtonTapped(sender:UIButton){
        SelectedPageIndex = 0
        pageMenu?.move(toPage: 0)
        setLabelText()
        searchBar?.text = ""
        self.flightArrivalController.filterDataForOrigin(origin: "")
        
        
    }
    
    
    @IBAction func departureButtonTapped(sender:UIButton){
        SelectedPageIndex = 1
         pageMenu?.move(toPage: 1)
        setLabelText()
        searchBar?.text = ""
        self.flightDepartureController.filterDataForDestination(destination: "")
        
    }
    
    
     @IBAction func refreshButtonTapped(sender:UIButton){
        
        switch SelectedPageIndex {
            
        case 0:
            //refersh page at 0 index
            flightArrivalController.getArrivalList()
            
         case 1:
            flightDepartureController.getDepartureList()
            
        default:
            print("wrong index")
        }
        
    }
    
    @IBAction func backBtnTapped(sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    deinit {

        print("\(String(describing: self.pageMenu)) pageMenu is deinitialized " )
        
    }
    
}

extension DetailController: CAPSPageMenuDelegate {
   
    func didMove(toPage controller: UIViewController!, index: Int) {
        print("index : \(index)")
        SelectedPageIndex = index
        setLabelText()
    }
}


extension DetailController:UISearchBarDelegate {
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        
        switch SelectedPageIndex {
            
        case 0:
            //refersh page at 0 index
            flightArrivalController.searchArrivalWithFlightNumber(flightNumber: searchBar.text!)
            
        case 1:
            flightDepartureController.searchArrivalWithFlightNumber(flightNumber: searchBar.text!)
            
        default:
            print("wrong index")
        }
        
       
        
    }
    

    
    
}
