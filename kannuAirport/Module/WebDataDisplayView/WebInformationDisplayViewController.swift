//
//  WebInformationDisplayViewController.swift
//  kannuAirport
//
//  Created by Arpana on 25/09/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

    import UIKit
    import SwiftLoader
    import SDWebImage
    class WebInformationDisplayViewController: UIViewController ,UIWebViewDelegate , UIScrollViewDelegate{
        
        @IBOutlet weak var webView: UIWebView!
        @IBOutlet weak var customNavigationBarForDrawer: CustomNavigationBarForDrawer!
        @IBOutlet weak var imgView: UIImageView!
         @IBOutlet weak var imgViewCommingSoon: UIImageView!
        var nodeID: NodeType = .NOTAPPLIED
        var WebURL :String?
        var nodeTitle :String?
        var isFromSideController: Bool = true
        
        
        override func viewDidLoad() {
            super.viewDidLoad()
            setUPUI()
        }
        
        override func viewDidLayoutSubviews() {
            
            self.webView.scrollView.contentSize = CGSize(width: self.webView.frame.size.width, height: webView.scrollView.contentSize.height)
            self.webView.updateConstraintsIfNeeded()
            
        }
        
        // MARK:- WebView Delegate
        func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
            print("request \(request)")
            return true
        }
        
        func webViewDidFinishLoad(_ webView: UIWebView) {
            
            SwiftLoader.hide()
            self.view.layoutIfNeeded()
        }
        
        func webViewDidStartLoad(_ webView: UIWebView) {
            SwiftLoader.show(animated: true)
        }
        func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
            SwiftLoader.hide()
            
        }
        
        func scrollViewDidScroll(_ scrollView: UIScrollView) {
            if scrollView.contentOffset.x > 0 {
                scrollView.contentOffset = CGPoint(x: 0, y: scrollView.contentOffset.y)
            }
            self.webView.scrollView.contentSize = CGSize(width: self.webView.frame.size.width, height: webView.frame.size.height)
            
            self.webView.updateConstraintsIfNeeded()
            self.view.layoutIfNeeded()
        }
        
        override func viewWillAppear(_ animated: Bool) {
            
            super.viewWillAppear(true)
            
            if WebURL == "" {
                
                imgViewCommingSoon.isHidden = false
                return
            } else if WebURL == "map" {
                
                imgViewCommingSoon.isHidden = false
                imgViewCommingSoon.image = UIImage(named: "map_image")
                return
            }
            customNavigationBarForDrawer.titleLabel.text = nodeTitle
            
            guard let completeURL = WebURL else {
                
                self.showAlertWithAction(with: "", message: "Directed Url is not found")
                
                return
            }
            webView.loadRequest(URLRequest(url: URL(string: completeURL)!))
            
        }
        
        override func viewDidDisappear(_ animated: Bool) {
            super.viewDidDisappear(animated)
            
        }
        func setUPUI()  {
            
            customNavigationBarForDrawer.titleLabel.text = nodeTitle
            customNavigationBarForDrawer.senderController = self
            customNavigationBarForDrawer.leftSideMenuButtonItem.tag = isFromSideController == true ? ButtonTag.MENU.rawValue :ButtonTag.BACK.rawValue
            customNavigationBarForDrawer.leftSideMenuButtonItem.setImage (isFromSideController == true ? #imageLiteral(resourceName: "3bar") : #imageLiteral(resourceName: "back"), for: .normal)
            self.webView.delegate = self
            self.webView.scrollView.showsHorizontalScrollIndicator = false
            
        }
        

        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
        
        // MARK:- IBActions
        @IBAction func backBtnTapped(sender:UIButton){
            self.navigationController?.popViewController(animated: true)
        }
        
        deinit {
            print(" \(webView)information class is deinitialized")
        }
}

