//
//  GalleryViewController.swift
//  kannuAirport
//
//  Created by Arpana on 30/05/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit
import SwiftLoader

class GalleryViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var customNavigationBarForDrawer: CustomNavigationBarForDrawer!
    private let refreshControl = UIRefreshControl()
    var showImageScrollView : ShowImageScrollView?
    var pageNumber = 0
    var photoObject : PhotoGalleryDetails? //Logo is an array
    var nodeID  : String? // to fetch detials with particular nodeID
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUPUI()
        getGalleryDetails()
    }
    
    func setUPUI(){
    
    customNavigationBarForDrawer.titleLabel.text =  "Gallery"
    customNavigationBarForDrawer.senderController = self
    customNavigationBarForDrawer.leftSideMenuButtonItem.tag = ButtonTag.BACK.rawValue
    customNavigationBarForDrawer.leftSideMenuButtonItem.setImage(#imageLiteral(resourceName: "back"), for: .normal)
    collectionView.refreshControl = refreshControl
        // Configure Refresh Control
    refreshControl.addTarget(self, action: #selector(refreshGalleryDetailsData(_:)), for: .valueChanged)
    }
    
    @objc private func refreshGalleryDetailsData(_ sender: Any) {
        // Fetch Weather Data
        getGalleryDetails()
    }
    
    //MARK:- Get banner images for advartise purpose
    func getGalleryDetails() {
        
        SwiftLoader.show(animated: true)
        
         pageNumber = pageNumber + 1
        
        let paramDict : [String : Any] = ["nid": Int(nodeID!)! ,"number" : pageNumber ,"per_page" : 10 ]
        
        APICall.callGalleryDetailsAPI(paramDict, header: [:] )  { (sucess, result, error) in
            
            DispatchQueue.main.async {
                
                weak var weakSelf = self

                SwiftLoader.hide()
                
                if (error != nil){
                   
                    weakSelf?.showAlertWithAction(with: "", message: error?.localizedDescription)
                    return

                }
                if sucess {
                    
                    do {
                        let jsonDecoder = JSONDecoder()
                        if(weakSelf?.pageNumber == 1){ //its 1st page
                            
                            weakSelf?.photoObject = try  jsonDecoder.decode(PhotoGalleryDetails.self, from: result!) as PhotoGalleryDetails
                        }else{
                            //its load more
                            //So add data to existing array
                            
                            let  nextPhotoObject : PhotoGalleryDetails =  try  jsonDecoder.decode(PhotoGalleryDetails.self, from: result!) as PhotoGalleryDetails
                            
                            self.photoObject?.data.append(contentsOf: nextPhotoObject.data )
                            
                            
                        }
                        
                        self.collectionView.reloadData()
                        
                    } catch  {
                        
                   weakSelf?.showAlertWithAction(with: "", message: AlertMessages.MODELERRORMESSAGE)

                    }
                    if (weakSelf?.refreshControl.isRefreshing)!  {
                        
                        weakSelf?.refreshControl.endRefreshing()
                    }
                }
                    
                else{
                    //Alert not sucessfull
                    weakSelf?.showAlertWithAction(with: "", message: AlertMessages.UnidetifiedErrorMessage)

                }
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    deinit {
        
        print("\(String(describing: photoObject)) photoObject is deinitialized")
    }
}

extension GalleryViewController: UICollectionViewDelegate , UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if let logoCount = photoObject?.data{
            
            return logoCount.count
            
        } else{
            return 0
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryCollectionViewCellID", for: indexPath) as! GalleryCollectionViewCell
        
        if let val  = self.photoObject?.data[indexPath.row] {

                cell.imgView.sd_imageIndicator?.startAnimatingIndicator()
                cell.imgView.sd_setImage(with: URL(string: val.medium!), completed: { (image, error, type, url) in
                    if error == nil{
                        cell.imgView.image = image
                    }
                    cell.imgView.sd_imageIndicator?.stopAnimatingIndicator()
                })
            }else{
                cell.imgView.image = nil
            }
        //test///////////////////
        
        if(indexPath.row ==  (photoObject?.data.count)! - 1 && CGFloat(pageNumber) <= ceil(CGFloat((photoObject?.allRecode)!/10))){// i.e 10 < total -> 1288/10
            //we are at last index of currently displayed cell
            //reload more data
            getGalleryDetails()
            
        }
            return cell
        
    
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let cell = collectionView.cellForItem(at: indexPath) as! GalleryCollectionViewCell
        
           if let val  = self.photoObject?.data[indexPath.row] {
            
          //  VDSAnimation.shared.setupAnimation(in: self.view, imageView:UIImageView(image: UIImage(named: val.large ?? val.medium ?? "")))
          //  VDSAnimation.shared.setupAnimation(in: self.view, imageView: cell.imgView)
            self.addImageScrollView(imageName: val.large)

           }else {
        }
    }
   
    
    func addImageScrollView(imageName:String!){
        
        if imageName == nil  || imageName == "" {
            return
        }
        
        showImageScrollView = Bundle.main.loadNibNamed("ShowImageScrollView", owner: self, options: nil)?[0] as? ShowImageScrollView
        showImageScrollView?.delegate = self

        showImageScrollView?.imageView.sd_setImage(with: URL(string:imageName!), completed: { (imageReceiced, error, type, url) in
            if error == nil{
                self.showImageScrollView?.setImage(image:imageReceiced!)
            }
            self.showImageScrollView?.imageView.sd_imageIndicator?.stopAnimatingIndicator()
        })
        
      //  showImageScrollView?.setImage(image:UIImage.init(named: imageName!)!)
        showImageScrollView?.alpha = 0
        showImageScrollView?.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
        kAppDelegate.window?.addSubview(showImageScrollView!)
        showImageScrollView?.photoObject = self.photoObject
        UIView.animate(withDuration: 0.3, animations: {
            self.showImageScrollView?.alpha = 1
        } ,completion: { (done) in } )
    }
    
}

extension GalleryViewController :ShowImageScrollViewDelegate {
    
    func removeShowImageScrollView() {
        
        self.showImageScrollView?.alpha = 1
        UIView.animate(withDuration: 0.3, animations: {
            self.showImageScrollView?.alpha = 0
        } ,completion: { (done) in
            self.showImageScrollView?.removeFromSuperview()
            self.showImageScrollView = nil
        } )
    }
    
}

extension GalleryViewController : UICollectionViewDelegateFlowLayout {
    //1
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let itemSide: CGFloat = (collectionView.bounds.width - 1) / 3
        return CGSize(width: itemSide, height: itemSide)
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsetsMake(0, 0, 0, 0)
        
        // return UIEdgeInsetsMake(10, 10, 10, 10)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat{
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
        return 0
    }
}
