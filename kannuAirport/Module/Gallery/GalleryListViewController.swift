//
//  GalleryListViewController.swift
//  kannuAirport
//
//  Created by Arpana on 01/06/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit
import SwiftLoader

class GalleryListViewController: UIViewController {

        @IBOutlet weak var customNavigationBarForDrawer: CustomNavigationBarForDrawer!
        
        @IBOutlet weak var tableView: UITableView!
        
     var gallerydataArray : PhotoGallery? //Logo is an array
     private let refreshControl = UIRefreshControl()

    
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
        override func viewDidLoad() {
            
            super.viewDidLoad()
            setUPUI()
            getGallerImages()
            
        }
        override func viewWillAppear(_ animated: Bool) {
            
            super.viewWillAppear(animated)
        }
        
        func setUPUI(){
            
            customNavigationBarForDrawer.titleLabel.text =  "Photo Gallery"
            customNavigationBarForDrawer.senderController = self
            customNavigationBarForDrawer.leftSideMenuButtonItem.tag = ButtonTag.MENU.rawValue
            customNavigationBarForDrawer.leftSideMenuButtonItem.setImage(#imageLiteral(resourceName: "3bar"), for: .normal)
            tableView.refreshControl = refreshControl
            // Configure Refresh Control
            refreshControl.addTarget(self, action: #selector(refreshGalleryData(_:)), for: .valueChanged)
        }
        
    @objc private func refreshGalleryData(_ sender: Any) {
        // Fetch Weather Data
        getGallerImages()
    }
    //MARK:- Get GAllery Images
    func getGallerImages() {
        
         SwiftLoader.show(animated: true)
        
        weak var weakSelf = self

         APICall.callGalleryAPI([:], header: [:]) { (sucess, result, error) in
            
            DispatchQueue.main.async {
                
                 SwiftLoader.hide()
                
                if (error != nil){
                    
                    weakSelf?.showAlertWithAction(with: "", message: error?.localizedDescription)

                }
                if sucess {
                    
                    do {
                        let jsonDecoder = JSONDecoder()
                        
                        self.gallerydataArray = try  jsonDecoder.decode(PhotoGallery.self, from: result!) as PhotoGallery
                        self.tableView.reloadData()
                        
                    } catch  {
                        
                    }
                }
                    
                    
                else{
                    //Alert not sucessfull
                    self.showAlertWithAction(with: "Failed", message: "Some error occured")

                }
                if (weakSelf?.refreshControl.isRefreshing)!  {
                    
                    weakSelf?.refreshControl.endRefreshing()
                }
            }
        }
    }
       
        deinit {
            print(" \(String(describing: gallerydataArray)) is gallerydataArray deallocated")
        }
        
    }
    
    extension GalleryListViewController : UITableViewDelegate, UITableViewDataSource , UIViewControllerTransitioningDelegate{
        
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section:
            Int) -> Int
        {
            if let array = gallerydataArray {
                
                return array.count
                
            }
            return 0
        }
        
        
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
        {
       
            let cell =
                tableView.dequeueReusableCell(withIdentifier: "galleryListTableViewCellID",
                                              for: indexPath) as! galleryListTableViewCell
            
            if let val : PhotoGalleryElement = self.gallerydataArray?[indexPath.row] {
                
                
                if let fieldImage = val.fieldImage {
                    
                    cell.imgView.sd_imageIndicator?.startAnimatingIndicator()
                    cell.imgView.sd_setImage(with: URL(string:imageBaseUrl + fieldImage ), completed: { (image, error, type, url) in
                        if error == nil{
                            cell.imgView.image = image
                        }
                        cell.imgView.sd_imageIndicator?.stopAnimatingIndicator()
                    })
                }
                let titleString: NSMutableAttributedString = NSMutableAttributedString.init(string: "")
                
                if let title = val.title {
                    titleString.append(NSAttributedString(string: title))
                }
                if let delta = val.delta {
                    titleString.append(NSAttributedString(string: "\n"))
                    titleString.append(NSAttributedString(string:"(\(delta))"))

                }
                cell.lblTitle.attributedText = titleString
               
            }else{
                cell.imgView.image = nil
            }
            return cell
        }
        
        func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath){
            let delay : CGFloat =     2.0
            UIView.beginAnimations("transalation", context: nil)
            UIView.animate(withDuration: 5.0, delay:TimeInterval(delay), options: UIViewAnimationOptions.repeat, animations: {}, completion: nil)
            UIView.commitAnimations()
            
        }
        
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            
            return tableView.frame.size.height / 3
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
        {
            //Go to details page by passing nodeId
            if let val : PhotoGalleryElement = self.gallerydataArray?[indexPath.row] {
                
                guard let nodeNumber = val.nid else {
                    return
                }
                NavigationManager.moveToGalleryViewControllerIfNotExists(kAppDelegate.menuNavController!, nodeId: nodeNumber)
                
            }
        }
        
}

