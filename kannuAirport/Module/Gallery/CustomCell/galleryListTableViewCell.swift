//
//  galleryListTableViewCell.swift
//  kannuAirport
//
//  Created by Arpana on 01/06/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit

class galleryListTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
