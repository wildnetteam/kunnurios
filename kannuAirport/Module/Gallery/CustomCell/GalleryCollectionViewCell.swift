//
//  GalleryCollectionViewCell.swift
//  kannuAirport
//
//  Created by Arpana on 30/05/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit

class GalleryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
}
