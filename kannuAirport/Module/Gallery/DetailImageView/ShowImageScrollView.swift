//
//  ShowImageScrollView.swift
//  Bizzalley
//
//  Created by Anshul on 22/03/17.
//  Copyright © 2017 Anuj Jha. All rights reserved.
//

import UIKit
import SDWebImage

protocol ShowImageScrollViewDelegate {
    func removeShowImageScrollView()
}

class ShowImageScrollView: UIView {

    @IBOutlet weak var scrollView:UIScrollView!
    @IBOutlet weak var imageView:UIImageView!
    @IBOutlet weak var closeBtn:UIButton!
    var xPosition: CGFloat = 20.0
    var photoObject : PhotoGalleryDetails? //Logo is an array

    var delegate:ShowImageScrollViewDelegate?
    var image:UIImage?
    
    
    //MARK:- Intializer
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    override func draw(_ rect: CGRect) {
        super.draw(rect)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    func initSlideImages(imageArry: PhotoGalleryDetails?)  {
        
        
        if var imgArr = self.photoObject?.data
        {
            for index in 0...imgArr.count - 1{
                
                let adImageView = UIImageView()
                
                if let val = self.photoObject?.data[index]
                {
                    
                    adImageView.sd_imageIndicator?.startAnimatingIndicator()
                    adImageView.contentMode = .scaleAspectFit
                    adImageView.sd_setImage(with: URL(string: val.medium!), completed: { (image, error, type, url) in
                        if error == nil{
                            adImageView.image = image
                        }
                        adImageView.sd_imageIndicator?.stopAnimatingIndicator()
                    })
                    
                    scrollView.isPagingEnabled = true
                    adImageView.frame = CGRect(x:self.xPosition,y: 0,width: self.scrollView.frame.size.width - 40, height:self.scrollView.frame.size.height)
                    //  imageTap.frame = adImageView.frame
                    //  imageTap.addTarget(self, action: #selector(self.handleImageTap(_:)), for: .touchUpInside)
                    //  imageTap.tag = index
                    self.scrollView.addSubview(adImageView)
                    
                    scrollView.minimumZoomScale = 1.0;
                    
                    scrollView.maximumZoomScale = 6.0
                    // self.scrollView.addSubview(imageTap)
                    self.xPosition += self.frame.width
                    self.scrollView.contentSize = CGSize(width: CGFloat(index + 1) * self.scrollView.frame.size.width, height: self.scrollView.frame.size.height)
                }
            }
            let scrollWidth = self.scrollView.contentSize.width + self.scrollView.frame.size.width
            self.scrollView.contentSize = CGSize(width: scrollWidth, height: self.scrollView.frame.size.height)
        }
    }
        
    func setImage(image:UIImage){
        
        imageView.contentMode = UIViewContentMode.scaleAspectFit;
      //  scrollView.contentSize = imageView.frame.size
        scrollView.clipsToBounds = false
        scrollView.delegate = self
        
        let swipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(self.getSwipeAction(_:)))
        imageView.addGestureRecognizer(swipeGesture)
        imageView.image = image
    }
    @objc func getSwipeAction( _ recognizer : UISwipeGestureRecognizer){
        
        if recognizer.direction == .right{
            print("Right Swiped")
        } else if recognizer.direction == .left {
            print("Left Swiped")
        }
    }

    @IBAction func closeBtnTapped(_ sender: UIButton) {
        if delegate != nil {
            delegate?.removeShowImageScrollView()
        }
    }
    public func viewForZooming(in scrollView: UIScrollView) -> UIView?{
        return imageView
    }
}
extension ShowImageScrollView: UIScrollViewDelegate {
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        self.imageView.isHidden = true
        initSlideImages(imageArry: (self.photoObject ?? nil) )

    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print("scrollViewDidScroll")
    }
}
