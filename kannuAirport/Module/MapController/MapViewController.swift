//
//  MapViewController.swift
//  kannuAirport
//
//  Created by Arpana on 27/11/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class MapViewController: UIViewController {
    
    @IBOutlet weak var customNavigationBarForDrawer: CustomNavigationBarForDrawer!
    
    @IBOutlet weak var mapView: MKMapView!
    var myLocation:CLLocationCoordinate2D?
    //let locationManager = CLLocationManager()
    
    
    var nodeTitle :String?
    var isFromSideController: Bool = true
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        setUPUI()
        
    }
    
    func setUPUI(){
        
        customNavigationBarForDrawer.senderController = self
        customNavigationBarForDrawer.leftSideMenuButtonItem.tag = isFromSideController == true ? ButtonTag.MENU.rawValue :ButtonTag.BACK.rawValue
        customNavigationBarForDrawer.leftSideMenuButtonItem.setImage (isFromSideController == true ? #imageLiteral(resourceName: "3bar") : #imageLiteral(resourceName: "back"), for: .normal)
        
        
        mapView.mapType = MKMapType.standard
        // Ask for Authorisation from the User.
//        self.locationManager.requestAlwaysAuthorization()
//
//        // For use in foreground
//        self.locationManager.requestWhenInUseAuthorization()
//
//        if CLLocationManager.locationServicesEnabled() {
//            locationManager.delegate = self
//            locationManager.desiredAccuracy = kCLLocationAccuracyBest
//            locationManager.startUpdatingLocation()
//        }
        
        mapView.delegate = self
        mapView.isZoomEnabled = true
        mapView.isScrollEnabled = true
        
        // 2)
        let location = CLLocationCoordinate2D(latitude: 11.913401,longitude: 75.5488766)
        
        // 3)
        let span = MKCoordinateSpanMake(0.05, 0.05)
        let region = MKCoordinateRegion(center: location, span: span)
        mapView.setRegion(region, animated: true)
        
        // 4)
        let annotation = MKPointAnnotation()
        annotation.coordinate = location
        annotation.title = "Kannur International Airport"
        mapView.addAnnotation(annotation)
        mapView.selectAnnotation(annotation, animated: true)


        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customNavigationBarForDrawer.titleLabel.text = nodeTitle
        //mapView.showsUserLocation = true;
        
        
    }
    
}

extension MapViewController : MKMapViewDelegate  , CLLocationManagerDelegate{

    
    func resetTracking(){
        if (mapView.showsUserLocation){
            mapView.showsUserLocation = false
            self.mapView.removeAnnotations(mapView.annotations)
           // self.locationManager.stopUpdatingLocation()
        }
    }
    
    func centerMap(_ center:CLLocationCoordinate2D){
        self.saveCurrentLocation(center)
        
        let spanX = 0.007
        let spanY = 0.007
        
        let newRegion = MKCoordinateRegion(center:center , span: MKCoordinateSpanMake(spanX, spanY))
        mapView.setRegion(newRegion, animated: true)
    }
    
    func saveCurrentLocation(_ center:CLLocationCoordinate2D){
        let message = "\(center.latitude) , \(center.longitude)"
        print(message)
       // self.lable.text = message
        myLocation = center
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        
        centerMap(locValue)
    }
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView){
        
        mapView.selectAnnotation(view.annotation!, animated: false)

    }
    static var enable:Bool = true
    
    @IBAction func getMyLocation(_ sender: UIButton) {
        
        if CLLocationManager.locationServicesEnabled() {
            if MapViewController.enable {
               // locationManager.stopUpdatingHeading()
                sender.titleLabel?.text = "Enable"
            }else{
               // locationManager.startUpdatingLocation()
                sender.titleLabel?.text = "Disable"
            }
            MapViewController.enable = !MapViewController.enable
        }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView?{
        let identifier = "pin"
        var view : MKPinAnnotationView
        if let dequeueView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKPinAnnotationView{
            dequeueView.annotation = annotation
            view = dequeueView
        }else{
            view = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            view.canShowCallout = true
            view.calloutOffset = CGPoint(x: -5, y: 5)
            view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        }
        view.pinTintColor =  .red
        view.canShowCallout = true
        return view
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl){
        openMapForPlace()
    }
    
    func openMapForPlace() {
        
        
        if let coor = mapView.userLocation.location?.coordinate{
            mapView.setCenter(coor, animated: true)
        }
        
        let location = CLLocationCoordinate2D(latitude: 11.913401
            ,longitude: 75.5488766)
        
        
        //        let latitude: CLLocationDegrees = (mapView.userLocation.location?.coordinate.latitude)!
        //        let longitude: CLLocationDegrees = (mapView.userLocation.location?.coordinate.longitude)!
        
        let regionDistance:CLLocationDistance = 10000
        let coordinates = CLLocationCoordinate2DMake(location.latitude, location.longitude)
        let regionSpan = MKCoordinateRegionMakeWithDistance(coordinates, regionDistance, regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = "Kannur International Airport"
        mapItem.openInMaps(launchOptions: options)
    }
}


