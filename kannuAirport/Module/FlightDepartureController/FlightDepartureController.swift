//
//  FlightDepartureController.swift
//  kannuAirport
//
//  Created by Rajeev on 21/05/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit
import SwiftLoader
class FlightDepartureController: UIViewController {
    
    @IBOutlet weak var imgView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var tableView: UITableView!
    
    var departureArry : [NSDictionary]?
    var arrivelArry : [NSDictionary]?
    
    var arrivalViewM : [ArrivalDepartModel] = []
    var tempArrivalViewM : [ArrivalDepartModel] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imgView.layer.masksToBounds = false
        imgView.layer.shadowOpacity = 0.4 ;
        imgView.layer.shadowColor = UIColor.black.cgColor
        imgView.layer.shadowOffset = CGSize(width: 0.0, height: 4.0)
        getDepartureList()
        
    }
    
    func getDepartureArray()->Array<String>{
        var array = [String]()
        for item in tempArrivalViewM {
            array.append(item.destination!)
        }
        return array
    }
    
    func  filterDataForDestination (destination:String) {
        
        
        if destination !=  "" {
            
            
            var filteredArray = [ArrivalDepartModel]()
            for item in tempArrivalViewM {
                if (item.destination! == destination) {
                    filteredArray.append(item)
                }
            }
            
            self.arrivalViewM.removeAll()
            self.arrivalViewM = filteredArray
            
        }else{
            self.arrivalViewM = tempArrivalViewM
        }
        tableView.reloadData()
        
    }
    
    
    func getFlightLogo(forFlight:String)->String{
        if forFlight.contains("AI") {
            return "Air_india"
        }else if forFlight.contains("G8") {
            return "GoAir"
        }else if forFlight.contains("6E") {
            return "IndiGo"
        }else if forFlight.contains("IX") {
            return "Air_India_Express"
        }
        return ""
    }
    
    
    
    func  dataAfterAppliedFilter () {
        
        if kAppDelegate.filterText ==  "Both" {
            
            getDepartureList()
        }
        else if kAppDelegate.filterText !=  "" {
            
            var filteredArray = [ArrivalDepartModel]()
            for item in tempArrivalViewM {
                if (item.type == kAppDelegate.filterText ) {
                    filteredArray.append(item)
                }
            }
            
            
//            let newArray = self.arrivalViewM.filter({ (obj) -> Bool in
//                obj.type == "\(kAppDelegate.filterText)"
//            })
//
            //print("filteredArray :\(filteredArray)")
            
            for item in filteredArray {
                print("Dest--\(item.destination!)--type:\(String(item.type!))")
            }
            
            
            
            self.arrivalViewM.removeAll()
            self.arrivalViewM = filteredArray
            
        }
        tableView.reloadData()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
      
    }
    
    func searchArrivalWithFlightNumber(flightNumber:String){
        if flightNumber !=  "" {
            
            
            var filteredArray = [ArrivalDepartModel]()
            for item in tempArrivalViewM {
                if (item.flightCode!.contains(flightNumber)) {
                    filteredArray.append(item)
                }
            }
            
            self.arrivalViewM.removeAll()
            self.arrivalViewM = filteredArray
            
        }else{
            self.arrivalViewM = tempArrivalViewM
        }
        tableView.reloadData()
    }
    
    
    func getDepartureList()  {
        
        self.arrivalViewM.removeAll()
        if tableView != nil {
            self.tableView.showAnimatedSkeleton()
        }
         SwiftLoader.show(animated: true)
        
        APICall .callFlightDepartAPI([ : ], header: ["Authorization" : AuthorizationToken ]) { (sucess, result, error) in
            DispatchQueue.main.async {
                
                weak var weakSelf = self
                
                 SwiftLoader.hide()
                
                if (error != nil){
                    
                    self.showAlertWithAction(with: "", message: error?.localizedDescription)
                    
                }
                if sucess {
                    
                    do {
                        
                        
                        let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                        
                        if (jsonData is NSDictionary){
                            
                            let modelArray = ArrivalDepartModel.initModel(res: jsonData as! [String : Any])
                            
                            print(modelArray)
                            
                            self.arrivalViewM = modelArray
                            self.tempArrivalViewM = self.arrivalViewM
                        }
                        
                        weakSelf?.tableView.reloadData()
                        
                        
                    } catch  {
                        weakSelf?.showAlertWithAction(with: "", message: AlertMessages.MODELERRORMESSAGE)
                        
                    }
                }
                else {
                    //Alert not sucessfull
                    self.showAlertWithAction(with: "Failed", message: "Some error occured")
                    
                }
                //                if (weakSelf?.refreshControl.isRefreshing)!  {
                //
                //                    weakSelf?.refreshControl.endRefreshing()
                //                }
                if weakSelf?.tableView != nil {
                    weakSelf?.tableView.hideSkeleton()
                }
            }
        }
    }
    
}
extension FlightDepartureController : UITableViewDelegate, UITableViewDataSource , UIViewControllerTransitioningDelegate{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section:
        Int) -> Int
    {
        if let array = self.arrivalViewM  as? [ArrivalDepartModel] {
            
            return array.count
            
        }
        return 0
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        // Allocates a Table View Cell
        let cell =
            tableView.dequeueReusableCell(withIdentifier: "arrivalDepartureCell",
                                          for: indexPath) as! ArrivalDepartureTableCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        if indexPath.row < arrivalViewM.count {
            
            if let model = self.arrivalViewM[indexPath.row] as? ArrivalDepartModel {
                print(model)
                let flightCode = model.flightCode
                let flightStatus = model.status
                let flightTime = model.etd
                let flighImg = getFlightLogo(forFlight: flightCode!)
                
//                if flighImg != ""{
//                    cell.flighImgView?.image = UIImage(named: flighImg)
//                }
                cell.flighImgView?.image = UIImage(named: flighImg)

                cell.flightNumberLbl?.text = flightCode
                
                if flightStatus == "Departed"{
                    cell.fligthStatusLbl?.textColor = UIColor.yellow
                }else if flightStatus == "Delayed"{
                    cell.fligthStatusLbl?.textColor = UIColor.red
                }else if flightStatus == "Rescheduled"{
                    cell.fligthStatusLbl?.textColor = UIColor.red
                }else if flightStatus == "Gate Closed"{
                    cell.fligthStatusLbl?.textColor = UIColor.red
                }else if flightStatus == "Boarding Final"{
                    cell.fligthStatusLbl?.textColor = UIColor.yellow
                }else if flightStatus == "Boarding"{
                    cell.fligthStatusLbl?.textColor = UIColor.green
                }else if flightStatus == "Scheduled"{
                    cell.fligthStatusLbl?.textColor = UIColor.white
                }else if flightStatus == "Check in"{
                    cell.fligthStatusLbl?.textColor = UIColor.white
                }else{
                    cell.fligthStatusLbl?.textColor = UIColor.white
                }
                
                
                cell.fligthStatusLbl?.text = flightStatus
                cell.fromLablCode?.text = "CNN"
                cell.toLablCode?.text = model.destination
                cell.toLablFull?.text = "Kannur"
                if model.gate == ""{
                    cell.flightGateLbl?.text = "Counter - NA"
                }else{
                    cell.flightGateLbl?.text = "Counter \(model.counter!)"
                    
                }
                
                cell.fligthDateLbl?.text = model.flightDate
                if let stdTime = model.std {
                   cell.flighTimeLbl?.text = "STD \(stdTime)"
                }else{
                    cell.flighTimeLbl?.text = "STD NA"
                }
                
                cell.flightTime2Lbl?.text = "ETD \(flightTime!)"
                // cell.setData(arriavlModel: model)
            }
        }
        
        return cell
    }
    
    //    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath){
    //        let delay : CGFloat =     1.0
    //        UIView.beginAnimations("transalation", context: nil)
    //        UIView.animate(withDuration: 5.0, delay:TimeInterval(delay), options: UIViewAnimationOptions.repeat, animations: {}, completion: nil)
    //        UIView.commitAnimations()
    //
    //    }
    
    
    //    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    //
    //        return heightForHeader
    //    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        
    }
    
    
    
}
    

