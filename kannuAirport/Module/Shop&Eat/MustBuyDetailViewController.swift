//
//  MustBuyDetailViewController.swift
//  kannuAirport
//
//  Created by Arpana on 05/07/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit
import SkeletonView
class MustBuyDetailViewController: UIViewController {

 @IBOutlet weak var tableView_Header: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var imgViewHeader: UIImageView!
    let imageView = UIImageView()
    let backButton = UIButton()
    var nodeID  : String? // to fetch detials with particular nodeID
    var tableheaderHeight : CGFloat = 300
    private let refreshControl = UIRefreshControl()
    var shopObject : MustBuy? //Shops is an array
    override func viewDidLoad() {
        super.viewDidLoad()
        getShopNameList()

        setUPUI()
        
    }
    func setUPUI(){
        tableView.estimatedRowHeight = 50
        tableView.contentInset = UIEdgeInsetsMake(-20, 0, 0, 0)
        tableView.refreshControl = refreshControl
        tableView.isSkeletonable = true
        // Configure Refresh Control
        refreshControl.addTarget(self, action: #selector(refreshShopData(_:)), for: .valueChanged)
      //  self.tableView.tableHeaderView = tableView_Header
        tableView.contentInset = UIEdgeInsetsMake(300, 0, 0, 0)
        
        
        imageView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 300)
        imageView.image = #imageLiteral(resourceName: "Bitmap-1")
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        backButton.frame = CGRect(x: 0, y: 0, width: 60, height: 60)
        backButton.setImage(#imageLiteral(resourceName: "back"), for: .normal)
        backButton.setBackgroundImage(#imageLiteral(resourceName: "shadow"), for: .normal)
        backButton.addTarget(self, action: #selector(backButtonPressed(_:)), for: .touchUpInside)
        view.addSubview(imageView)
        imageView.addSubview(backButton)

        tableView.setNeedsUpdateConstraints()
        tableView.layoutIfNeeded()
    }
    @objc private func refreshShopData(_ sender: Any) {
        // Fetch Weather Data
        getShopNameList()
    }
   
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //MARK:- Get shop names for the category
    func getShopNameList() {
        
        
        if tableView != nil {
            self.tableView.showAnimatedSkeleton()
        }
        
        APICall.callMustBuyDetailsAPI([:], header: [:] ,idToFetchDetails: nodeID!)  { (sucess, result, error) in
            
            DispatchQueue.main.async {
                
                weak var weakSelf = self
                
                
                if (error != nil){
                    
                    weakSelf?.showAlertWithAction(with: "", message: error?.localizedDescription)
                    return
                    
                }
                if sucess {
                    
                    do {
                        let jsonDecoder = JSONDecoder()
                        
                        
                        weakSelf?.shopObject = try  jsonDecoder.decode(MustBuy.self, from: result!) as MustBuy
                        if let fieldImage = weakSelf?.shopObject![0].fieldImage as? String {
                        
                        weakSelf?.updateheader(url: fieldImage)
                        }
                        self.tableView.reloadData()
                        
                        
                    } catch  {
                        
                        weakSelf?.showAlertWithAction(with: "", message: AlertMessages.MODELERRORMESSAGE)

                    }
                }
                    
                else{
                    //Alert not sucessfull
                    weakSelf?.showAlertWithAction(with: "", message: AlertMessages.UnidetifiedErrorMessage)

                }
                if (weakSelf?.refreshControl.isRefreshing)!  {
                    
                    weakSelf?.refreshControl.endRefreshing()
                }
                
                if weakSelf?.tableView != nil {
                    weakSelf?.tableView.hideSkeleton()
                }
            }
        }
    }
  
    func updateheader(url:String)  {
        
        imageView.sd_imageIndicator?.startAnimatingIndicator()
        imageView.sd_setImage(with: URL(string: url.trimmingCharacters(in: .whitespacesAndNewlines)), completed: { (image, error, type, url) in
            if error == nil{
                self.imageView.image = image
            }
        })
    }
}




extension MustBuyDetailViewController: UITableViewDelegate, UITableViewDataSource, SkeletonTableViewDataSource , UIViewControllerTransitioningDelegate{
    
    func collectionSkeletonView(_ skeletonView: UITableView, cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return "ShopDetailTableViewCellID"
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if let array = shopObject {

            if array.count == 0 {
                self.tableView.setEmptyMessage("Sorry, No Shops Found.")
            } else {
                self.tableView.restore()
            }
            return array.count

        }
        return 10
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        tableView.setNeedsDisplay()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.bounds.size.height - 400
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Allocates a Table View Cell
        let cell =
            tableView.dequeueReusableCell(withIdentifier: "ShopDetailTableViewCellID",
                                          for: indexPath) as! ShopDetailTableViewCell
      
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.backGroundWhiteView.layer.cornerRadius = (cell.backGroundWhiteView.frame.size.width * 0.02)
        cell.backGroundWhiteView.layer.masksToBounds = true
        cell.shadowView.layer.shadowOpacity = 1 ;
        cell.shadowView.layer.shadowColor = UIColor(red:0, green:0, blue:0, alpha:0.1).cgColor
        cell.shadowView.layer.shadowOffset = CGSize(width: 0.0, height: 10.0)

        
        if let val : MustBuyElement = self.shopObject?[indexPath.row] {
            
           
            if let title = val.title {
                cell.lblHeading.text = title
            }
//            if let details = val.body as? String {
//                cell.lblDescription.text = details.withoutHtml
//
//            }

        }
        return cell
    
    }
    
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//
//        let v = UIView()
//        v.frame = CGRect(x: 0, y: 0, width: tableView.frame.width, height: 160)
//        v.backgroundColor = .black
//        v.alpha = 0.5
//        let titlelabel = UILabel(frame: CGRect(x: 10, y: 0, width: tableView.frame.width - 20, height: 60))
//        titlelabel.text = "An Euphoric, Aquatic and cool..."
//        titlelabel.textColor = .white
//        v.addSubview(titlelabel)
//
//        return v
//    }
//
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 60.0
//    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let y = 300 - (scrollView.contentOffset.y + 300)
        let height = min(max(y, 60), 400)
        imageView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: height)


    }

}
