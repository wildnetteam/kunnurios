//
//  MustBuyViewController.swift
//  kannuAirport
//
//  Created by Arpana on 02/07/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit
import SkeletonView
class MustBuyViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    private let refreshControl = UIRefreshControl()
    @IBOutlet weak var shadowView: UIView!
    var nodeID  : String?
    var shopObject : MustBuy? //Shops is an array

    override func viewDidLoad() {
        super.viewDidLoad()
        setUPUI()
        getShopNameList()
    }

    func setUPUI()  {
        
        shadowView.layer.masksToBounds = false
        shadowView.layer.shadowOpacity = 0.4 ;
        shadowView.layer.shadowColor = UIColor.black.cgColor
        shadowView.layer.shadowOffset = CGSize(width: 0.0, height: 4.0)
        tableView.refreshControl = refreshControl
        tableView.isSkeletonable = true
        // Configure Refresh Control
        refreshControl.addTarget(self, action: #selector(refreshShopData(_:)), for: .valueChanged)
    }
    
    @objc private func refreshShopData(_ sender: Any) {
        // Fetch Weather Data
        getShopNameList()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Get shop names for the category
    func getShopNameList() {
        
        
        if tableView != nil {
            self.tableView.showAnimatedSkeleton()
        }
        
        APICall.callMustBuyAPI([:], header: [:] ,idToFetchDetails: nodeID!)  { (sucess, result, error) in
            
            DispatchQueue.main.async {
                
                weak var weakSelf = self
                
                
                if (error != nil){
                    
                    weakSelf?.showAlertWithAction(with: "", message: error?.localizedDescription)
                    return
                    
                }
                if sucess {
                    
                    do {
                        let jsonDecoder = JSONDecoder()
                        
                        
                        weakSelf?.shopObject = try  jsonDecoder.decode(MustBuy.self, from: result!) as MustBuy
                        
                        self.tableView.reloadData()
                        
                    } catch  {
                        
                        weakSelf?.showAlertWithAction(with: "", message: AlertMessages.MODELERRORMESSAGE)
                    }
                }
                    
                else{
                    //Alert not sucessfull
                    weakSelf?.showAlertWithAction(with: "", message: AlertMessages.UnidetifiedErrorMessage)

                }
                if (weakSelf?.refreshControl.isRefreshing)!  {
                    
                    weakSelf?.refreshControl.endRefreshing()
                }
           
                if weakSelf?.tableView != nil {
                    weakSelf?.tableView.hideSkeleton()
                }
            }
        }
    }

}

extension MustBuyViewController : UITableViewDelegate, UITableViewDataSource , SkeletonTableViewDataSource , UIViewControllerTransitioningDelegate{
    func collectionSkeletonView(_ skeletonView: UITableView, cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return "ShopDetailTableViewCellID"
    }
    

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        if let array = shopObject {
            
            if array.count == 0 {
                self.tableView.setEmptyMessage("Sorry, No Shops Found.")
            } else {
                self.tableView.restore()
            }
            return array.count
            
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        // Allocates a Table View Cell
        let cell =
            tableView.dequeueReusableCell(withIdentifier: "ShopDetailTableViewCellID",
                                          for: indexPath) as! ShopDetailTableViewCell
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.backGroundWhiteView.layer.cornerRadius = (cell.backGroundWhiteView.frame.size.width * 0.02)
        cell.backGroundWhiteView.layer.masksToBounds = true
        cell.shadowView.layer.shadowOpacity = 1 ;
        cell.shadowView.layer.shadowColor = UIColor(red:0, green:0, blue:0, alpha:0.1).cgColor
        cell.shadowView.layer.shadowOffset = CGSize(width: 0.0, height: 10.0)
        
        
        if let val : MustBuyElement = self.shopObject?[indexPath.row] {
            
            
            if let fieldImage = val.fieldImage {
                cell.imgView.sd_imageIndicator?.startAnimatingIndicator()
                cell.imgView.sd_setImage(with: URL(string: fieldImage.trimmingCharacters(in: .whitespacesAndNewlines)), completed: { (image, error, type, url) in
                    if error == nil{
                        cell.imgView.image = image
                    }
                    cell.imgView.sd_imageIndicator?.stopAnimatingIndicator()
                })
            }
            if let title = val.title {
                cell.lblHeading.text = title
            }
            if let details = val.body {
              //  cell.lblDescription.text = details.withoutHtml
                
            }
            
        }else{
            cell.imgView.image = nil
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath){
        let delay : CGFloat =     1.0
        UIView.beginAnimations("transalation", context: nil)
        UIView.animate(withDuration: 5.0, delay:TimeInterval(delay), options: UIViewAnimationOptions.repeat, animations: {}, completion: nil)
        UIView.commitAnimations()
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return self.tableView.frame.size.height/4
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
       // NavigationManager.moveToMustBuyDetailViewControllerIfNotExists(kAppDelegate.menuNavController!, nodeId: "1")
    }
    
}

