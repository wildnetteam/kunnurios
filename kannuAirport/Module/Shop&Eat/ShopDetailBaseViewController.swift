//
//  ShopDetailBaseViewController.swift
//  kannuAirport
//
//  Created by Arpana on 26/06/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit

class ShopDetailBaseViewController: UIViewController {

  //  @IBOutlet weak var gskView : GSKStretchyHeaderView
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        // you can change wether it expands at the top or as soon as you scroll down
//        gskView.expansionMode = .immediate
//        
//        // You can change the minimum and maximum content heights
//        gskView.minimumContentHeight = 64 // you can replace the navigation bar with a stretchy header view
//        gskView.maximumContentHeight = 280
//        
//        // You can specify if the content expands when the table view bounces, and if it shrinks if contentView.height < maximumContentHeight. This is specially convenient if you use auto layout inside the stretchy header view
//        gskView.contentShrinks = true
//        gskView.contentExpands = false // useful if you want to display the refreshControl below the header view
//        
//        // You can specify wether the content view sticks to the top or the bottom of the header view if one of the previous properties is set to `false`
//        // In this case, when the user bounces the scrollView, the content will keep its height and will stick to the bottom of the header view
//        gskView.contentAnchor = .bottom
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
