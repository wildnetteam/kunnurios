//
//  ShopAndEatBaseViewController.swift
//  kannuAirport
//
//  Created by Arpana on 26/06/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit

class ShopAndEatBaseViewController: UIViewController , CAPSPageMenuDelegate , UIScrollViewDelegate{

    @IBOutlet weak var customNavigationBarForDrawer: UIView!
    var pageMenu : CAPSPageMenu?
    var shopID  : String? // to fetch detials with particular nodeID
    var shopDetails: ShopNameElement?
    var layoutArtistHeight: NSLayoutConstraint!
    var headerImageViewHeight : CGFloat?
    @IBOutlet  var imgMultiplierConstant: NSLayoutConstraint!
    @IBOutlet var lblHeadingTitle: UILabel!
    
    @IBOutlet weak var imageViewHeader: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        layoutArtistHeight = imgMultiplierConstant //Store actual height and multiplier for customNavigationBarForDrawer
        headerImageViewHeight = self.imageViewHeader.frame.minY + self.imageViewHeader.frame.size.height ////Store actual height of image View
        addPageMenuForPrimaryManager()
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let shopDataTodisplay = shopDetails {
            
             lblHeadingTitle.text = shopDataTodisplay.name ??  "Shop"
            
            imageViewHeader.sd_imageIndicator?.startAnimatingIndicator()
            if let imgURl = shopDataTodisplay.fieldStoreImage {
               imageViewHeader.sd_setImage(with: URL(string: imgURl), completed: { (image, error, type, url) in
                    if error == nil{
                        self.imageViewHeader.image = image
                    }
                self.imageViewHeader.sd_imageIndicator?.stopAnimatingIndicator()
                })
            }
            
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - ADD PAGE MENU PRIMARY
    func addPageMenuForPrimaryManager() {
        var controllerArray : [UIViewController] = []
        
        let shopCategoriesViewController = UIStoryboard.AboutUsViewController()
        shopCategoriesViewController.title = "About"
        shopCategoriesViewController.accessibilityLabel = shopDetails?.name ?? "Comming soon"
        controllerArray.append(shopCategoriesViewController)
        
        let shopViewController = UIStoryboard.mustBuyViewController()
        shopViewController.nodeID = shopID
        shopViewController.title = "Must Buy"
        controllerArray.append(shopViewController)
        
        let offerViewController = UIStoryboard.OffersViewController()
        offerViewController.title = "Offers"
        controllerArray.append(offerViewController)
        
        
        let dictionary:NSDictionary = [
            CAPSPageMenuOptionScrollMenuBackgroundColor : ColorConstants.appBackGroundCOLOR()
            ,   CAPSPageMenuOptionViewBackgroundColor : UIColor.clear,CAPSPageMenuOptionMenuItemWidth : self.view.frame.size.width/2 - 40 ,CAPSPageMenuOptionMenuHeight: IS_IPHONE_X ?55 : customNavigationBarForDrawer.frame.maxY - imageViewHeader.frame.maxY  , CAPSPageMenuOptionCenterMenuItems: true ,CAPSPageMenuOptionMenuItemFont: UIFont(name: "Karla-Bold", size: 16.0)!
        ]
        
        if (pageMenu != nil) {
            pageMenu?.view.removeFromSuperview()
        }
        
        let frame = CGRect(x:0.0,y:IS_IPHONE_X ? imageViewHeader.frame.maxY + 12: imageViewHeader.frame.maxY , width:self.view.frame.size.width,height:self.view.frame.size.height)
        pageMenu = CAPSPageMenu.init(viewControllers: controllerArray, frame: frame, options: dictionary as? [AnyHashable: Any] ?? [:])
        
        pageMenu!.delegate = self
        self.view.addSubview(pageMenu!.view)
       // (shopCategoriesViewController.scrollView as UIScrollView).delegate = self
    }
    func didMove(toPage controller: UIViewController!, index: Int) {
        print(controller)
        switch index {
        case 0:  print("Need to implement")//  ((controller as! FlightDepartureController).scrollView as UIScrollView).delegate = self
            
        case 1: print("Need to implement")
         //   ((controller as! MustBuyViewController).tableView as UIScrollView).delegate = self
            
        case 2: print("Need to implement")
        default: print("Wrong index")
            
        }
        viewDidLayoutSubviews()
    }
    override func viewDidLayoutSubviews() {

        var frame :CGRect
        if #available(iOS 11.0, *) {
               frame = CGRect(x:0.0,y:IS_IPHONE_X ? imageViewHeader.frame.maxY + 12: imageViewHeader.frame.maxY , width:self.view.frame.size.width,height:self.view.frame.size.height)
        } else {
            // Fallback on earlier versions
            frame = CGRect(x:0.0,y: imageViewHeader.frame.maxY , width:self.view.frame.size.width,height:self.view.frame.size.height)
            
        }
        pageMenu?.view.frame = frame
        view.layoutIfNeeded()
    }
    
    // we set a variable to hold the contentOffSet before scroll view scrolls
    var lastContentOffset: CGFloat = 0
    
    // this delegate is called when the scrollView (i.e your UITableView) will start scrolling
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
//        self.lastContentOffset =  scrollView.contentOffset.y
//        if (self.lastContentOffset == 0.0){
//            UIView.animate(withDuration: 1.0) {
//                let newConstraint = self.imgMultiplierConstant.constraintWithMultiplier(0.5)
//                self.view!.removeConstraint(self.imgMultiplierConstant)
//                if self.view.constraints.contains(self.layoutArtistHeight){
//                    self.view!.removeConstraint(self.layoutArtistHeight)
//                }
//                self.imgMultiplierConstant = (newConstraint as NSLayoutConstraint) //make imageview hide slowly and move CAPs page on top
//                self.view!.addConstraint ( self.imgMultiplierConstant)
//                self.imageViewHeader.frame = CGRect(x: self.imageViewHeader.frame.origin.x, y: self.imageViewHeader.frame.origin.y, width: self.imageViewHeader.frame.size.width, height: 0.0)
//                self.view!.layoutIfNeeded()
//            }
//        }
    }
    
    // while scrolling this delegate is being called so you may now check which direction your scrollView is being scrolled to
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        if (self.lastContentOffset == 0.0) {
//
//
//        } else if (self.lastContentOffset > scrollView.contentOffset.y) {
//            print(" moved to bottom")
//
//            UIView.animate(withDuration: 0.5) {
//                if self.imgMultiplierConstant != nil {
//                    if  self.view.constraints.contains(self.imgMultiplierConstant){
//                        self.view!.removeConstraint(self.imgMultiplierConstant)
//                        self.view!.addConstraint ( self.layoutArtistHeight)
//
//                        //Reset all views frame
//                        self.imageViewHeader.frame = CGRect(x: self.imageViewHeader.frame.origin.x, y: self.customNavigationBarForDrawer.frame.origin.y, width: self.imageViewHeader.frame.size.width, height: self.headerImageViewHeight!)
//
//                    }
//                    self.viewDidLayoutSubviews()
//                }
//            }
//        } else {
//            // didn't move
//        }
    }
    @IBAction func menuButtonTapped(_ sender: Any) {
        
        if  isMenuItemRetrieved == true {
            
            kAppDelegate.drawerController.setDrawerState(.opened, animated: true)
            
        }
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
}
