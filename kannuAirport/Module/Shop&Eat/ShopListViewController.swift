//
//  ShopListViewController.swift
//  kannuAirport
//
//  Created by Arpana on 13/06/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit
import SwiftLoader

class ShopListViewController: UIViewController {

    @IBOutlet weak var customNavigationBarForDrawer: CustomNavigationBarForDrawer!

    @IBOutlet weak var collectionViewCategory: UICollectionView!
    
    var categoriesArray : ShopCategory? //Logo is an array

    private let refreshControl = UIRefreshControl()


    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUPUI()
        getShopCategories()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    
    }
    
    func setUPUI()  {
        
        customNavigationBarForDrawer.titleLabel.text = "Shop & Eat"
        customNavigationBarForDrawer.senderController = self
        customNavigationBarForDrawer.leftSideMenuButtonItem.tag = ButtonTag.BACK.rawValue
        customNavigationBarForDrawer.leftSideMenuButtonItem.setImage(#imageLiteral(resourceName: "back"), for: .normal)
        collectionViewCategory.refreshControl = refreshControl
        // Configure Refresh Control
        refreshControl.addTarget(self, action: #selector(refreshCategoryData(_:)), for: .valueChanged)
    }
    
    @objc private func refreshCategoryData(_ sender: Any) {
        // Fetch Weather Data
        getShopCategories()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK:- Get GAllery Images
    func getShopCategories() {
        
        SwiftLoader.show(animated: true)
        
        APICall.callCategoriesAPI([:], header: [:]) { (sucess, result, error) in
            
            weak var weakSelf = self

            DispatchQueue.main.async {
                
                SwiftLoader.hide()
                
                if (error != nil){
                    
                    self.showAlertWithAction(with: "", message: error?.localizedDescription)
                    
                }
                if sucess {
                    
                    do {
                        let jsonDecoder = JSONDecoder()
                        
                        weakSelf?.categoriesArray = try  jsonDecoder.decode(ShopCategory.self, from: result!) as ShopCategory
                        weakSelf?.collectionViewCategory.reloadData()
                        
                    } catch  {
                        weakSelf?.showAlertWithAction(with: "", message: AlertMessages.MODELERRORMESSAGE)

                    }
                }
                    
                    
                else{
                    //Alert not sucessfull
                    self.showAlertWithAction(with: "Failed", message: "Some error occured")

                }
                if (weakSelf?.refreshControl.isRefreshing)!  {
                    
                    weakSelf?.refreshControl.endRefreshing()
                }
            }
        }
    }
    
    @IBAction func backBtnTapped(sender:UIButton){
        
        self.navigationController?.popViewController(animated: true)
    }
}


extension ShopListViewController: UICollectionViewDelegate , UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if let array = categoriesArray {
            
            return array.count
            
        }
        return 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ShopListCollectionViewCellID", for: indexPath) as! ShopListCollectionViewCell
        
        if let val : ShopCategoryElement = self.categoriesArray?[indexPath.row] {
            
            cell.lblTitle.text = val.name
            cell.imgView.sd_imageIndicator?.startAnimatingIndicator()
            cell.imgView.sd_setImage(with: URL(string: val.fieldShopCategoryImage!), completed: { (image, error, type, url) in
                if error == nil{
                    cell.imgView.image = image
                }
                cell.imgView.sd_imageIndicator?.stopAnimatingIndicator()
            })
        }else{
            cell.imgView.image = nil
        }

//        if(indexPath.row ==  (photoObject?.data.count)! - 1 && CGFloat(pageNumber) <= ceil(CGFloat((photoObject?.allRecode)!/10))){// i.e 10 < total -> 1288/10
//            //we are at last index of currently displayed cell
//            //reload more data
//            getGalleryDetails()
//
//        }
        
      
        cell.shodowView.layer.cornerRadius = (cell.shodowView.frame.size.width * 0.04)
        cell.shodowView.layer.masksToBounds = true
        cell.backGroundWhiteView.layer.shadowOpacity = 1 ;
        cell.backGroundWhiteView.layer.shadowColor = UIColor(red:0, green:0, blue:0, alpha:0.1).cgColor
        cell.backGroundWhiteView.layer.shadowOffset = CGSize(width: 0.0, height: 10.0)
        
        return cell
        
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        //        VDSAnimation.shared.setupAnimation(in: self.view, imageView: cell.imgView)
        if let val : ShopCategoryElement = self.categoriesArray?[indexPath.row] {
            
            guard let nodeNumber = val.tid else {
                return
            }
            NavigationManager.moveToShopDetailViewControllerIfNotExists(kAppDelegate.menuNavController!, nodeId: nodeNumber , title: val.name ?? "" )
            
        }
    }
    
}

extension ShopListViewController : UICollectionViewDelegateFlowLayout {
    //1
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let itemSide: CGFloat = (collectionView.bounds.width - 10) / 2
        return CGSize(width: itemSide, height: itemSide )
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsetsMake(0, 0, 0, 0)
        
        // return UIEdgeInsetsMake(10, 10, 10, 10)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat{
        
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
        return 10
    }
}
