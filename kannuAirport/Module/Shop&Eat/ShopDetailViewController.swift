//
//  ShopDetailViewController.swift
//  kannuAirport
//
//  Created by Arpana on 14/06/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit
import SwiftLoader

class ShopDetailViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var customNavigationBarForDrawer: CustomNavigationBarForDrawer!
    private let refreshControl = UIRefreshControl()

    var nodeID  : String? // to fetch detials with particular nodeID
    var nodeTitle: String?
    var shopObject : ShopName? //Logo is an array

    override func viewDidLoad() {
        super.viewDidLoad()
        setUPUI()
        getShopNameList()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

    }
    
    func setUPUI()  {
        
        customNavigationBarForDrawer.titleLabel.text = nodeTitle
        customNavigationBarForDrawer.senderController = self
        customNavigationBarForDrawer.leftSideMenuButtonItem.tag = ButtonTag.BACK.rawValue
        customNavigationBarForDrawer.leftSideMenuButtonItem.setImage(#imageLiteral(resourceName: "back"), for: .normal)
        tableView.refreshControl = refreshControl
        // Configure Refresh Control
        refreshControl.addTarget(self, action: #selector(refreshShopData(_:)), for: .valueChanged)
    }
    
    @objc private func refreshShopData(_ sender: Any) {
        // Fetch Weather Data
        getShopNameList()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Get shop names for the category
    func getShopNameList() {
        
        SwiftLoader.show(animated: true)
                
        APICall.callShopAPI([:], header: [:] ,idToFetchDetails: nodeID!)  { (sucess, result, error) in
            
            DispatchQueue.main.async {
                
                weak var weakSelf = self
                
                SwiftLoader.hide()
                
                if (error != nil){
                    
                    weakSelf?.showAlertWithAction(with: "", message: error?.localizedDescription)
                    
                }
                if sucess {
                    
                    do {
                        let jsonDecoder = JSONDecoder()
                     
                            
                            weakSelf?.shopObject = try  jsonDecoder.decode(ShopName.self, from: result!) as ShopName
                        
                        self.tableView.reloadData()
                        
                    } catch  {
                        
                        weakSelf?.showAlertWithAction(with: "", message: AlertMessages.MODELERRORMESSAGE)
                    }
                }
                    
                else{
                    //Alert not sucessfull
                    self.showAlertWithAction(with: "Failed", message: "Some error occured")

                }
                if (weakSelf?.refreshControl.isRefreshing)!  {
                    
                    weakSelf?.refreshControl.endRefreshing()
                }

            }
        }
    }
    
}

extension ShopDetailViewController : UITableViewDelegate, UITableViewDataSource , UIViewControllerTransitioningDelegate{
    
    
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section:
//        Int) -> Int
//    {
////        if let array = shopObject {
////
////            return array.count
////
////        }
////        return 0
//    }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       

        if let array = shopObject {
            
            if array.count == 0 {
                self.tableView.setEmptyMessage("Sorry, No Shops Found.")
            } else {
                self.tableView.restore()
            }
            return array.count
            
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        // Allocates a Table View Cell
        let cell =
            tableView.dequeueReusableCell(withIdentifier: "ShopDetailTableViewCellID",
                                          for: indexPath) as! ShopDetailTableViewCell
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.backGroundWhiteView.layer.cornerRadius = (cell.backGroundWhiteView.frame.size.width * 0.02)
        cell.backGroundWhiteView.layer.masksToBounds = true
        cell.shadowView.layer.shadowOpacity = 1 ;
        cell.shadowView.layer.shadowColor = UIColor(red:0, green:0, blue:0, alpha:0.1).cgColor
        cell.shadowView.layer.shadowOffset = CGSize(width: 0.0, height: 10.0)
        
        
        if let val : ShopNameElement = self.shopObject?[indexPath.row] {
            
            cell.imgView.sd_imageIndicator?.startAnimatingIndicator()
            if let imgURl = val.fieldStoreImage {
                cell.imgView.sd_setImage(with: URL(string: imgURl), completed: { (image, error, type, url) in
                    if error == nil{
                        cell.imgView.image = image
                    }
                    cell.imgView.sd_imageIndicator?.stopAnimatingIndicator()
                })
            }
            if let title = val.name {
               cell.lblHeading.text = title
            }
            if let details = val.fieldStoreDetails {
           //    cell.lblDescription.text = details
                
            }
            
        }else{
            cell.imgView.image = nil
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath){
        let delay : CGFloat =     1.0
        UIView.beginAnimations("transalation", context: nil)
        UIView.animate(withDuration: 5.0, delay:TimeInterval(delay), options: UIViewAnimationOptions.repeat, animations: {}, completion: nil)
        UIView.commitAnimations()
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return self.tableView.frame.size.height/4
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if let val : ShopNameElement = self.shopObject?[indexPath.row] {
            NavigationManager.moveToShopAndEatBViewControllerIfNotExists(kAppDelegate.menuNavController! ,nodeId: val.storeID!  , shopObject: val )
        }
    }
    
}
