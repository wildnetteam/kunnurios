//
//  ShopListCollectionViewCell.swift
//  kannuAirport
//
//  Created by Arpana on 13/06/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit

class ShopListCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var backGroundWhiteView: UIView!

    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var shodowView: UIView!
    
    @IBOutlet weak var lblTitle: UILabel!
}
