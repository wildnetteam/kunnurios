//
//  ShopDetailTableViewCell.swift
//  kannuAirport
//
//  Created by Arpana on 14/06/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit

class ShopDetailTableViewCell: UITableViewCell {


    @IBOutlet weak var shadowView: UIView!
    
    @IBOutlet weak var backGroundWhiteView: UIView!
    
    @IBOutlet weak var imgView: UIImageView!

    @IBOutlet weak var lblHeading: UILabel!
    
    
    @IBOutlet weak var lblRating: UILabel!
    
  //  @IBOutlet weak var lblDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
