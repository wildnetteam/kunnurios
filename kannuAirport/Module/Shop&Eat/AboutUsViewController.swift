//
//  AboutUsViewController.swift
//  kannuAirport
//
//  Created by Arpana on 27/11/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit

class AboutUsViewController: UIViewController {

    @IBOutlet weak var descrLbel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        descrLbel.text = accessibilityLabel
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
