//
//  FlightArrivalController.swift
//  kannuAirport
//
//  Created by Rajeev on 21/05/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit

class FlightArrivalController: UIViewController {

    @IBOutlet weak var label: UILabel!
    
    // a few constants that identify what element names we're looking for inside the XML
    
    // a few constants that identify what element names we're looking for inside the XML
    
    let recordKey = "record"
    let dictionaryKeys = Set<String>(["Status", "Type", "FlightDate"])
    
    // a few variables to hold the results as we parse the XML
    
    var results: [[String: String]]?         // the whole array of dictionaries
    var currentDictionary: [String: String]? // the current dictionary
    var currentValue: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
 
        self.label.text = "asfsfgsfg"
        
    }

    func getXMLDocFromURL(){
        
        if let url = URL(fileURLWithPath: "http://58.84.23.95:8080/flightData.xml") as? URL{
            let task = URLSession.shared.dataTask(with: url) { data, response, error in
                guard let data = data, error == nil else {
                    print(error ?? "Unknown error")
                    return
                }
                
                let parser = XMLParser(data: data)
                parser.delegate = self
                if parser.parse() {
                    print(self.results ?? "No results")
                }
            }
            task.resume()
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
extension FlightArrivalController: XMLParserDelegate {

    // initialize results structure
    
    func parserDidStartDocument(_ parser: XMLParser) {
        results = []
    }
    
    // start element
    //
    // - If we're starting a "record" create the dictionary that will hold the results
    // - If we're starting one of our dictionary keys, initialize `currentValue` (otherwise leave `nil`)
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        if elementName == recordKey {
            currentDictionary = [:]
        } else if dictionaryKeys.contains(elementName) {
            currentValue = ""
        }
    }
    
    // found characters
    //
    // - If this is an element we care about, append those characters.
    // - If `currentValue` still `nil`, then do nothing.
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        currentValue? += string
    }
    
    // end element
    //
    // - If we're at the end of the whole dictionary, then save that dictionary in our array
    // - If we're at the end of an element that belongs in the dictionary, then save that value in the dictionary
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if elementName == recordKey {
            results!.append(currentDictionary!)
            currentDictionary = nil
        } else if dictionaryKeys.contains(elementName) {
            currentDictionary![elementName] = currentValue
            currentValue = nil
        }
    }
    
    // Just in case, if there's an error, report it. (We don't want to fly blind here.)
    
    func parser(_ parser: XMLParser, parseErrorOccurred parseError: Error) {
        print(parseError)
        
        currentValue = nil
        currentDictionary = nil
        results = nil
    }
    
}
