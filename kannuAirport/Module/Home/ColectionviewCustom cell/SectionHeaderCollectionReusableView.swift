//
//  SectionHeaderCollectionReusableView.swift
//  kannuAirport
//
//  Created by Arpana on 08/05/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit

class SectionHeaderCollectionReusableView: UICollectionReusableView {
    
    @IBOutlet weak var headerLabel :UILabel!
}
