//
//  AirlineLogoCollectionViewCell.swift
//  kannuAirport
//
//  Created by Arpana on 03/05/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit

class AirlineLogoCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageViewLogo: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
      //  imageViewLogo.contentMode = .scaleAspectFill
    }

}
