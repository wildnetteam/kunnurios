//
//  HomeViewController.swift
//  kannuAirport
//
//  Created by Arpana on 01/05/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit
import SideMenu
import SDWebImage
import FTPopOverMenu_Swift
import KYDrawerController
import SwiftLoader
import Alamofire
class HomeViewController: UIViewController {
  
    @IBOutlet weak var ScrollViewImg: ImageScroller!
    @IBOutlet weak var scrollViewBase: UIScrollView!
    @IBOutlet weak var collectionViewAirlinesLogo: UICollectionView!
    @IBOutlet weak var collectionViewCategory: UICollectionView!
    @IBOutlet weak var scrollViewAdvertiseBanner: UIScrollView!
    @IBOutlet weak var _pageControl:UIPageControl?
    @IBOutlet weak var _langBtn:UIButton?
    @IBOutlet weak var _webView:UIWebView?
    
    var BannerObj: Banner? // variable to store advertise data after parsing
    var timer = Timer.init()
    var timerLogo = Timer.init()
    var logoArray : Logo? //Logo is an array
    let imgArr = Array<String>()
    var xPosition: CGFloat = 0.0
    var currentCellLogo = 0
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        showHeaderImages()
        registerNibs()
        //getBannerImages()
        getSideMenuItems()
        getHeaderBanner()
        
        if let currentLang = LocalDB.shared.currentLanguage {
            
            if currentLang == Language.english {
                
                _langBtn?.setTitle(" English", for: .normal)
            }else if currentLang == Language.hindi {
               _langBtn?.setTitle(" Hindi", for: .normal)
            }else{
                _langBtn?.setTitle(" Malyalam", for: .normal)
               
            }
        }else{
            _langBtn?.setTitle(" Englsih", for: .normal)
            
            LocalDB.shared.currentLanguage = Language.english
            
        }
        
        let htmlStr = "<!DOCTYPE html><html><body><div align=\"center\">For advertisement in this mobile app, please contact: <br />(+91) 7510381798</div></body></html>"
        _webView?.loadHTMLString(htmlStr, baseURL: nil)
        
    }
    
    func resetData(){
        getSideMenuItems()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        xPosition = 10
        scrollViewBase.isScrollEnabled = true
        
        
        let height = collectionViewCategory.frame.size.height+collectionViewCategory.frame.origin.x+collectionViewAirlinesLogo.frame.size.height+collectionViewAirlinesLogo.frame.origin.x+scrollViewAdvertiseBanner.frame.size.height+scrollViewAdvertiseBanner.frame.origin.x
        
        
        
        let contentSize = CGSize(width: self.view.frame.size.width, height:height+100)
        
        print("Height :  \(height)")
         print("Height :  \(scrollViewBase.frame.size.height)")
        
        
        scrollViewBase.contentSize = contentSize
        
        
        
    }
    
    override func viewDidLayoutSubviews() {
     
        self.view.updateConstraintsIfNeeded()
        self.view.layoutIfNeeded()
    }
    
    @IBAction func menuButtonTapped(_ sender: Any) {
        
        if  isMenuItemRetrieved == true {
            
            kAppDelegate.drawerController.setDrawerState(.opened, animated: true)

        }else {
            self.showAlertWithAction(with: "", message: "Error in retriving side menu items")
            
        }
    }
    
    func getHeaderBanner()  {
        
        if APIWrapper.isInternetAvailable() {
            SwiftLoader.show(animated: true)
            
            APICall .callHeaderBannerImagesAPI([ : ], header: ["Authorization" : AuthorizationToken ]) { (sucess, result, error) in
                
                DispatchQueue.main.async {
                    
                    weak var weakSelf = self
                    
                    SwiftLoader.hide()
                    
                    if (error != nil) {
                        
                        weakSelf?.showAlertWithAction(with: "", message: error?.localizedDescription)
                    }
                    
                    if sucess {
                        
                        
                        do {
                            let jsonDecoder = JSONDecoder()
                            
                            self.BannerObj = try  jsonDecoder.decode(Banner.self, from: result!) as Banner
                            
                            
                            self.initAdSlide()
                            
                        } catch  {
                            weakSelf?.showAlertWithAction(with: "", message: AlertMessages.MODELERRORMESSAGE)

                        }
                    }
                        
                    else{
                        //Alert not sucessfull
                    }
                }
            }
        }else {
            self.showAlert(with: "", message: AlertMessages.networkError)
            
        }
    }
    func getSideMenuItems()  {
        
        isMenuItemRetrieved = false

        if APIWrapper.isInternetAvailable() {
            
            SwiftLoader.show(animated: true)
            
            APICall .callMainMenuItemsAPI([ : ], header: ["Authorization" : AuthorizationToken ]) { (sucess, result, error) in
                DispatchQueue.main.async {
                    
                    weak var weakSelf = self
                    
                    
                    SwiftLoader.hide()
                    
                    if (error != nil){
                        
                        weakSelf?.showAlertWithAction(with: "", message: error?.localizedDescription)
                        return
                        
                    }
                    if sucess {
                        
                        do {
                            let jsonDecoder = JSONDecoder()
                            
                            if let mainMenuItem = try  jsonDecoder.decode(MainMenu.self, from: result!) as MainMenu? {
                                
                                isMenuItemRetrieved = true
                                (kAppDelegate.drawerController.drawerViewController as? SideMenuTableViewController)?.setMenuItems(menuItemsArray: mainMenuItem )
                            }else{
                                
                            }
                            
                            
                        } catch  {
                            
                            weakSelf?.showAlertWithAction(with: "", message: AlertMessages.MODELERRORMESSAGE)
                            
                        }
                    }
                        
                    else{
                        //Alert not sucessfull
                    }
                }
            }
            
        }else {
            self.showAlert(with: "", message: AlertMessages.networkError)

        }
    }
    
    override func okBtnTapped() {
     isMenuItemRetrieved = true
        getSideMenuItems()
    }
    
    //MARK:- IBActions
    @IBAction func languageBtnTapped(_ sender:UIButton){
        var array:NSArray?
        
        if let currentLang = LocalDB.shared.currentLanguage {
            
            if currentLang == Language.english {
                
                array = [" Hindi"," Malyalam"]
                
            }else if currentLang == Language.hindi {
                
                 array = [" English"," Malyalam"]
                
            }else if currentLang == Language.malayalam {
                array = [" Hindi"," English"]
            }
        }
        else{
            array = [" Hindi"," Malyalam"]
            LocalDB.shared.currentLanguage = .english
        }
        
        FTPopOverMenu.showForSender(sender: sender,
                                    with: array as! [String],
                                    done: { (selectedIndex) -> () in
                                        sender.setTitle(array?[selectedIndex] as? String, for: .normal)
                                        print(selectedIndex)
                                        print(Language.english)
                                        if ((array?[selectedIndex] as! String) == " English"){
                                            LocalDB.shared.currentLanguage = Language.english
                                        }else if ((array?[selectedIndex] as! String) == " Hindi"){
                                            LocalDB.shared.currentLanguage = Language.hindi
                                        }else{
                                            LocalDB.shared.currentLanguage = Language.malayalam
                                        }
                                        DispatchQueue.main.async {
                                            
                                            //Instantiate storyboard again specific to language
                                            
                                            let mainViewController   = UIStoryboard.homeScreenViewController()
                                            
                                            let sideMenuTableViewControllerObj = UIStoryboard.sideMenuController()
                                            
                                            kAppDelegate.menuNavController = UINavigationController(rootViewController: mainViewController)
                                            kAppDelegate.drawerController.mainViewController =  kAppDelegate.menuNavController
                                            kAppDelegate.drawerController.drawerViewController = sideMenuTableViewControllerObj
                                            kAppDelegate.menuNavController?.isNavigationBarHidden = true
                                            kAppDelegate.window?.rootViewController =  kAppDelegate.drawerController
                                            kAppDelegate.drawerController.setDrawerState(.closed, animated: true)
                                            
                                            self.resetData()
                                            self.collectionViewCategory.reloadData()
                                        }
                                        
        }) {
            
        }
        
    }
    
    
    
    //MARK:- Get banner images for advartise purpose
    func getBannerImages() {
        
        if APIWrapper.isInternetAvailable() {
            
            SwiftLoader.show(animated: true)
            
            
            
            APICall.callLogoAPI([:], header: [:]) { (sucess, result, error) in
                
                DispatchQueue.main.async {
                    
                    weak var weakSelf = self
                    
                    SwiftLoader.hide()
                    
                    if (error != nil){
                        
                        weakSelf?.showAlert(with: "", message: error?.localizedDescription)
                    }
                    if sucess {
                        
                        do {
                            let jsonDecoder = JSONDecoder()
                            
                            self.logoArray = try  jsonDecoder.decode(Logo.self, from: result!) as Logo
                            self.collectionViewAirlinesLogo.reloadData()
                            
                        } catch  {
                            weakSelf?.showAlertWithAction(with: "", message: AlertMessages.MODELERRORMESSAGE)

                        }
                    }
                        
                    else{
                        //Alert not sucessfull
                        weakSelf?.showAlertWithAction(with: "", message: AlertMessages.UnidetifiedErrorMessage)

                    }
                }
            }
        }else {
            self.showAlert(with: "", message: AlertMessages.networkError)
            
        }
    }
    
    func showHeaderImages()   {
        
        var images =  Array<String>()
        
        if APIWrapper.isInternetAvailable() {
            
            APICall.callHeaderSlidingImagesAPI([:], header: [:]) { (sucess, result, error) in
                
                DispatchQueue.main.async {
                    
                    weak var weakSelf = self
                    
                    SwiftLoader.hide()
                    
                    if (error != nil){
                        
                        weakSelf?.showAlertWithAction(with: "", message: error?.localizedDescription)
                    }
                    if sucess {
                        
                        do {
                            let jsonDecoder = JSONDecoder()
                            
                            let slidingObj  = try  jsonDecoder.decode(HomeSlidingImages.self, from: result!) as HomeSlidingImages
                            
                            images = slidingObj.sliderimage!
                            
                            if (images.count) > 0 {
                                //scroller config
                                self.ScrollViewImg.isAutoScrollEnabled = true
                                self.ScrollViewImg.delegate = weakSelf
                                self.ScrollViewImg.scrollTimeInterval = 2.0 //time interval
                                self.ScrollViewImg.scrollView.bounces = false
                                self.ScrollViewImg.setupScrollerWithImages(images: images )
                                self.view.layoutIfNeeded()
                                self._pageControl?.numberOfPages = images.count
                            }
                            
                            
                        } catch  {
                            weakSelf?.showAlertWithAction(with: "", message: AlertMessages.MODELERRORMESSAGE)

                        }
                    }
                        
                    else{
                        //Alert not sucessfull
                        weakSelf?.showAlertWithAction(with: "", message: AlertMessages.UnidetifiedErrorMessage)

                    }
                }
            }
            
        }else {
            self.showAlert(with: "", message: AlertMessages.networkError)
            
        }
    }
    
    func registerNibs(){

        collectionViewCategory?.register(UINib(nibName: "CategoryCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CategoryCollectionViewCell")

         collectionViewAirlinesLogo?.register(UINib(nibName: "AirlineLogoCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "AirlineLogoCollectionViewCell")
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func initAdSlide()
    {
        
        if var imgArr = self.BannerObj?.fieldBannerImage
        {
            for index in 0...imgArr.count - 1{
                
                let adImageView = UIImageView()
                let imageTap = UIButton()
                
                if let url = URL(string : imgArr[index].url! )
                {
                    adImageView.sd_setImage(with: url, completed: { (image, error, type, url) in
                        if error == nil{
                            adImageView.image = image
                        }
                    })
                }
                adImageView.frame = CGRect(x:self.xPosition,y: 0,width: self.scrollViewAdvertiseBanner.frame.size.width - 20, height:self.scrollViewAdvertiseBanner.frame.size.height)
                imageTap.frame = adImageView.frame
                imageTap.addTarget(self, action: #selector(self.handleImageTap(_:)), for: .touchUpInside)
                imageTap.tag = index
                self.scrollViewAdvertiseBanner.addSubview(adImageView)
                self.scrollViewAdvertiseBanner.addSubview(imageTap)
                self.xPosition += self.view.frame.width
                self.scrollViewAdvertiseBanner.contentSize = CGSize(width: CGFloat(index + 1) * self.scrollViewAdvertiseBanner.frame.size.width, height: self.scrollViewAdvertiseBanner.frame.size.height)
            }
            let scrollWidth = self.scrollViewAdvertiseBanner.contentSize.width + self.scrollViewAdvertiseBanner.frame.size.width
            self.scrollViewAdvertiseBanner.contentSize = CGSize(width: scrollWidth, height: self.scrollViewAdvertiseBanner.frame.size.height)
            
            self.timer.invalidate()
            self.timer = Timer.scheduledTimer(timeInterval: 5.0
                , target: self, selector: #selector(self.onSliderShow), userInfo: nil, repeats: true)
            
            self.timerLogo.invalidate()
            self.timerLogo = Timer.scheduledTimer(timeInterval: 3.0
                , target: self, selector: #selector(self.onLogoShow), userInfo: nil, repeats: true)
            
             self._pageControl?.currentPage = 0
            self._pageControl?.pageIndicatorTintColor = UIColor.white
            self._pageControl?.currentPageIndicatorTintColor = UIColor(red: 30/255.0, green: 151/255.0, blue: 231/255.0, alpha: 1.0)
        }
        
    }
    
    @objc func onLogoShow(timer: Timer){
        if let coll  = collectionViewAirlinesLogo {
            for cell in coll.visibleCells {
                let indexPath: IndexPath? = coll.indexPath(for: cell)
                if ((indexPath?.row)!  <  (logoArray?.count)! - 1){
                    let indexPath1: IndexPath?
                    indexPath1 = IndexPath.init(row: (indexPath?.row)! + 1, section: (indexPath?.section)!)
                    
                    coll.scrollToItem(at: indexPath1!, at: .right, animated: true)
                }
                else{
                    let indexPath1: IndexPath?
                    indexPath1 = IndexPath.init(row: 0, section: (indexPath?.section)!)
                    coll.scrollToItem(at: indexPath1!, at: .left, animated: true)
                }
                
            }
        }
    }
    
    @objc func onSliderShow(timer: Timer){

        if let imgArr = self.BannerObj?.fieldBannerImage
        {
            self.timer = timer
            let pageWidth = self.scrollViewAdvertiseBanner.frame.width
            let maxWidth = pageWidth * CGFloat(imgArr.count)
            let contentOffset = self.scrollViewAdvertiseBanner.contentOffset.x
            var slideToX = contentOffset + pageWidth
           
            if contentOffset + pageWidth == maxWidth{
                slideToX = 0
            }
            self.scrollViewAdvertiseBanner.scrollRectToVisible(CGRect(x:slideToX,y: 0,width: pageWidth,height: self.scrollViewAdvertiseBanner.frame.height), animated: true)
        }
    }
    
    @objc func handleImageTap(_ sender: UIButton){
       // Utility.openUrl(url: self.linkArr[sender.tag] as! String)
    }
    
    deinit {
        
        print("\(String(describing: self.logoArray)) logoArray is deinitialized " )
        print("\(self.timer) timer is deinitialized " )
        print("\(String(describing: self.BannerObj)) BannerObj is deinitialized " )
        print("\(self.imgArr) imgArr is deinitialized " )
        print("\(String(describing: self.navigationController)) navigationController is deinitialized " )

    }
}


extension HomeViewController: UICollectionViewDelegate , UICollectionViewDataSource {
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(collectionView.tag == 99){
            //Its category collection view
        return 15
        }
        if(collectionView.tag == 1002){
            if let logoCount = logoArray{
                
                return logoCount.count
                
            } else{
                return 0
            }
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if(collectionView.tag == 99){
            //Its category collection view
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCollectionViewCell", for: indexPath) as! CategoryCollectionViewCell
            cell.labelTitle.text = getTittlesForeMenu(categoryType: MenuCategory(rawValue: indexPath.row)!)
            cell.imagViewLogo.image = UIImage.init(named: getImagesForMenuCategory(categoryType: MenuCategory(rawValue: indexPath.row)!))
            return cell
            
        }else if(collectionView.tag == 1002){
            
            //Its logo's collection view
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AirlineLogoCollectionViewCell", for: indexPath) as! AirlineLogoCollectionViewCell
            
            if let val :LOGOElement = self.logoArray?[indexPath.row] {
                
                if let fieldImage = val.fieldImage {
                cell.imageViewLogo.sd_imageIndicator?.startAnimatingIndicator()
                    cell.imageViewLogo.sd_setImage(with: URL(string:imageBaseUrl + fieldImage), completed: { (image, error, type, url) in
                        if error == nil{
                            cell.imageViewLogo.image = image
                        }
                        cell.imageViewLogo.sd_imageIndicator?.stopAnimatingIndicator()
                    })
                }
            }else{
                cell.imageViewLogo.image = nil
            }
                
            return cell
        }
        else{
            //Will never enter to this condition
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "",
                                                          for: indexPath)
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == collectionViewCategory {
            
            if APIWrapper.isInternetAvailable() {
                
                if indexPath.row == MenuCategory.ARRIVAL.rawValue {
                    
                    let detailVC = UIStoryboard.detailController()
                    self.navigationController?.pushViewController(detailVC, animated: true)
                    
                }else if indexPath.row == MenuCategory.FLIGHTTIMETABLE.rawValue{
                    
                      NavigationManager.moveToInformationViewControllerIfNotExists(kAppDelegate.menuNavController! ,node: .NOTAPPLIED, nodeIden: "56" ,nodeName :  "Flight Timetable", isSideMenuIconToAppear: false)
                }else if indexPath.row == MenuCategory.WEBCHECKIN.rawValue{
                    
                    NavigationManager.moveToInformationViewControllerIfNotExists(kAppDelegate.menuNavController! ,node: .NOTAPPLIED, nodeIden: "59" ,nodeName :  "Web Check In", isSideMenuIconToAppear: false)
                }else if indexPath.row == MenuCategory.AIRLINECONTACTS.rawValue{
                    
                    NavigationManager.moveToInformationViewControllerIfNotExists(kAppDelegate.menuNavController! ,node: .NOTAPPLIED, nodeIden: "58" ,nodeName :  "Airline Contacts", isSideMenuIconToAppear: false)
                }else if indexPath.row == MenuCategory.MEETNGREET.rawValue{
                    
                    NavigationManager.moveToInformationViewControllerIfNotExists(kAppDelegate.menuNavController! ,node: .NOTAPPLIED, nodeIden: "98" ,nodeName :  "Meet and Greet", isSideMenuIconToAppear: false)
                }else if indexPath.row == MenuCategory.PREPAIDTAXI.rawValue{
                    
                    NavigationManager.moveToInformationViewControllerIfNotExists(kAppDelegate.menuNavController! ,node: .NOTAPPLIED, nodeIden: "101" ,nodeName :  "Prepaid Taxi", isSideMenuIconToAppear: false)
                }
                else if indexPath.row == MenuCategory.SHOPPING.rawValue{
                    
                    NavigationManager.moveToInformationViewControllerIfNotExists(kAppDelegate.menuNavController! ,node: .NOTAPPLIED, nodeIden: "4" ,nodeName :  "Shopping", isSideMenuIconToAppear: false)
                }else if indexPath.row == MenuCategory.EATNDINE.rawValue{
                    
                    NavigationManager.moveToInformationViewControllerIfNotExists(kAppDelegate.menuNavController! ,node: .NOTAPPLIED, nodeIden: "106" ,nodeName :  "EAT and Dine", isSideMenuIconToAppear: false)
                }else if indexPath.row == MenuCategory.MEDICALAID.rawValue{
                    
                    NavigationManager.moveToInformationViewControllerIfNotExists(kAppDelegate.menuNavController! ,node: .NOTAPPLIED, nodeIden: "95" ,nodeName :  "Medical Aid", isSideMenuIconToAppear: false)
                }
                else if indexPath.row == MenuCategory.BUSSCHEDULE.rawValue {
                    
                    NavigationManager.moveToInformationViewControllerIfNotExists(kAppDelegate.menuNavController! ,node: .NOTAPPLIED, nodeIden: "100" ,nodeName :  "Bus Schedule", isSideMenuIconToAppear: false)
                }else if indexPath.row == MenuCategory.HELPDESK.rawValue {
                    
                    NavigationManager.moveToInformationViewControllerIfNotExists(kAppDelegate.menuNavController! ,node: .NOTAPPLIED, nodeIden: "108" ,nodeName :  "Help Desk", isSideMenuIconToAppear: false)
                }else if indexPath.row == MenuCategory.FAQ.rawValue {
                    
                    NavigationManager.moveToInformationViewControllerIfNotExists(kAppDelegate.menuNavController! ,node: .NOTAPPLIED, nodeIden: "50" ,nodeName :  "FAQ", isSideMenuIconToAppear: false)
                }else if indexPath.row == MenuCategory.CAREER.rawValue {
                    
                    
                    
                    NavigationManager.moveToInformationAsTabularViewControllerIfNotExists(kAppDelegate.menuNavController! , nodeURI: "press-room-view" ,nodeName : "News", isSideMenuIconToAppear: false, typeOfView: true)
//                    NavigationManager.moveToInformationViewControllerIfNotExists(kAppDelegate.menuNavController! ,node: .NOTAPPLIED, nodeIden: "7" ,nodeName :  "Career", isSideMenuIconToAppear: false)
                }
                
                else if indexPath.row == MenuCategory.LOUNGES.rawValue{
                    
                    NavigationManager.moveToInformationViewControllerIfNotExists(kAppDelegate.menuNavController! ,node: .NOTAPPLIED, nodeIden: "107" ,nodeName :  "Lounges", isSideMenuIconToAppear: false)
                }
                else if  indexPath.row == 5 {
                    NavigationManager.moveToShopListViewControllerIfNotExists(kAppDelegate.menuNavController!)
                    
                }else if  indexPath.row == MenuCategory.MAPS.rawValue {
                    //map
                    NavigationManager.moveTMapViewControllerIfNotExists(kAppDelegate.menuNavController!, webURL: "map")
                   // NavigationManager.moveToWebInformationDisplayViewControllerIfNotExists(kAppDelegate.menuNavController!, webURL: "map")

                    
                }else{
                    print("row is \(indexPath.row)")
                    NavigationManager.moveToWebInformationDisplayViewControllerIfNotExists(kAppDelegate.menuNavController!, webURL: "")
                }
            }else {
                
                self.showAlertWithAction(with: "", message: AlertMessages.networkError)

            }
        }
    }
}

extension HomeViewController:ImageScrollerDelegate {
    func pageChanged(index: Int) {
       _pageControl?.currentPage = index
    }
}
extension HomeViewController : UICollectionViewDelegateFlowLayout {
    //1
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {

        var   size   =  CGSize(width: (self.view.frame.width / 3) , height: ((collectionView.frame.height - 20 ) / 1.2 )) // 2.4
        
        if(collectionView.tag == 99){
            //Its category collection view
         size   =  CGSize(width: (self.view.frame.width / 3) - 0.5, height: (collectionView.frame.height / ( (1 / 3) *   15) - 0.5))
            
        }
        
        return size

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        if(collectionView.tag == 99){
            //Its category collection view
        return UIEdgeInsetsMake(0, 0, 0, 0)
        }
        return UIEdgeInsetsMake(10, 10, 10, 10)

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat{
        
        return 0.5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
        return 0.5
    }
    
    
}


