//
//  FlightTimeTableViewCell.swift
//  kannuAirport
//
//  Created by Arpana on 08/05/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit

class FlightTimeTableViewCell: UITableViewCell {

    @IBOutlet weak var lblType : UILabel!
    @IBOutlet weak var lblFlightNumber : UILabel!
    @IBOutlet weak var lblDestination : UILabel!
    @IBOutlet weak var lblFlightDate : UILabel!
    @IBOutlet weak var lblSTD : UILabel!
    @IBOutlet weak var lblETD : UILabel!
    @IBOutlet weak var lblFlightStatus : UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData (arriavlModel: ArrivalDepartModel?){
        
        if let arrivalData = arriavlModel {
            
            if let type = arrivalData.type {
                
                lblType.text = type
            }
            
            if let origin = arrivalData.origin {
                
                lblDestination.text = origin
            }
            if let code = arrivalData.flightCode {
                
                lblFlightNumber.text = code
            }
            if let flightDate = arrivalData.flightDate {
                
                lblFlightDate.text = flightDate
            }
            if let flightStatus = arrivalData.status {
                
                lblFlightStatus.text = flightStatus
            }
            if let std = arrivalData.sta {
                
                lblSTD.text = std
            }
            if let etd = arrivalData.eta {
                
                lblETD.text = etd
            }
            if let type = arrivalData.type {
                
                lblType.text = type
            }
        }
    }
    
    func setDepartData (departModel: ArrivalDepartModel?){
        
        if let arrivalData = departModel {
            
            if let type = arrivalData.type {
                
                lblType.text = type
            }
            if let destination = arrivalData.destination {
                
                lblDestination.text = destination
            }
            if let code = arrivalData.flightCode {
                
                lblFlightNumber.text = code
            }
            if let flightDate = arrivalData.flightDate {
                
                lblFlightDate.text = flightDate
            }
            if let flightStatus = arrivalData.status {
                
                lblFlightStatus.text = flightStatus
            }
            if let std = arrivalData.std {
                
                lblSTD.text = std
            }
            if let etd = arrivalData.etd {
                
                lblETD.text = etd
            }
            if let type = arrivalData.type {
                
                lblType.text = type
            }
        }
    }

}
