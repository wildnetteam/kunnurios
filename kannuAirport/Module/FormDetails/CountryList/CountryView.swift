//
//  CountryView.swift
//  Bizzalley
//
//  Created by Anshul on 16/03/17.
//  Copyright © 2017 Anuj Jha. All rights reserved.
//

import UIKit

protocol CountryViewDelegate {
    func removeCountryView()
    func getCountryCode(CountryName: String)
}

class CountryView: UIView {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var containerView:UIView!
    @IBOutlet weak var tableView:UITableView!
    
    var allCountryObject : Country?

    // Search setup...
    @IBOutlet weak var searchBar: UISearchBar!
    var searchActive : Bool = false
    var filtered: Country?
    
    var delegate:CountryViewDelegate?
    var selectedIndex:NSInteger?
    
    //MARK:- Intializer
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    override func draw(_ rect: CGRect) {
        super.draw(rect)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func registerNibs(){
        
        self.tableView.register(UINib.init(nibName: "CountryTableViewCell", bundle: nil), forCellReuseIdentifier: "CountryTableViewCell")
        
        //UISearchBar left alignment setup...
        // 320:43  375:57  414:67
        let spaceCount = UIScreen.main.bounds.size.width == 320 ? 43 : UIScreen.main.bounds.size.width == 375 ? 57 : 67
        let newText = "Search \((Array(repeating: " ", count: Int(spaceCount)).joined(separator: "")))"
        searchBar.placeholder = newText
        
    }
    
    func managerRadius(){
        containerView.layer.cornerRadius = 5
    }
    
    @IBAction func outSideTapped(sender:UIButton){
        if delegate != nil {
            delegate?.removeCountryView()
        }
    }
    
    //MARK:- GET COUNTRY FROM API
    
    func getCountryDetails()  {
        
        //   SwiftLoader.show(animated: true)
        
        APICall .callCountryAPI([ : ], header: ["Authorization" : AuthorizationToken ]) { (sucess, result, error) in
            DispatchQueue.main.async {
                
                //  SwiftLoader.hide()
                
                if (error != nil){
                 
                    self.showAlertMessgae((error?.localizedDescription)! ,0)
                    
                }
                if sucess {
                    
                    do {
                        
                        let jsonDecoder = JSONDecoder()
                        self.allCountryObject = try  jsonDecoder.decode(Country.self, from: result!) as Country
                        
                        self.tableView.reloadData()
                        
                    } catch  {

                    }
                }
                else {
                    //Alert not sucessfull
                    self.showAlertMessgae("Some error occured" ,0)

                }
            }
        }
    }

    
    //MARK:- IBAction
    
    @IBAction func doneBtnTapped(sender:UIButton){
        
        if delegate != nil {
            
            if selectedIndex != nil &&  selectedIndex! > 0 {
                
                 if(searchActive) {

                    delegate?.getCountryCode(CountryName: (filtered?.data![selectedIndex!].country)!)

                 }else{
                delegate?.getCountryCode(CountryName:(allCountryObject?.data![selectedIndex!].country)!)
                }
            }
        }
        delegate?.removeCountryView()

    }
    
    //MARK:- Custom Methods
    
    func showAlertMessgae(_ message: String, _ tag : Int) {
        
        let alertController = UIAlertController(title: "Sorry", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
            
            if tag == 0 {
                
            }
            
        }))
        
        kAppDelegate.menuNavController?.present(alertController, animated: true, completion: nil)
    }
}

extension CountryView : UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //var count = 0
        
        if(searchActive) {
            
            if let countryOb = filtered {
                return (countryOb.data?.count)!
            }else{
                
                return 0
            }
        }else{
            
            if let countryOb = allCountryObject {
                return (countryOb.data?.count)!
            }else{
                return 0
                
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CountryTableViewCell", for: indexPath) as? CountryTableViewCell
        
        if let index = selectedIndex {
            if index == indexPath.row{
                cell?.accessoryType = .checkmark
            }else{
                cell?.accessoryType = .none
            }
        }else{
            cell?.accessoryType = .none
        }
        
        cell?.selectionStyle = .none
        
        if (searchActive) {
            let titleText = filtered?.data?[indexPath.row].country
            cell?.titleLbl.text = titleText


        } else {
    //    if(self.tag == 1){
            let titleText = allCountryObject?.data?[indexPath.row].country
                
            cell?.titleLbl.text = titleText
        }
//        }else  if(self.tag == 2 ){
//
//            let titleText = (countryViewArray?[indexPath.row] as! NSDictionary).object(forKey: "region")
//            cell?.titleLbl.text = titleText as! String?
//
//        }else  if(self.tag == 3){
//
//            let titleText = (countryViewArray?[indexPath.row] as! NSDictionary).object(forKey: "city")
//            cell?.titleLbl.text = titleText as! String?
//            }
//
        return cell!
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        tableView.separatorInset = UIEdgeInsets.zero
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        selectedIndex = indexPath.row
        

        self.tableView.reloadData()

    }
    
    //MARK:- UISearchBarDelegate
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        //searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.endEditing(true)
        searchActive = false;
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
       
        if(self.tag == 1){
            
          //  .range(of: searchText, options: [.caseInsensitive]) != nil
//            filtered?.data = allCountryObject?.data?.filter { $0.country?.range(of: searchText, options: [.caseInsensitive]) != nil
//            }
            
            
            let FilteredCountries = allCountryObject?.data?.filter { ($0.country?.contains(searchText))!
            }
            
            filtered = Country(data: FilteredCountries)
        }

        if allCountryObject?.data != nil {

            if(searchText.count == 0){
                searchActive = false;
            } else {
                searchActive = true;
            }
            selectedIndex  = -1
            self.tableView.reloadData()
        }

    }
}

