//
//  Constants.swift
//  kannuAirport
//
//  Created by Arpana on 03/05/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import Foundation

//public enum MenuCategory :Int
//{
//    case ARRIVAL
//    case FLIGHTTIMETABLE
//    case TOANDFROM
//    case LOUNGES
//    case MAPS
//    case SHOPEAT
//    case WEBCHECKIN
//    case AIRLINECONTACT
//    case MEETNGREET
//    case PREPAIDTAXI
//    case MEDICALAID
//    case HELPDESK
//    case OTHER
//}

public enum MenuCategory :Int
{
    case ARRIVAL = 0
    case FLIGHTTIMETABLE
    case WEBCHECKIN
    case AIRLINECONTACTS
    case BUSSCHEDULE
    case LOUNGES
    case MEETNGREET
    case PREPAIDTAXI
    case SHOPPING
    case EATNDINE
    case MEDICALAID
    case HELPDESK
    case MAPS
    case FAQ
    case CAREER
    case OTHER
}

public func getTittlesForeMenu (categoryType : MenuCategory) -> String {
    
    switch categoryType {

    case .ARRIVAL:
        return  "Flight Status"
    case .FLIGHTTIMETABLE:
        return  "Flight Timetable"
    case .WEBCHECKIN:
        return  "Web Check In"
    case .AIRLINECONTACTS:
        return "Airline Contact"
    case .BUSSCHEDULE:
        return "Bus Schedule"
    case .LOUNGES:
        return "Lounges"
    case .MEETNGREET:
        return "Meet & Greet"
    case .PREPAIDTAXI:
        return "Prepaid Taxi"
    case .SHOPPING:
        return "Shopping"
    case .EATNDINE:
        return "Eat & Dine"
    case .MEDICALAID:
        return "Medical Aid"
    case .HELPDESK:
        return "Help Desk"
    case .MAPS:
        return "Maps"
    case .FAQ:
        return "FAQs"
    case .CAREER:
        return "News"
        
    default: return "default"
        
        
    }
}

public func getImagesForMenuCategory (categoryType : MenuCategory) -> String {
    
    switch categoryType {
        
    case .ARRIVAL:
        return  "departuresGray"
    case .FLIGHTTIMETABLE:
        return  "Timetable"
    case .WEBCHECKIN:
        return  "web checkin"
    case .AIRLINECONTACTS:
        return "airport contact"
    case .BUSSCHEDULE:
        return "Bus"
    case .LOUNGES:
        return "loungeGray"
    case .MEETNGREET:
        return "meeting"
    case .PREPAIDTAXI:
        return "cab_new"
    case .SHOPPING:
        return "shopping"
    case .EATNDINE:
        return "dinner"
    case .MEDICALAID:
        return "medical"
    case .HELPDESK:
        return "helpdesk"
    case .MAPS:
        return "Map"
    case .FAQ:
        return "FAQ"
    case .CAREER:
        return "career"
        
        
    default: return "maps"
        
    
    }
}

public let AuthorizationToken = "YWRtaW46YWRtaW5A"
public let user = "admin"
public let password = "super@!#$admin"


let kNavigationBarColor = UIColor.init(red: 0.12, green: 0.58, blue: 0.56, alpha: 1.0)

let kAppDelegate     =    UIApplication.shared.delegate as! AppDelegate


public var InBottom : CGFloat = 0.0
public var InTop :CGFloat = 0.0

let IS_IPHONE_X: Bool   =   (UIScreen.main.bounds.size.height == 812.0)
let IS_IPHONE_5    =             UIScreen.main.bounds.size.width > 320 ? false : true

var isMenuItemRetrieved = false

struct SizeConstants {

    static func setInsetsForTop(_ val : CGFloat) {
        InTop = val
    }
    static func getInsetsForTop() -> CGFloat{
        return InTop
    }
    static func setInsetsForBottom(_ val : CGFloat) {
        InBottom = val
    }
    static func getInsetsForBottom() -> CGFloat{
        return InBottom
    }
}

struct ColorConstants {
    
    static func RGBCOLOR(_ red: Int, green: Int, blue: Int) -> UIColor {
        
        return UIColor(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1)
    }
    static func textSelectedCOLOR() -> UIColor {
        
        return UIColor(red:0.77, green:0.53, blue:0.85, alpha:1)
    }
    
    static func textDeSelectedCOLOR() -> UIColor {
        
        return UIColor.white
    }
    static func appBackGroundCOLOR() -> UIColor {
        
        return UIColor(red:0.12, green:0.58, blue:0.86, alpha:1)
        
    }
    
    
}
let userDefault = UserDefaults.standard

enum Language:String {
    case english
    case hindi
    case malayalam
}

func getLanguageKey (lang : Language) -> String {
    
    switch lang {
        
    case .english: return "Base"
    case .hindi:    return "hi-IN"
    case .malayalam:  return "ml-IN"
        
    }
}

struct Local_DB_Key {
    static let language = "current_language"
    
}

struct AlertMessages {
    
    static let MODELERRORMESSAGE = "Response structure has been changed."
    static let NOTADICTIONARYMESSAGE = "Response is not a dictionary."
    static let UnidetifiedErrorMessage = "Something went wrong!!"
    static let networkError = "Internet connection appears to be offline."
    static let bodyError = "Structure for key body has been changed."
    
}
