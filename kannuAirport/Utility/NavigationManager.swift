//
//  NavigationManager.swift
//  my digital nrg
//
//  Created by Tushar Agarwal on 18/01/17.
//  Copyright © 2017 Wildnet Technologies Pvt. Ltd. All rights reserved.
//

import UIKit
import SideMenu

class NavigationManager: NSObject {

    class func moveToHomeViewControllerIfNotExists(_ navigationController : UINavigationController) {
        let viewControllers = navigationController.viewControllers
        if viewControllers.count > 0 {
            if !(viewControllers.last is HomeViewController) {
                navigationController.pushViewController(UIStoryboard.homeScreenViewController(), animated: false)
            }
        }
    }
    
    
    class func moveToFilterControllerIfNotExists(_ navigationController : UINavigationController) {
        let viewControllers = navigationController.viewControllers
        if viewControllers.count > 0 {
            if !(viewControllers.last is FlightListViewController) {
                let filterVC = UIStoryboard.filterController()
                navigationController.pushViewController(filterVC, animated: false)
            }
        }
    }
    
    class func moveToInformationViewControllerIfNotExists(_ navigationController : UINavigationController , node : NodeType , nodeIden :String ,nodeName :String , isSideMenuIconToAppear :Bool) {
        let viewControllers = navigationController.viewControllers
        if viewControllers.count > 0 {
            if !(viewControllers.last is InformationViewController) {
                
                let informationVC = UIStoryboard.informationController()
                informationVC.nodeID = node
                informationVC.nodeIDToGetDetails = nodeIden
                informationVC.nodeTitle = nodeName
                informationVC.isFromSideController = isSideMenuIconToAppear
                navigationController.pushViewController(informationVC, animated: false)
            }else{
                //Just change the nodeId reload the page
                (viewControllers.last as! InformationViewController).nodeID = node
                (viewControllers.last as! InformationViewController).nodeIDToGetDetails = nodeIden
                (viewControllers.last as! InformationViewController).nodeTitle = nodeName
                (viewControllers.last as! InformationViewController).viewWillAppear(true)
            }
            
        }
    }
    
    class func moveToNewsViewControllerIfNotExists(_ navigationController : UINavigationController , node : NodeType) {
        let viewControllers = navigationController.viewControllers
        if viewControllers.count > 0 {
            if !(viewControllers.last is NewsViewController) {
                
                let informationVC = UIStoryboard.newsController()
                informationVC.isNewsToDisplay = node == .NEWS ? true :false
                navigationController.pushViewController(informationVC, animated: false)
            }else{
                                //Just change the nodeId reload the page
                (viewControllers.last as! NewsViewController).isNewsToDisplay = node == .NEWS ? true :false
                
                (viewControllers.last as! NewsViewController).viewWillAppear(true)
            }
            
        }
    }
    
    class func moveToPDFViewControllerIfNotExists(_ navigationController : UINavigationController , node : NodeType ,pdfURL :String ,nodeName :String ) {
        let viewControllers = navigationController.viewControllers
        if viewControllers.count > 0 {
            if !(viewControllers.last is PDFViewController) {
                
                let informationVC = UIStoryboard.PDFController()
                informationVC.PDFUrl = pdfURL
                informationVC.nodeTitle = nodeName
                navigationController.pushViewController(informationVC, animated: false)
            }else{
                //Just change the nodeId reload the page
                (viewControllers.last as! PDFViewController).PDFUrl = pdfURL
                (viewControllers.last as! PDFViewController).nodeTitle = nodeName
                (viewControllers.last as! PDFViewController).viewWillAppear(true)
            }
            
        }
    }
    
    class func moveToNotificationViewControllerIfNotExists(_ navigationController : UINavigationController , node : NodeType) {
        let viewControllers = navigationController.viewControllers
        if viewControllers.count > 0 {
            if !(viewControllers.last is NotificationListViewController) {
                
                let informationVC = UIStoryboard.notificationListViewController()
              //  informationVC.isNewsToDisplay = node == .NEWS ? true :false
                navigationController.pushViewController(informationVC, animated: false)
            }else{
                //Just change the nodeId reload the page
                (viewControllers.last as! NotificationListViewController).isNewsToDisplay = node == .NEWS ? true :false
                
                (viewControllers.last as! NotificationListViewController).viewWillAppear(true)
            }
            
        }
    }
    class func moveToGalleryViewControllerIfNotExists(_ navigationController : UINavigationController , nodeId :String) {
        let viewControllers = navigationController.viewControllers
        if viewControllers.count > 0 {
            if !(viewControllers.last is GalleryViewController) {
                
                let informationVC = UIStoryboard.galleryController()
                informationVC.nodeID = nodeId
                navigationController.pushViewController(informationVC, animated: false)
            }else {
                
                //Just change the nodeId reload the page
                (viewControllers.last as! GalleryViewController).nodeID = nodeId
                
                (viewControllers.last as! GalleryViewController).viewWillAppear(true)
                
            }
        }
    }
    
    
    class func moveToInformationAsTabularViewControllerIfNotExists(_ navigationController : UINavigationController , nodeURI : String?  , nodeName :String , isSideMenuIconToAppear :Bool , typeOfView : Bool) {
        let viewControllers = navigationController.viewControllers
        if viewControllers.count > 0 {
            if !(viewControllers.last is InformationAsTabularViewController) {
                
                let informationVC = UIStoryboard.informationAsTabularViewController()
                informationVC.nodeIDToGetDetails = nodeURI!
                informationVC.nodeTitle = nodeName
                informationVC.isTypeContentView = typeOfView
                informationVC.isFromSideController = isSideMenuIconToAppear
                navigationController.pushViewController(informationVC, animated: false)
            }else{
                //Just change the nodeId reload the page
                (viewControllers.last as! InformationAsTabularViewController).nodeIDToGetDetails = nodeURI!
                (viewControllers.last as! InformationAsTabularViewController).nodeTitle = nodeName
                (viewControllers.last as! InformationAsTabularViewController).isTypeContentView = typeOfView
                (viewControllers.last as! InformationAsTabularViewController).viewWillAppear(true)
            }
            
        }
    }
    
    class func moveToGalleryListViewControllerIfNotExists(_ navigationController : UINavigationController) {
        let viewControllers = navigationController.viewControllers
        if viewControllers.count > 0 {
            if !(viewControllers.last is GalleryListViewController) {
                
                let informationVC = UIStoryboard.galleryListViewController()
                navigationController.pushViewController(informationVC, animated: false)
            }
        }
    }
    class func moveToNewsAndNotiListViewControllerIfNotExists(_ navigationController : UINavigationController ,  node : NodeType) {
        let viewControllers = navigationController.viewControllers
        
        if viewControllers.count > 0 {
            if !(viewControllers.last is NewsAndNotificationViewController) {
               let newsVC = UIStoryboard.newsAndNotificationViewController()
                  newsVC.isNewsToDisplay = node == .NEWS ? true :false
                  navigationController.pushViewController(newsVC, animated: false)
            }
//            }else{
//                //Just change the nodeId reload the page
//                (viewControllers.last as! News_NotificationViewController).isNewsToDisplay = node == .NEWS ? true :false
//
//                (viewControllers.last as! News_NotificationViewController).viewWillAppear(true)
//            }
        }
    }
    
    class func moveToShopListViewControllerIfNotExists(_ navigationController : UINavigationController) {
        let viewControllers = navigationController.viewControllers
        if viewControllers.count > 0 {
            if !(viewControllers.last is ShopListViewController) {
                
                let shopVC = UIStoryboard.shopListViewController()
                navigationController.pushViewController(shopVC, animated: false)
            }
        }
    }
    
    class func moveToShopAndEatBViewControllerIfNotExists(_ navigationController : UINavigationController ,nodeId :String , shopObject : ShopNameElement?) {
        let viewControllers = navigationController.viewControllers
        if viewControllers.count > 0 {
            if !(viewControllers.last is ShopAndEatBaseViewController) {
                
                let shopVC = UIStoryboard.shopAndEatbaseViewController()
                shopVC.shopID = nodeId
                shopVC.shopDetails = shopObject
                navigationController.pushViewController(shopVC, animated: false)
            }
        }
    }
    
    class func moveToShopDetailViewControllerIfNotExists(_ navigationController : UINavigationController ,nodeId :String ,title : String) {
        let viewControllers = navigationController.viewControllers
        if viewControllers.count > 0 {
            if !(viewControllers.last is ShopDetailViewController) {
                
                let shopVC = UIStoryboard.shopDetailViewController()
                shopVC.nodeID = nodeId
                shopVC.nodeTitle = title
                navigationController.pushViewController(shopVC, animated: false)
            }else {
                
                //Just change the nodeId reload the page
                (viewControllers.last as! ShopDetailViewController).nodeID = nodeId
                (viewControllers.last as! ShopDetailViewController).nodeTitle = title
                (viewControllers.last as! ShopDetailViewController).viewWillAppear(true)
                
            }
        }
    }
    class func moveToMustBuyViewControllerIfNotExists(_ navigationController : UINavigationController ,nodeId :String) {
        let viewControllers = navigationController.viewControllers
        if viewControllers.count > 0 {
            if !(viewControllers.last is MustBuyViewController) {
                
                let mustBuyVC = UIStoryboard.mustBuyViewController()
                mustBuyVC.nodeID = nodeId
                navigationController.pushViewController(mustBuyVC, animated: false)
            }else {
                
                //Just change the nodeId reload the page
                (viewControllers.last as! MustBuyViewController).nodeID = nodeId
                
                (viewControllers.last as! MustBuyViewController).viewWillAppear(true)
                
            }
        }
    }
    class func moveToMustBuyDetailViewControllerIfNotExists(_ navigationController : UINavigationController ,nodeId :String) {
        let viewControllers = navigationController.viewControllers
        if viewControllers.count > 0 {
            if !(viewControllers.last is MustBuyViewController) {
                
                let mustBuyVC = UIStoryboard.mustBuyDetailViewController()
                mustBuyVC.nodeID = nodeId
                navigationController.pushViewController(mustBuyVC, animated: false)
            }else {
                
                //Just change the nodeId reload the page
                (viewControllers.last as! MustBuyDetailViewController).nodeID = nodeId
                
                (viewControllers.last as! MustBuyDetailViewController).viewWillAppear(true)
                
            }
        }
    }
    
    class func moveToFeedbackViewControllerIfNotExists(_ navigationController : UINavigationController) {
        let viewControllers = navigationController.viewControllers
        
        if viewControllers.count > 0 {
            
            if !(viewControllers.last is FeedbackViewController) {
                
                let shopVC = UIStoryboard.feedbackViewController()
                navigationController.pushViewController(shopVC, animated: false)
            }
        }
    }
    
    class func moveToGravienceViewControllerIfNotExists(_ navigationController : UINavigationController) {
        let viewControllers = navigationController.viewControllers
        
        if viewControllers.count > 0 {
            
            if !(viewControllers.last is GravienceViewController) {
                
                let shopVC = UIStoryboard.gravienceViewController()
                navigationController.pushViewController(shopVC, animated: false)
            }
        }
    }
    
    class func moveToWebInformationDisplayViewControllerIfNotExists(_ navigationController : UINavigationController , webURL :String) {
        let viewControllers = navigationController.viewControllers
        if viewControllers.count > 0 {
            if !(viewControllers.last is MapViewController) {
                
                let webiteView = UIStoryboard.webInformationDisplayViewController()
                webiteView.WebURL = webURL
                webiteView.isFromSideController  = false
                navigationController.pushViewController(webiteView, animated: false)
            }else{
                
                //Just change the nodeId reload the page
                (viewControllers.last as! WebInformationDisplayViewController).WebURL = webURL
                (viewControllers.last as! WebInformationDisplayViewController).isFromSideController  = false
                (viewControllers.last as! WebInformationDisplayViewController).viewWillAppear(true)
            }
            
        }
    }
    class func moveTMapViewControllerIfNotExists(_ navigationController : UINavigationController , webURL :String) {
        let viewControllers = navigationController.viewControllers
        if viewControllers.count > 0 {
            if !(viewControllers.last is MapViewController) {
                
                let webiteView = UIStoryboard.MapViewController()
                webiteView.nodeTitle = webURL
                webiteView.isFromSideController  = false
                navigationController.pushViewController(webiteView, animated: false)
            }else{
                
                //Just change the nodeId reload the page
                (viewControllers.last as! MapViewController).nodeTitle = webURL
                (viewControllers.last as! MapViewController).isFromSideController  = false
                (viewControllers.last as! MapViewController).viewWillAppear(true)
            }
            
        }
    }
    
    class func moveToOffersViewControllerIfNotExists(_ navigationController : UINavigationController , webURL :String) {
        let viewControllers = navigationController.viewControllers
        if viewControllers.count > 0 {
            if !(viewControllers.last is OffersViewController) {
                
                let webiteView = UIStoryboard.OffersViewController()
                navigationController.pushViewController(webiteView, animated: false)
            }else{
                
                //Just change the nodeId reload the page
  
                (viewControllers.last as! MapViewController).viewWillAppear(true)
            }
            
        }
    }
    class func moveToAboutUsViewControllerIfNotExists(_ navigationController : UINavigationController , webURL :String) {
        let viewControllers = navigationController.viewControllers
        if viewControllers.count > 0 {
            if !(viewControllers.last is AboutUsViewController) {
                
                let webiteView = UIStoryboard.AboutUsViewController()
                navigationController.pushViewController(webiteView, animated: false)
            }else{
                
                //Just change the nodeId reload the page
                
                (viewControllers.last as! AboutUsViewController).viewWillAppear(true)
            }
            
        }
    }
}
