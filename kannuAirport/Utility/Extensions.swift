//
//  Extensions.swift
//  my digital nrg
//
//  Created by Tushar Agarwal on 18/01/17.
//  Copyright © 2017 Wildnet Technologies Pvt. Ltd. All rights reserved.
//

import UIKit

extension UIStoryboard {
    
    class func mainStoryBoard() -> UIStoryboard{
        
        
        //Three storyboar are there for three languages , So whenever lang changed instantiate accordingly
        if let currentLang = LocalDB.shared.currentLanguage {
            
            if currentLang == Language.hindi {
                
                return UIStoryboard(name: "MainStoryboard_Hi", bundle: Bundle.main)
            }
            if currentLang == Language.malayalam {
                
                return UIStoryboard(name: "MainStoryboard_ml", bundle: Bundle.main)
            }
            
        }
        return UIStoryboard(name: "Main", bundle: Bundle.main)

    }

//
    class func homeScreenViewController() -> HomeViewController {
        return mainStoryBoard().instantiateViewController(withIdentifier: "HomeViewControllerID") as! HomeViewController
    }
    
    
    class func SpalshViewController() -> SpalshViewController {
        return mainStoryBoard().instantiateViewController(withIdentifier: "SpalshViewController") as! SpalshViewController
    }
    
    
    
    class func sideMenuController() -> SideMenuTableViewController {
        return mainStoryBoard().instantiateViewController(withIdentifier: "SideMenuTableViewControllerID") as! SideMenuTableViewController
    }

    class func FlightListController() -> FlightListViewController {
        return mainStoryBoard().instantiateViewController(withIdentifier: "FlightListViewControllerID") as! FlightListViewController
    }
    class func detailController() -> DetailController {
        return mainStoryBoard().instantiateViewController(withIdentifier: "DetailController") as! DetailController
    }
    class func flightDepartureController() -> FlightDepartureController {
        return mainStoryBoard().instantiateViewController(withIdentifier: "FlightDepartureController") as! FlightDepartureController
    }
    class func flightArrivalController() -> FlightArrivalController {
        return mainStoryBoard().instantiateViewController(withIdentifier: "FlightArrivalController") as! FlightArrivalController
    }
    
    class func newsController() -> NewsViewController {
        return mainStoryBoard().instantiateViewController(withIdentifier: "NewsViewControllerID") as! NewsViewController
    }
    
    class func PDFController() -> PDFViewController {
        return mainStoryBoard().instantiateViewController(withIdentifier: "PDFViewControllerID") as! PDFViewController
    }
    
    class func informationController() -> InformationViewController {
        return mainStoryBoard().instantiateViewController(withIdentifier: "InformationViewControllerID") as! InformationViewController
    }

    class func filterController() -> FilterViewController {
        return mainStoryBoard().instantiateViewController(withIdentifier: "FilterViewControllerID") as! FilterViewController
    }
    
    class func galleryController() -> GalleryViewController {
        return mainStoryBoard().instantiateViewController(withIdentifier: "GalleryViewControllerID") as! GalleryViewController
    }

    class func galleryListViewController() -> GalleryListViewController {
        return mainStoryBoard().instantiateViewController(withIdentifier: "GalleryListViewControllerID") as! GalleryListViewController
    }

    class func newsAndNotificationViewController() -> NewsAndNotificationViewController {
        return mainStoryBoard().instantiateViewController(withIdentifier: "News_NotificationViewControllerID") as! NewsAndNotificationViewController
    }

    class func notificationListViewController() -> NotificationListViewController {
        return mainStoryBoard().instantiateViewController(withIdentifier: "NotificationListViewControllerID") as! NotificationListViewController
    }
    class func informationAsTabularViewController() -> InformationAsTabularViewController {
        return mainStoryBoard().instantiateViewController(withIdentifier: "InformationAsTabularViewControllerID") as! InformationAsTabularViewController
    }
    
    class func shopAndEatbaseViewController() -> ShopAndEatBaseViewController {
        return mainStoryBoard().instantiateViewController(withIdentifier: "ShopAndEatBaseViewControllerID") as! ShopAndEatBaseViewController
    }
    
    class func shopListViewController() -> ShopListViewController {
        return mainStoryBoard().instantiateViewController(withIdentifier: "ShopListViewControllerID") as! ShopListViewController
    }
    class func shopDetailViewController() -> ShopDetailViewController {
        return mainStoryBoard().instantiateViewController(withIdentifier: "ShopDetailViewControllerID") as! ShopDetailViewController
    }
    
    class func mustBuyViewController() -> MustBuyViewController {
        return mainStoryBoard().instantiateViewController(withIdentifier: "MustBuyViewControllerID") as! MustBuyViewController
    }
    class func mustBuyDetailViewController() -> MustBuyDetailViewController {
        return mainStoryBoard().instantiateViewController(withIdentifier: "MustBuyDetailViewControllerID") as! MustBuyDetailViewController
    }
    
    
    class func feedbackViewController() -> FeedbackViewController {
        return mainStoryBoard().instantiateViewController(withIdentifier: "FeedbackViewControllerID") as! FeedbackViewController
    }
    class func gravienceViewController() -> GravienceViewController {
        return mainStoryBoard().instantiateViewController(withIdentifier: "GravienceViewControllerID") as! GravienceViewController
    }
    class func webInformationDisplayViewController() -> WebInformationDisplayViewController {
        return mainStoryBoard().instantiateViewController(withIdentifier: "WebInformationDisplayViewControllerID") as! WebInformationDisplayViewController
    }
    class func MapViewController() -> MapViewController {
        return mainStoryBoard().instantiateViewController(withIdentifier: "MapViewControllerID") as! MapViewController
    }
    class func AboutUsViewController() -> AboutUsViewController {
        return mainStoryBoard().instantiateViewController(withIdentifier: "AboutUsViewControllerID") as! AboutUsViewController
    }
    class func OffersViewController() -> OffersViewController {
        return mainStoryBoard().instantiateViewController(withIdentifier: "OffersViewControllerID") as! OffersViewController
    }
}

extension UIView{
    
    @IBInspectable var borderColor:UIColor{
        set{
            self.layer.borderColor = (newValue as UIColor).cgColor
        }
        get{
            let color  = self.layer.borderColor
            return UIColor(cgColor: color!)
        }
    }
    
    @IBInspectable var borderWidth:CGFloat{
        set{
            self.layer.borderWidth = newValue
        }
        get{
            return self.layer.borderWidth
        }
    }
    
    @IBInspectable var cornerRadius:CGFloat{
        set{
            self.layer.cornerRadius = newValue
        }
        get{
            return self.layer.cornerRadius
        }
    }
    
    @IBInspectable var maskToBounds:Bool{
        set{
            self.layer.masksToBounds = newValue
        }
        get{
            return self.layer.masksToBounds
        }
    }
}
extension UIViewController {
    
    //Extension to show alert with default ok button
    func showAlert(with title:String?, message:String?){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        present(alertController, animated: true, completion: nil)
        
    }
    
    func showAlertWithAction(with title:String?, message:String?){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .default,  handler: { (action) in
            self.okBtnTapped()
        }))
        
        present(alertController, animated: true, completion: nil)
        
    }
    @objc  func okBtnTapped(){
        
    }
}
extension UIAlertController {
    
    class func showInfoAlertWithTitle(_ title: String, message: String, buttonTitle: String){
        DispatchQueue.main.async(execute: {
            let alertController = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
            let okayAction = UIAlertAction.init(title: buttonTitle, style: .default, handler: { (okayAction) in
                UIApplication.shared.keyWindow?.rootViewController?.dismiss(animated: true, completion: nil)
            })
            alertController.addAction(okayAction)
            UIApplication.shared.keyWindow?.rootViewController?.present(alertController, animated: true, completion: nil)
        })
    }
}
extension UITextField
{
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            // Placeholder text color
            
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedStringKey.foregroundColor: newValue!])
        }
        
    }
    @IBInspectable
    var cornerRadiusTxtField: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable
    var borderWidthTxtField: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    var borderColorTxtField: UIColor? {
        get {
            let color = UIColor.init(cgColor: layer.borderColor!)
            return color
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
    
    @IBInspectable
    var shadowRadiusTxtField: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowColor = UIColor.black.cgColor
            layer.shadowOffset = CGSize(width: 0, height: 2)
            layer.shadowOpacity = 0.4
            layer.shadowRadius = shadowRadiusTxtField
        }
    }
        enum Direction
        {
            case Left
            case Right
        }
    
        func AddImage(direction:Direction,imageName:String,Frame:CGRect,backgroundColor:UIColor)
        {
            let View = UIView(frame: Frame)
            View.backgroundColor = backgroundColor
    
            let imageView = UIImageView(frame: Frame)
            imageView.image = UIImage(named: imageName)
            imageView.contentMode = .center
            View.addSubview(imageView)
    
            if Direction.Left == direction
            {
                self.leftViewMode = .always
                self.leftView = View
            }
            else
            {
                self.rightViewMode = .always
                self.rightView = View
            }
        }
    
    
}
extension UITextView: UITextViewDelegate {
    override open var bounds: CGRect {
        didSet {
            self.resizePlaceholder()
        }
    }
    
    public var placeholder: String? {
        get {
            var placeholderText: String?
            
            if let placeholderLbl = self.viewWithTag(50) as? UILabel {
                placeholderText = placeholderLbl.text
            }
            
            return placeholderText
        }
        set {
            if let placeholderLbl = self.viewWithTag(50) as! UILabel? {
                placeholderLbl.text = newValue
                placeholderLbl.sizeToFit()
            } else {
                self.addPlaceholder(newValue!)
            }
        }
    }
    
    public func textViewDidChange(_ textView: UITextView) {
        if let placeholderLbl = self.viewWithTag(50) as? UILabel {
            placeholderLbl.isHidden = self.text.count > 0
        }
    }
    
    private func resizePlaceholder() {
        if let placeholderLbl = self.viewWithTag(50) as! UILabel? {
            let x = self.textContainer.lineFragmentPadding
            let y = self.textContainerInset.top - 2
            let width = self.frame.width - (x * 2)
            let height = placeholderLbl.frame.height
            
            placeholderLbl.frame = CGRect(x: x, y: y, width: width, height: height)
        }
    }
    
    private func addPlaceholder(_ placeholderText: String) {
        let placeholderLbl = UILabel()
        
        placeholderLbl.text = placeholderText
        placeholderLbl.sizeToFit()
        
        placeholderLbl.font = UIFont(name: "Karla-Regular", size: 16.0)!
        placeholderLbl.textColor = UIColor(red:0.42, green:0.51, blue:0.57, alpha:1)
        placeholderLbl.tag = 50
        
        placeholderLbl.isHidden = self.text.count > 0
        
        self.addSubview(placeholderLbl)
        self.resizePlaceholder()
        self.delegate = self
    }
    
    
    func textViewBorderColor(_ view:UIView, color:UIColor) -> Void {
        view.layer.cornerRadius = 4.0
        view.layer.masksToBounds = true
        view.layer.borderColor = color.cgColor
        view.layer.borderWidth = 1.0
    }
    
}
extension UITableView {
    
    func setEmptyMessage(_ message: String) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = .black
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        messageLabel.font = UIFont(name: "Karla-Bold", size: 24.0)!
        messageLabel.sizeToFit()
        
        self.backgroundView = messageLabel;
        self.separatorStyle = .none;
    }
    
    func restore() {
        self.backgroundView = nil
    }
}
extension NSLayoutConstraint {
    func constraintWithMultiplier(_ multiplier: CGFloat) -> NSLayoutConstraint {
        return NSLayoutConstraint(item: self.firstItem!, attribute: self.firstAttribute, relatedBy: self.relation, toItem: self.secondItem, attribute: self.secondAttribute, multiplier: multiplier, constant: self.constant)
    }
}
extension String {
    func localized(lang:String) ->String {
        
        let path = Bundle.main.path(forResource: lang, ofType: "lproj")
        let bundle = Bundle(path: path!)
        
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
    }
    public var withoutHtml: String {
        guard let data = self.data(using: .utf8) else {
            return self
        }
        
        let options: [NSAttributedString.DocumentReadingOptionKey: Any] = [
            .documentType: NSAttributedString.DocumentType.html,
            .characterEncoding: String.Encoding.utf8.rawValue
        ]
        
        guard let attributedString = try? NSAttributedString(data: data, options: options, documentAttributes: nil) else {
            return self
        }
        
        return attributedString.string
    }
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
}
extension UIImageView{
    
}
