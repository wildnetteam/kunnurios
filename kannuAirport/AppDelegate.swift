//
//  AppDelegate.swift
//  kannuAirport
//
//  Created by Arpana on 01/05/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit
import KYDrawerController
import Fabric
import Crashlytics


//WARNING:- rememeber to change this for changing base url
var kBOOL_LIVE  =  true // To distingush live and staging URL


@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate {

  //  var locationManager: CLLocationManager?

    var window: UIWindow?
    var menuNavController :UINavigationController?
    var filterText = ""
    static var BASE_URL = ""

    let drawerController  = KYDrawerController(drawerDirection: .left, drawerWidth: 300)

    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        
        //let splashViewController = UIStoryboard.SpalshViewController()



        initialize()
        return true
    }
    
    func goingToHomeScreen(){
            let mainViewController   = UIStoryboard.homeScreenViewController()
        
               let sideMenuTableViewControllerObj = UIStoryboard.sideMenuController()
        
                menuNavController = UINavigationController(rootViewController: mainViewController)
                menuNavController?.isNavigationBarHidden = true
                drawerController.mainViewController = menuNavController
                drawerController.drawerViewController = sideMenuTableViewControllerObj
        
                //Set the language for the first time
        
                if LocalDB.shared.currentLanguage == nil{
        
                    LocalDB.shared.currentLanguage = Language.english
                }
        
                window?.rootViewController = drawerController
                window?.makeKeyAndVisible()
    }

    func initialize()  {
        
        AppDelegate.BASE_URL  = ServiceApiNames.initializeBaseURL()
        Fabric.with([Crashlytics.self])
       // locationManager = CLLocationManager()
        //locationManager?.requestWhenInUseAuthorization()


    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

