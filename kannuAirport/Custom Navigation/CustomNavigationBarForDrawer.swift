//
//  CustomNavigationBarForDrawer.swift
//  my digital nrg
//
//  Created by Tushar Agarwal on 18/01/17.
//  Copyright © 2017 Wildnet Technologies Pvt. Ltd. All rights reserved.
//

import UIKit

public enum ButtonTag :Int {
    case BACK
    case MENU
}

class CustomNavigationBarForDrawer: UIView {
    
    @IBOutlet weak var leftSideMenuButtonItem: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    var view : UIView?
    var isFromSideMenu: Bool?
    var senderController : UIViewController?
    
    func xibSetup() {
        view = loadViewFromNib()
        view!.frame = bounds
        view!.autoresizingMask = [.flexibleWidth, .flexibleHeight]
       // view?.backgroundColor = kNavigationBarColor
        addSubview(view!)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "CustomNavigationBarForDrawer", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        if(leftSideMenuButtonItem.tag == ButtonTag.BACK.rawValue){
            leftSideMenuButtonItem.setImage(#imageLiteral(resourceName: "back"), for: .normal)
        }else {
        leftSideMenuButtonItem.setImage(#imageLiteral(resourceName: "3bar"), for: .normal)
        }
        return view
    }
    
  
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func hideLeftBarButtonItem() {
        leftSideMenuButtonItem.isHidden = true
    }
    
    //MARK:- UIButton Action Methods
    
    @IBAction func leftSideMenuBtnTapped(_ sender: Any) {
        
        
        switch (sender as! UIButton).tag  {
            
        case ButtonTag.BACK.rawValue:
            senderController?.navigationController?.popViewController(animated: true)

        case ButtonTag.MENU.rawValue:
            kAppDelegate.drawerController.setDrawerState(.opened, animated: true)

           // senderController?.menuButtontaped()
            
            senderController?.view.endEditing(true)
           // id myCurrentController = tmpDelegate.myNavigationController.topViewController;


        default:
            print("othe button tapped")
            
        }

    }
}
