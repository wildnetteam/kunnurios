//
//  Terminal.swift
//  kannuAirport
//
//  Created by Arpana on 27/06/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

// To parse the JSON, add this file to your project and do:
//
//   let Terminal = try Terminal(json)

// To parse the JSON, add this file to your project and do:
//
//   let location = try Location(json)

import Foundation

struct Terminal: Codable {
    let data: [TerminalArray]?
    
    enum CodingKeys: String, CodingKey {
        case data = "data"
    }
}

struct TerminalArray: Codable {
    let location: String?
    
    enum CodingKeys: String, CodingKey {
        case location = "Location"
    }
}

// MARK: Convenience initializers

extension Terminal {
    
    init(data: Data) throws {
        self = try JSONDecoder().decode(Terminal.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func jsonData() throws -> Data {
        return try JSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension TerminalArray {
    init(data: Data) throws {
        self = try JSONDecoder().decode(TerminalArray.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func jsonData() throws -> Data {
        return try JSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}
