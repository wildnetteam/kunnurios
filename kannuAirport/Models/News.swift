//
//  News.swift
//  kannuAirport
//
//  Created by Arpana on 29/05/18.
//  Copyright © 2018 Arpana. All rights reserved.
//


//[{"title":"Corrigendum No. 10 to NIT for Construction of Office Complex","created":"May 10,  2018"},{"title":"CSR Policy of the Company","created":"May 2,  2018"},{"title":"Half Yearly Compliance report of Dec 16 to May 17","created":"May 2,  2018"}]

import UIKit

typealias News = [CurrentNews]

//struct CurrentNews: Codable {
//    let title, created: String
//}

struct CurrentNews: Codable {
    let nid: String?
    let title: String?
    let created: String?
    let fieldUploadPDF: String?
    let viewNode: String?
    let uri: String?
    
    enum CodingKeys: String, CodingKey {
        case nid = "nid"
        case title = "title"
        case created = "created"
        case fieldUploadPDF = "field_upload_pdf"
        case viewNode = "view_node"
        case uri = "uri"
    }
}

// MARK: Convenience initializers

extension CurrentNews {
    
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(CurrentNews.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    func dateFromString(dt : String) -> String {
        
        //MMM d, yyyy
        
        let dateFormatterPrint = DateFormatter()
//      /  dateFormatterPrint.dateFormat = "MMM d, yyyy"
        
        dateFormatterPrint.setLocalizedDateFormatFromTemplate("MMM d, yyyy")
        
        if let date = dateFormatterPrint.date(from: dt){
            return (dateFormatterPrint.string(from: date))
        }
        else {
            print("There was an error decoding the string")
        }
        
        return ""
    }
    
    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

extension Array where Element == News.Element {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(News.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonDataNews: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonDataNews else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

