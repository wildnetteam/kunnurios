//
//  PhotoGallery.swift
//  kannuAirport
//
//  Created by Arpana on 31/05/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit

// To parse the JSON, add this file to your project and do:
//
//   let photoGallery = try PhotoGallery(json)

import Foundation

typealias PhotoGallery = [PhotoGalleryElement]

struct PhotoGalleryElement: Codable {
    let nid: String?
    let title: String?
    let delta: String?
    let fieldImage: String?
    
    enum CodingKeys: String, CodingKey {
        case nid = "nid"
        case title = "title"
        case delta = "delta"
        case fieldImage = "field_image"
    }
}

// MARK: Convenience initializers

extension PhotoGalleryElement {
    init(data: Data) throws {
        self = try JSONDecoder().decode(PhotoGalleryElement.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func jsonData() throws -> Data {
        return try JSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension Array where Element == PhotoGallery.Element {
    init(data: Data) throws {
        self = try JSONDecoder().decode(PhotoGallery.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func jsonData() throws -> Data {
        return try JSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}
