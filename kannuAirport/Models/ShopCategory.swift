//
//  ShopCategory.swift
//  kannuAirport
//
//  Created by Arpana on 29/06/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit

// To parse the JSON, add this file to your project and do:
//
//   let shopCategory = try ShopCategory(json)

import Foundation

typealias ShopCategory = [ShopCategoryElement]

struct ShopCategoryElement: Codable {
    let tid: String?
    let name: String?
    let fieldShopCategoryImage: String?
    
    enum CodingKeys: String, CodingKey {
        case tid = "tid"
        case name = "name"
        case fieldShopCategoryImage = "field_shop_category_image"
    }
}

// MARK: Convenience initializers

extension ShopCategoryElement {
    init(data: Data) throws {
        self = try JSONDecoder().decode(ShopCategoryElement.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func jsonData() throws -> Data {
        return try JSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension Array where Element == ShopCategory.Element {
    init(data: Data) throws {
        self = try JSONDecoder().decode(ShopCategory.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func jsonData() throws -> Data {
        return try JSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}
