//
//  PhotoGalleryDetails.swift
//  kannuAirport
//
//  Created by Arpana on 12/06/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

// To parse the JSON, add this file to your project and do:
//
//   let photoGalleryDetails = try PhotoGalleryDetails(json)

import Foundation

struct PhotoGalleryDetails: Codable {
    let allRecode: Int?
    var data = [Datum]()
    
    enum CodingKeys: String, CodingKey {
        case allRecode = "all_recode"
        case data = "data"
    }
}

struct Datum: Codable {
    let targetID: String?
    let alt: String?
    let medium: String?
    let large: String?
    
    enum CodingKeys: String, CodingKey {
        case targetID = "target_id"
        case alt = "alt"
        case medium = "medium"
        case large = "large"
    }
}

// MARK: Convenience initializers

extension PhotoGalleryDetails {
    init(data: Data) throws {
        self = try JSONDecoder().decode(PhotoGalleryDetails.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func jsonData() throws -> Data {
        return try JSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension Datum {
    init(data: Data) throws {
        self = try JSONDecoder().decode(Datum.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func jsonData() throws -> Data {
        return try JSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}
