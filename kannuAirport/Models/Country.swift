//
//  Country.swift
//  kannuAirport
//
//  Created by Arpana on 27/06/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit

// To parse the JSON, add this file to your project and do:
//
//   let country = try Country(json)

import Foundation

struct Country: Codable {
    let data: [CountryArray]?
    
    enum CodingKeys: String, CodingKey {
        case data = "data"
    }
}

struct CountryArray: Codable {
    let country: String?
    
    enum CodingKeys: String, CodingKey {
        case country = "Country"
    }
}

// MARK: Convenience initializers

extension Country {
    init(data: Data) throws {
        self = try JSONDecoder().decode(Country.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func jsonData() throws -> Data {
        return try JSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension CountryArray {
    init(data: Data) throws {
        self = try JSONDecoder().decode(CountryArray.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func jsonData() throws -> Data {
        return try JSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

