//
//  MustBuy.swift
//  kannuAirport
//
//  Created by Arpana on 05/07/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit
// To parse the JSON, add this file to your project and do:
//
//   let mustBuy = try MustBuy(json)

import Foundation

typealias MustBuy = [MustBuyElement]

struct MustBuyElement: Codable {
    let productID: String?
    let title: String?
    let fieldImage: String?
    let body: String?
    let variations: String?
    
    enum CodingKeys: String, CodingKey {
        case productID = "product_id"
        case title = "title"
        case fieldImage = "field_image"
        case body = "body"
        case variations = "variations"
    }
}

// MARK: Convenience initializers

extension MustBuyElement {
    init(data: Data) throws {
        self = try JSONDecoder().decode(MustBuyElement.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func jsonData() throws -> Data {
        return try JSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension Array where Element == MustBuy.Element {
    init(data: Data) throws {
        self = try JSONDecoder().decode(MustBuy.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func jsonData() throws -> Data {
        return try JSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

