//
//  LogoModel.swift
//  kannuAirport
//
//  Created by Arpana on 04/05/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

// To parse the JSON, add this file to your project and do:
//
//   let logo = try Logo(json)

import Foundation



typealias Logo = [LOGOElement]

struct LOGOElement: Codable {
    let fieldImage: String?
    
    enum CodingKeys: String, CodingKey {
        case fieldImage = "field_image"
    }
}

// MARK: Convenience initializers

extension LOGOElement {
    init(data: Data) throws {
        self = try JSONDecoder().decode(LOGOElement.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func jsonData() throws -> Data {
        return try JSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension Array where Element == Logo.Element {
    init(data: Data) throws {
        self = try JSONDecoder().decode(Logo.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func jsonData() throws -> Data {
        return try JSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}
