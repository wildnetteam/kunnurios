//
//  HomeSlidingImages.swift
//  kannuAirport
//
//  Created by Arpana on 09/05/18.
//  Copyright © 2018 Arpana. All rights reserved.
//


    // To parse the JSON, add this file to your project and do:
    //
    //   let homeSlidingImages = try HomeSlidingImages(json)


import UIKit

    struct HomeSlidingImages: Codable {
        let status: String?
        let sliderimage: [String]?
        
        enum CodingKeys: String, CodingKey {
            case status = "status"
            case sliderimage = "sliderimage"
        }
    }
    
    // MARK: Convenience initializers
    
    extension HomeSlidingImages {
        
        init(data: Data) throws {
            self = try JSONDecoder().decode(HomeSlidingImages.self, from: data)
        }
        
        init(_ json: String, using encoding: String.Encoding = .utf8) throws {
            guard let data = json.data(using: encoding) else {
                throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
            }
            try self.init(data: data)
        }
        
        init(fromURL url: URL) throws {
            try self.init(data: try Data(contentsOf: url))
        }
        
        func jsonData() throws -> Data {
            return try JSONEncoder().encode(self)
        }
        
        func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
            return String(data: try self.jsonData(), encoding: encoding)
        }
    }


