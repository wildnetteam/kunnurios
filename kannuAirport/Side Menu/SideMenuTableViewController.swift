//
//  SideMenuTableViewController.swift
//  SideMenu
//
//  Created by Jon Kent on 4/5/16.
//  Copyright © 2016 CocoaPods. All rights reserved.
//

import Foundation
import SideMenu

class SideMenuTableViewController:  UIViewController , UINavigationControllerDelegate , UITableViewDelegate, UITableViewDataSource
{
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet weak var lebelTitle: UILabel!
    var treeTableView:YSTreeTableView?
    var menuItems:NSArray?
  

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // refresh cell blur effect in case it changed
        tableView.reloadData()
        
        guard SideMenuManager.default.menuBlurEffectStyle == nil else {
            return
        }
        
        // Set up a cool background image for demo purposes
        let imageView = UIImageView(image: UIImage(named: "saturn"))
        imageView.contentMode = .scaleAspectFit
        imageView.backgroundColor = UIColor.black.withAlphaComponent(0.2)
        tableView.backgroundView = imageView
    
        
    }
    
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellIdentifier", for: indexPath)  as! UITableViewVibrantCell
        
        cell.blurEffectStyle = SideMenuManager.default.menuBlurEffectStyle
        
        return cell
    }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
  
        return 5
    }
    
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 60
    }
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
        case 0:
            self.navigationController?.pushViewController( UIStoryboard.homeScreenViewController(), animated: true)
            
        case 4: //News Screen
            self.navigationController?.pushViewController( UIStoryboard.newsController(), animated: true)
            
        default:
            self.navigationController?.pushViewController( UIStoryboard.FlightListController(), animated: true)
        }
        
    }
    
}

extension SideMenuTableViewController:YSTreeTableViewDelegate{
  
    
    func setMenuItems(menuItemsArray:MainMenu){
        
        var rootNodes:[YSTreeTableViewNode]?
        
        
        var globalDic = MainMenuElement.init()
        for data in menuItemsArray {
            let dict = data as MainMenuElement
            var canSpand = false
            if let below = dict.below{
                if (((below as NSArray).count) > 0 ){
                    canSpand = true
                }
            }else{
              globalDic = dict
            }
            
            
            let parentNode = YSTreeTableViewNode(nodeID: 1, nodeName: dict.title!, leftImageName: "", rightImageName: "", isExpand: false , canSpand:canSpand, info:globalDic)
            
            var subNodeArray:[YSTreeTableViewNode]?
            
            
            if let below = dict.below{
                let belowArray = below
                for belowData in belowArray {
                    
                    let belowDict = belowData
                    
                    var canSpand = false
                    if let below = belowData.below{
                        if (below.count > 0 ){
                            canSpand = true
                        }
                    }else{
                        
                    }
                    
                    let subNode = YSTreeTableViewNode(nodeID: 2, nodeName: belowDict.title! , leftImageName: "", rightImageName: "", isExpand: false,canSpand:canSpand , info: belowData)
                    
                    var subNodeChildArray:[YSTreeTableViewNode]?
                    
                    if let subNodeChild = belowDict.below{
                        let subNodeArray = subNodeChild
                        for subNodeData in subNodeArray {
                            
                            var canSpand = false
                            if let below = (subNodeData.below){
                                if (below.count  > 0 ){
                                    canSpand = true
                                }
                            }else{
                                
                            }
                            
                            let subNodeDict = subNodeData //as! NSDictionary
                            let childNode = YSTreeTableViewNode(nodeID: 3, nodeName: subNodeDict.title!, leftImageName: "", rightImageName: "", isExpand: false,canSpand:canSpand ,info: subNodeData)
                            
                            var subNodeInnerChildArray:[YSTreeTableViewNode]?
                            if let belowChild = subNodeDict.below{
                                let childArray = belowChild
                                for child in childArray {
                                    let node = YSTreeTableViewNode(nodeID: 4, nodeName: child.title!, leftImageName: "", rightImageName: "", isExpand: false,canSpand:false , info: child )
                                    if subNodeInnerChildArray == nil {
                                        subNodeInnerChildArray = [YSTreeTableViewNode]()
                                    }
                                    subNodeInnerChildArray?.append(node!)
                                    
                                }
                                childNode?.subNodes = subNodeInnerChildArray!
                            }
                            
                            if subNodeChildArray == nil {
                                subNodeChildArray = [YSTreeTableViewNode]()
                            }
                            subNodeChildArray?.append(childNode!)
                        }
                        subNode?.subNodes = subNodeChildArray!
                    }
                    if subNodeArray == nil {
                        subNodeArray = [YSTreeTableViewNode]()
                    }
                    subNodeArray?.append(subNode!)
                    
                }
                parentNode?.subNodes = subNodeArray!
            }
            
            if rootNodes == nil {
                rootNodes = [YSTreeTableViewNode]()
                
                let settingNode = YSTreeTableViewNode(nodeID: 191, nodeName: "Home".localized(lang: getLanguageKey(lang:  LocalDB.shared.currentLanguage!)), leftImageName: "", rightImageName: "", isExpand: false,canSpand:false , info: MainMenuElement.init())
                
                rootNodes?.append(settingNode!)
                
//                let notificationNode = YSTreeTableViewNode(nodeID: 102, nodeName: "News and Tenders/Notifications"
//                    .localized(lang: getLanguageKey(lang:  LocalDB.shared.currentLanguage!)), leftImageName: "", rightImageName: "", isExpand: false,canSpand:false , info: MainMenuElement.init())
//
//                rootNodes?.append(notificationNode!)
                
            }
           

            
            rootNodes?.append(parentNode!)
        }
       
        
        
        let feedbackNode = YSTreeTableViewNode(nodeID: 103, nodeName: "Feedback".localized(lang: getLanguageKey(lang:  LocalDB.shared.currentLanguage!)), leftImageName: "", rightImageName: "", isExpand: false,canSpand:false , info: MainMenuElement.init())
        
        rootNodes?.append(feedbackNode!)
        
        let grievanceNode = YSTreeTableViewNode(nodeID: 104, nodeName: "Grievance".localized(lang: getLanguageKey(lang:  LocalDB.shared.currentLanguage!)), leftImageName: "", rightImageName: "", isExpand: false,canSpand:false , info: MainMenuElement.init())
        
        rootNodes?.append(grievanceNode!)
        
        treeTableView = YSTreeTableView(frame: view.bounds, style: .plain)
        treeTableView?.rootNodes = rootNodes!
        treeTableView?.treeDelegate = self
        view.addSubview(treeTableView!)
    }
    
    
    func treeCellClick(node: YSTreeTableViewNode, indexPath: IndexPath) {
        print("indexPath is \(indexPath)")
        print("currentNodeModel is \(node)")
        print("currentNodeModelName is \(node.nodeName)")
        var cell:AnyObject?
        if (node.depth == 0 || node.depth == 1 || node.depth == 2) && node.canSpand {
            cell = treeTableView?.cellForRow(at: indexPath) as? YSTreeTableViewContentCell
        }else{
            cell = treeTableView?.cellForRow(at: indexPath) as? YSTreeTableViewNodeCell
        }
        
        if node.canSpand {
            if node.isExpand {
                print("Expanded ")
                UIView.animate(withDuration: 0.5, animations: {
                    cell?.accessoryView??.transform = CGAffineTransform(rotationAngle: 0 * CGFloat(Double.pi/180))
                })
              
            } else {
                print("colapsed")
                UIView.animate(withDuration: 0.5, animations: {
                    cell?.accessoryView??.transform = CGAffineTransform(rotationAngle: 90 * CGFloat(Double.pi/180))
                })
            }
        }
        
        if node.isExpand{ // 当前为展开状态，要进行收缩操作
            print("Expanded")
            if node.canSpand {
                
                
            }
        } else{
            print("colapsed")
            if node.canSpand {
                //It has child
            }else {
                //Its a leaf node
            }
        }
    }
    
    func treeCellNavigateToAnotherViewController(node:YSTreeTableViewNode, id:NodeType){
        
        
        // For All static pages call
        switch (id) {
            
            //        case .CARGO_FACILITIES:  print("HII")
            //
            //        case .FLIGHT_ARRIVAL_INFO:  print("HII")
            //
            //        case .FLIGHT_DEPARTURE_INFO:  print("HII")
            //
            //        case .ABOUT:  print("HII")
            //
        case .TENDER :  NavigationManager.moveToNewsAndNotiListViewControllerIfNotExists(kAppDelegate.menuNavController! , node:  id)
            
        case .FEEDBACK:  NavigationManager.moveToFeedbackViewControllerIfNotExists(kAppDelegate.menuNavController!)
            
        case .GRAVEINCE: NavigationManager.moveToGravienceViewControllerIfNotExists(kAppDelegate.menuNavController!)
            
        case .HOME:   NavigationManager.moveToHomeViewControllerIfNotExists(kAppDelegate.menuNavController!)
         
        case .GALLARY:  NavigationManager.moveToGalleryListViewControllerIfNotExists(kAppDelegate.menuNavController!)
        
        case .Other :
            
            var  nodeUriID : Array? = (node.info.uri?.components(separatedBy: "/"))!
            
            
            
                if let typeOfView = node.info.description {
                    
                    
                if typeOfView == "view" {
                    
                    //show tabular Info

                    NavigationManager.moveToInformationAsTabularViewControllerIfNotExists(kAppDelegate.menuNavController! , nodeURI: node.info.uri ,nodeName : node.nodeName, isSideMenuIconToAppear: true, typeOfView: false)
                    
                }else  if typeOfView == "content" {
                    
                    //show tabular Info

                    if let number : String = nodeUriID![1] {

                    NavigationManager.moveToInformationViewControllerIfNotExists(kAppDelegate.menuNavController! ,node: id , nodeIden: number ,nodeName : node.nodeName, isSideMenuIconToAppear: true)
                    }
                }else  if typeOfView == "contentview" {
                    
                    // In case of tabular details click , logic of pdf will not work, Need to open node/id concept
                    NavigationManager.moveToInformationAsTabularViewControllerIfNotExists(kAppDelegate.menuNavController! , nodeURI: node.info.uri ,nodeName : node.nodeName, isSideMenuIconToAppear: true, typeOfView: true)
                    
                    }
            }
            
        default:
            NavigationManager.moveToInformationViewControllerIfNotExists(kAppDelegate.menuNavController! ,node: id , nodeIden: String(id.rawValue), nodeName: node.nodeName , isSideMenuIconToAppear: true)
            print("Need to implement")
        }
        
        kAppDelegate.drawerController.setDrawerState(.closed, animated: true)

    }


}
