//
//  YSTreeTableView.swift
//  YSTreeTableView
//
//  Created by yaoshuai on 2017/1/10.
//  Copyright © 2017年 ys. All rights reserved.
//

import UIKit

private let YSTreeTableViewNodeCellID:String = "YSTreeTableViewNodeCellID"
private let YSTreeTableViewContentCellID:String = "YSTreeTableViewContentCellID"

private let YSTreeTableViewContentCellHeight :CGFloat = 60
private let YSTreeTableViewNodeCellHeight:  CGFloat = 60
private let heightForHeader  :CGFloat  = 128.0


protocol YSTreeTableViewDelegate:NSObjectProtocol {
    
    func treeCellClick(node:YSTreeTableViewNode,indexPath:IndexPath)
    
    func treeCellNavigateToAnotherViewController(node:YSTreeTableViewNode, id:NodeType)
}

class YSTreeTableView: UITableView {
    
    var treeDelegate:YSTreeTableViewDelegate?
    
    /// 外部传入的数据源(根节点集合)
    var rootNodes:[YSTreeTableViewNode] = [YSTreeTableViewNode](){
        didSet{
            getExpandNodeArray()
            reloadData()
        }
    }
    
    /// 内部使用的数据源(由nodeArray变形而来)
    fileprivate var tempNodeArray:[YSTreeTableViewNode] = [YSTreeTableViewNode]()
    
    /// 点击“展开”，添加的子节点的“索引数组”
    fileprivate var insertIndexPaths:[IndexPath] = [IndexPath]()
    private var insertRow = 0
    
    /// 点击“收缩”，删除的子节点的“索引数组”
    fileprivate var deleteIndexPaths:[IndexPath] = [IndexPath]()
    
    override init(frame:CGRect, style: UITableViewStyle){
        super.init(frame: frame, style: style)
        setupTableView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupTableView()
    }
    
    private func setupTableView(){
        dataSource = self
        delegate = self
        
        self.separatorStyle = .none
        
        register(UINib(nibName: "YSTreeTableViewContentCell", bundle: nil), forCellReuseIdentifier: YSTreeTableViewContentCellID)
        
        register(UINib(nibName: "YSTreeTableViewNodeCell", bundle: nil), forCellReuseIdentifier: YSTreeTableViewNodeCellID)
        
    }
    
    
    /// 添加本节点及展开的子节点(包括展开的孙子节点)至数组中
    ///
    /// - Parameters:
    ///   - node: 本节点
    private func addExpandNodeToArray(node:YSTreeTableViewNode) -> Void{
        tempNodeArray.append(node)
        
        if node.isExpand{ // 当前节点展开，添加其子节点
            for childNode in node.subNodes{
                addExpandNodeToArray(node: childNode)
            }
        }
    }
    
    /// 获取所有展开的节点数组
    private func getExpandNodeArray() -> (){
        for rootNode in rootNodes{
            if rootNode.parentNode == nil{ // 再次判断是否是根节点
                addExpandNodeToArray(node: rootNode)
            }
        }
    }
    
    /// 是否是叶子节点
    ///
    /// - Parameter node: 节点
    /// - Returns: true-是；false-否
    fileprivate func isLeafNode(node:YSTreeTableViewNode) -> Bool{
        return node.subNodes.count == 0
    }
    
    
    /// 点击“展开”，添加子节点(如果子节点也处于展开状态，添加孙子节点)
    ///
    /// - Parameter node: 节点
    fileprivate func insertChildNode(node:YSTreeTableViewNode){
        node.isExpand = true
        if node.subNodes.count == 0{
            return
        }
        
        insertRow = tempNodeArray.index(of: node)! + 1
        
        for childNode in node.subNodes{
            let childRow = insertRow
            let childIndexPath = IndexPath(row: childRow, section: 0)
            insertIndexPaths.append(childIndexPath)
            
            tempNodeArray.insert(childNode, at: childRow)
            
            insertRow += 1
            if childNode.isExpand{
                insertChildNode(node: childNode)
            }
        }
    }
    
    /// 点击“收缩”，删除所有处于展开状态的子节点的索引
    ///
    /// - Parameter node: 节点
    fileprivate func getDeleteIndexPaths(node:YSTreeTableViewNode){
        if node.isExpand{ // 再次确认节点处于展开状态
            
            for childNode in node.subNodes{
                let childRow = tempNodeArray.index(of: childNode)!
                let childIndexPath = IndexPath(row: childRow, section: 0)
                deleteIndexPaths.append(childIndexPath)
                
                if childNode.isExpand{
                    getDeleteIndexPaths(node: childNode)
                }
            }
        }
    }
    
    /// 点击“收缩”，删除所有处于展开状态的子节点
    ///
    /// - Parameter node: 节点
    fileprivate func deleteChildNode(node:YSTreeTableViewNode){
        getDeleteIndexPaths(node: node)
        
        node.isExpand = false
        
        for _ in deleteIndexPaths{
            tempNodeArray.remove(at: deleteIndexPaths.first!.row)
        }
    }
}

extension YSTreeTableView:UITableViewDataSource,UITableViewDelegate{
    // MARK: - cell
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        let headerView = UIView(frame: CGRect(x: tableView.frame.origin.x + 20 , y: tableView.frame.origin.y, width: tableView.contentSize.width, height: heightForHeader))
        let imgView = UIImageView.init(frame: headerView.frame)
        imgView.image = #imageLiteral(resourceName: "logo")
        imgView.contentMode = .left
        headerView.backgroundColor = UIColor.white
        headerView.addSubview(imgView)
        return headerView

    }
    

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let headerView = UIView(frame: CGRect(x: tableView.frame.origin.x + 20 , y: tableView.frame.origin.y, width: tableView.contentSize.width, height: 60))
        headerView.backgroundColor = UIColor.white
        return headerView
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tempNodeArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let node = tempNodeArray[indexPath.row]
        
        if isLeafNode(node: node){ // 叶子节点，内容cell
            let cell:YSTreeTableViewNodeCell = tableView.dequeueReusableCell(withIdentifier: YSTreeTableViewNodeCellID, for: indexPath) as! YSTreeTableViewNodeCell
            cell.selectionStyle = .none
            cell.node = node
            
            return cell
        } else{ // 节点cell
            
            let cell = tableView.dequeueReusableCell(withIdentifier: YSTreeTableViewContentCellID) as! YSTreeTableViewContentCell
            //let cell:YSTreeTableViewContentCell = tableView.dequeueReusableCell(withIdentifier: YSTreeTableViewContentCellID, for: indexPath) as! YSTreeTableViewContentCell
            cell.selectionStyle = .none
            cell.node = node
            
            return cell
        }
    }
    
    // MARK: - 高度

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return heightForHeader
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let node = tempNodeArray[indexPath.row]
        
        if isLeafNode(node: node){ // 叶子节点，内容cell
            return YSTreeTableViewContentCellHeight
        } else{ // 节点cell
            return YSTreeTableViewNodeCellHeight
        }
    }
    
    // MARK: - 点击
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let node = tempNodeArray[indexPath.row]
        //*************Debug*******************//
       /* if let nodeURL = node.info.uri {
            
             print(nodeURL)
        }
        if let nodeTitle = node.info.title {
            
            print(nodeTitle)
        }*/
        //**************************************//
        
        treeDelegate?.treeCellClick(node: node, indexPath: indexPath)
        
        if isLeafNode(node: node){ // 叶子节点，内容cell
            
            if node.info.uri != "" {
                
                var  nodeUriID : Array? = (node.info.uri?.components(separatedBy: "/"))!
                
                guard let nodeDetailsArray = nodeUriID else{
                    //no uri information available
                    return
                }
                if  (nodeUriID?.count)! <= 1 {
                    //handle some other uri Ex: "tender"
                    treeDelegate?.treeCellNavigateToAnotherViewController(node: node, id: .Other)
                    return
                }
                
                guard let nodeUriIdAsInt = Int(nodeDetailsArray[1]) else{
                    
                    //node detail is not found , So can node open any specific page
                    // Fo example : "http://kannurairport.aero/archive" will give nil

                    treeDelegate?.treeCellNavigateToAnotherViewController(node: node, id: .Other)
                    return
                }
                if let number = NodeType(rawValue: nodeUriIdAsInt){
                    
                   // Fo example : "node/55"
                    let IDFetch :NodeType? = number
                    
                    if let nodeNumber = IDFetch {
                        
                        treeDelegate?.treeCellNavigateToAnotherViewController(node: node, id: nodeNumber)
                    }
                }else{
                    
                        //node detail is not found , So can node open any specific page
                        treeDelegate?.treeCellNavigateToAnotherViewController(node: node, id: .Other)
                }
                
                
            }else{
                
                if let nodeRetrieved = NodeType(rawValue: Int(node.nodeID)){
                    
                    treeDelegate?.treeCellNavigateToAnotherViewController(node: node, id:nodeRetrieved)
                }else{
                    
//                    if let nodeTitle = node.nodeName as? String {
//
//                        if nodeTitle == "Gallery"{
//
//                            treeDelegate?.treeCellNavigateToAnotherViewController(node: node, id: .GALLARY)
//
//                        }
//                    }
                    UIAlertController.showInfoAlertWithTitle("", message: "Could not retrieve the details", buttonTitle: "OK")
                }
            }
        } else{ // 节点cell
            if node.isExpand{ // 当前节点展开，要进行收缩操作
                deleteIndexPaths = [IndexPath]()
                deleteChildNode(node: node)
                deleteRows(at: deleteIndexPaths, with: .top)
            }
            else{ // 当前节点收缩，要进行展开操作
                insertIndexPaths = [IndexPath]()
                insertChildNode(node: node)
                insertRows(at: insertIndexPaths, with: .top)
            }
        }
      
    }
}
