//
//  YSTreeTableViewCell.swift
//  YSTreeTableView
//
//  Created by yaoshuai on 2017/1/10.
//  Copyright © 2017年 ys. All rights reserved.
//

import UIKit

/// 节点Cell
class YSTreeTableViewNodeCell: UITableViewCell {
    
    @IBOutlet weak var imgLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblText: UILabel!
    
    var node:YSTreeTableViewNode?{
        didSet{
            indentationLevel = node?.depth ?? 0 // 缩进层次
            indentationWidth = 10 // 每次缩进宽度
            
            self.backgroundColor = UIColor.white

            if (node?.canSpand)! {
               // let chevron = UIImage(named: "next")
                self.accessoryView = UIImageView(image: #imageLiteral(resourceName: "arrow right"))
            }
            
            if indentationLevel == 0 {
                
                switch(node?.nodeName){
                    
                case "Home".localized(lang: getLanguageKey(lang:  LocalDB.shared.currentLanguage!)):  imageView?.isHidden = false
                      self.imgView.image = #imageLiteral(resourceName: "home")
               
                case "News and Tenders/Notifications".localized(lang: getLanguageKey(lang:  LocalDB.shared.currentLanguage!)):  imageView?.isHidden = false
                self.imgView.image = #imageLiteral(resourceName: "tender-1")
                
                case "Cargo User".localized(lang: getLanguageKey(lang:  LocalDB.shared.currentLanguage!)):  imageView?.isHidden = false
                self.imgView.image = #imageLiteral(resourceName: "cargo")
                    
                case "Passengers".localized(lang: getLanguageKey(lang:  LocalDB.shared.currentLanguage!)):  imageView?.isHidden = false
                self.imgView.image = #imageLiteral(resourceName: "passengers")
                    
                case "Staff Login".localized(lang: getLanguageKey(lang:  LocalDB.shared.currentLanguage!)):  imageView?.isHidden = false
                self.imgView.image = #imageLiteral(resourceName: "staff")
                    
                case "Corporate User".localized(lang: getLanguageKey(lang:  LocalDB.shared.currentLanguage!)):  imageView?.isHidden = false
                self.imgView.image = #imageLiteral(resourceName: "corporate")
                
                case "General".localized(lang: getLanguageKey(lang:  LocalDB.shared.currentLanguage!)):  imageView?.isHidden = false
                self.imgView.image = #imageLiteral(resourceName: "general")
                    
                default: imageView?.isHidden = true
                          self.imgView.image = #imageLiteral(resourceName: "tick")
                }
                
            }
            else
            {
                imgView.image = nil
            }
            lblText?.text = node?.nodeName
            lblText?.font = UIFont(name: "Karla-Regular", size: 16.0)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
       // if(self.indentationLevel != 0){
            
            self.imgLeadingConstraint.constant = 10 + CGFloat(self.indentationLevel) * self.indentationWidth
            self.contentView.layoutIfNeeded()
      //  }
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
