//
//  YSTreeTableViewContentCell.swift
//  YSTreeTableView
//
//  Created by yaoshuai on 2017/1/10.
//  Copyright © 2017年 ys. All rights reserved.
//

import UIKit

/// 内容Cell-最底层Cell
class YSTreeTableViewContentCell: UITableViewCell {

    @IBOutlet weak var lblText: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var imgLeadingConstraint: NSLayoutConstraint!

    var node:YSTreeTableViewNode?{
        didSet{
            indentationLevel = node?.depth ?? 0 // 缩进层次
            indentationWidth = 10 // 每次缩进宽度
            
//            if indentationLevel == 0 {
//                self.backgroundColor = UIColor(red: 30/255.0, green: 147/255.0, blue: 219/255.0, alpha: 1.0)
//            }else if indentationLevel == 1 {
//                self.backgroundColor = UIColor(red: 30/255.0, green: 149/255.0, blue: 229/255.0, alpha: 1.0)
//            }else if indentationLevel == 2 {
//                self.backgroundColor = UIColor(red: 30/255.0, green: 151/255.0, blue: 231/255.0, alpha: 1.0)
//            }
            if (node?.canSpand)! {
              //  let chevron = UIImage(named: "next")
                self.accessoryView = UIImageView(image: #imageLiteral(resourceName: "arrow right"))
                self.accessoryView?.contentMode = .center
                
                if node!.isExpand == true {
                    UIView.animate(withDuration: 0.5, animations: {
                        self.accessoryView?.transform = CGAffineTransform(rotationAngle: 90 * CGFloat(Double.pi/180))
                    })
                } else {
                    UIView.animate(withDuration: 0.5, animations: {
                        self.accessoryView?.transform = CGAffineTransform(rotationAngle: 0 * CGFloat(Double.pi/180))
                    })

                }

                
            }
            if indentationLevel == 0 {
                
                //Images need to be implemented only on top level , not on sub levels
                switch(node?.nodeName){
                    
                case "Home".localized(lang: getLanguageKey(lang:  LocalDB.shared.currentLanguage!)):  imageView?.isHidden = false
                self.imgView.image = #imageLiteral(resourceName: "home")
                    
                case "News and Tenders/Notifications".localized(lang: getLanguageKey(lang:  LocalDB.shared.currentLanguage!)):  imageView?.isHidden = false
                self.imgView.image = #imageLiteral(resourceName: "tender-1")
                    
                case "Cargo User".localized(lang: getLanguageKey(lang:  LocalDB.shared.currentLanguage!)):  imageView?.isHidden = false
                self.imgView.image = #imageLiteral(resourceName: "cargo")
                    
                case "Passengers".localized(lang: getLanguageKey(lang:  LocalDB.shared.currentLanguage!)):  imageView?.isHidden = false
                self.imgView.image = #imageLiteral(resourceName: "passengers")
                    
                case "Staff Login".localized(lang: getLanguageKey(lang:  LocalDB.shared.currentLanguage!)):  imageView?.isHidden = false
                self.imgView.image = #imageLiteral(resourceName: "staff")
                
                case "Visitors".localized(lang: getLanguageKey(lang:  LocalDB.shared.currentLanguage!)):  imageView?.isHidden = false
                self.imgView.image = #imageLiteral(resourceName: "corporate")
                    
                case "Corporate User".localized(lang: getLanguageKey(lang:  LocalDB.shared.currentLanguage!)):  imageView?.isHidden = false
                self.imgView.image = #imageLiteral(resourceName: "corporate")
                  
                case "General".localized(lang: getLanguageKey(lang:  LocalDB.shared.currentLanguage!)):  imageView?.isHidden = false
                self.imgView.image = #imageLiteral(resourceName: "general")
                    
                default: imageView?.isHidden = true
                self.imgView.image = #imageLiteral(resourceName: "tick")
                    
                }
               
            }
            else
            {
                imgView.image = nil
            }
            lblText?.text = node?.nodeName
            lblText?.font = UIFont(name: "Karla-Regular", size: 16.0)
        }
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
       // if(self.indentationLevel != 0){
        self.imgLeadingConstraint.constant = 10 + CGFloat(self.indentationLevel) * self.indentationWidth
        self.contentView.layoutIfNeeded()
       // }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
