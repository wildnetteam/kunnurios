//
//  AppDelegate.swift
//  kannuAirport
//
//  Created by Arpana on 03/05/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit


class APICall: NSObject {
    
    class func callLogoAPI(_ parameters:[String: Any], header : [String: Any] , onCompletion: @escaping ServiceResponse) -> Void
    {
        
        let urlToRequsetLanguage =  ServiceApiNames.initializeBaseURLWithLanguage(providedURL: AppDelegate.BASE_URL, languageType: .english)
        
        let urlToRequset = ServiceApiNames.getCompleteMethodURL(providedURL: urlToRequsetLanguage, method: MethodName.LOGO , nodeId: NodeType.NOTAPPLIED)
        
        
        
        APIWrapper.callGet(urlToRequset, param: parameters , header: [ : ]) { (isSucess, responseDictionary, error) in
            
            
            onCompletion(isSucess, responseDictionary, error)
        }
    }
    
    class func callMainMenuItemsAPI(_ parameters:[String: Any], header : [String: Any] , onCompletion: @escaping ServiceResponse) -> Void
    {
        
        let urlToRequsetLanguage =  ServiceApiNames.initializeBaseURLWithLanguage(providedURL: AppDelegate.BASE_URL, languageType: LocalDB.shared.currentLanguage!)
        
        let urlToRequset = ServiceApiNames.getCompleteMethodURL(providedURL: urlToRequsetLanguage, method: MethodName.SIDEMENUITEMS , nodeId: NodeType.NOTAPPLIED)
        
        print("urlToRequset\(urlToRequset)")
        
        APIWrapper.callGet(urlToRequset, param: parameters , header: header as! [String : String]) { (isSucess, responseDictionary, error) in
            
            
            onCompletion(isSucess, responseDictionary, error)
        }
    }
    class func callHeaderBannerImagesAPI(_ parameters:[String: Any], header : [String: Any] , onCompletion: @escaping ServiceResponse) -> Void
    {
        
        let urlToRequsetLanguage =  ServiceApiNames.initializeBaseURLWithLanguage(providedURL: AppDelegate.BASE_URL, languageType: .english)
        
        let urlToRequset = ServiceApiNames.getCompleteMethodURL(providedURL: urlToRequsetLanguage, method: MethodName.HEADERBANNER,  nodeId: NodeType.NOTAPPLIED)
        
        APIWrapper.callGet(urlToRequset, param: parameters , header: header as! [String : String]) { (isSucess, responseDictionary, error) in
            
            
            onCompletion(isSucess, responseDictionary, error)
        }
    }
    
    class func callHeaderSlidingImagesAPI(_ parameters:[String: Any], header : [String: Any] , onCompletion: @escaping ServiceResponse) -> Void
    {
        
        
        let urlToRequsetLanguage =  ServiceApiNames.initializeBaseURLWithLanguage(providedURL: AppDelegate.BASE_URL, languageType: LocalDB.shared.currentLanguage!)
        
        let urlToRequset = ServiceApiNames.getCompleteMethodURL(providedURL: urlToRequsetLanguage, method: MethodName.TOPSLIDER , nodeId: NodeType.NOTAPPLIED)
        
        
          let urlNew:String = urlToRequset.replacingOccurrences(of: " ", with: "")
        
        APIWrapper.callGetWithoutAuthantication(urlNew, param: parameters , header: [:]) { (isSucess, responseDictionary, error) in
            
            onCompletion(isSucess, responseDictionary, error)
        }
    }
    
    class func callInformationAPI(_ parameters:[String: Any], header : [String: Any] , nodeIDToGetInfo : NodeType, idToFetchDetails : String , onCompletion: @escaping ServiceResponse) -> Void
    {
        
        let urlToRequsetLanguage =  ServiceApiNames.initializeBaseURLWithLanguage(providedURL: AppDelegate.BASE_URL, languageType: LocalDB.shared.currentLanguage!)
        
        
        let urlToRequset = urlToRequsetLanguage + ServiceApiNames.getNodeURLForDetailView(nodeID: idToFetchDetails)
        
        APIWrapper.callGet(urlToRequset, param: parameters , header: header as! [String : String]) { (isSucess, responseDictionary, error) in
            
            onCompletion(isSucess, responseDictionary, error)
        }
    }
    class func callNewsAPI(_ parameters:[String: Any], header : [String: Any] , onCompletion: @escaping ServiceResponse) -> Void
    {
        
        let urlToRequsetLanguage =  ServiceApiNames.initializeBaseURLWithLanguage(providedURL: AppDelegate.BASE_URL, languageType: LocalDB.shared.currentLanguage!)
        
        let urlToRequset = ServiceApiNames.getCompleteMethodURL(providedURL: urlToRequsetLanguage, method: MethodName.CURRENTNEWS ,  nodeId: NodeType.NOTAPPLIED)
        
        APIWrapper.callGet(urlToRequset, param: parameters , header: header as! [String : String]) { (isSucess, responseDictionary, error) in
            
            onCompletion(isSucess, responseDictionary, error)
        }
    }
    class func callNotificationAPI(_ parameters:[String: Any], header : [String: Any] ,  onCompletion: @escaping ServiceResponse) -> Void
    {
        
        let urlToRequsetLanguage =  ServiceApiNames.initializeBaseURLWithLanguage(providedURL: AppDelegate.BASE_URL, languageType: LocalDB.shared.currentLanguage!)
        
        let urlToRequset = ServiceApiNames.getCompleteMethodURL(providedURL: urlToRequsetLanguage, method: MethodName.TENDERNOTIFICATION ,  nodeId: NodeType.NOTAPPLIED)
        
        APIWrapper.callGet(urlToRequset, param: parameters , header: header as! [String : String]) { (isSucess, responseDictionary, error) in
            
            onCompletion(isSucess, responseDictionary, error)
        }
    }
    class func callGalleryAPI(_ parameters:[String: Any], header : [String: Any] ,  onCompletion: @escaping ServiceResponse) -> Void
    {
        
        let urlToRequsetLanguage =  ServiceApiNames.initializeBaseURLWithLanguage(providedURL: AppDelegate.BASE_URL, languageType: .english)
        
        let urlToRequset = ServiceApiNames.getCompleteMethodURL(providedURL: urlToRequsetLanguage, method: MethodName.PHOTOGALLERY ,  nodeId: NodeType.NOTAPPLIED)
        
        APIWrapper.callGet(urlToRequset, param: parameters , header: header as! [String : String]) { (isSucess, responseDictionary, error) in
            
            onCompletion(isSucess, responseDictionary, error)
        }
    }
    
    class func callGalleryDetailsAPI(_ parameters: [String: Any], header : [String: Any] ,  onCompletion: @escaping ServiceResponse) -> Void
    {
        
        let urlToRequsetLanguage =  ServiceApiNames.initializeBaseURLWithLanguage(providedURL: AppDelegate.BASE_URL, languageType: .english)
        
        let urlToRequset = ServiceApiNames.getCompleteMethodURL(providedURL: urlToRequsetLanguage, method: MethodName.PHOTOGALLERYDETAILS ,  nodeId: NodeType.NOTAPPLIED)
    
        
        APIWrapper.callPost(urlToRequset, param: parameters , header: header as! [String : String]) { (isSucess, responseDictionary, error) in
            
            onCompletion(isSucess, responseDictionary, error)
        }
    }
    
    class func callCountryAPI(_ parameters:[String: Any], header : [String: Any] ,  onCompletion: @escaping ServiceResponse) -> Void
    {
        
        let urlToRequsetLanguage =  ServiceApiNames.initializeBaseURLWithLanguage(providedURL: AppDelegate.BASE_URL, languageType: .english)
        
        let urlToRequset = ServiceApiNames.getCompleteMethodURL(providedURL: urlToRequsetLanguage, method: MethodName.COUNTRY ,  nodeId: NodeType.NOTAPPLIED)
        
        APIWrapper.callGet(urlToRequset, param: parameters , header: header as! [String : String]) { (isSucess, responseDictionary, error) in
            
            onCompletion(isSucess, responseDictionary, error)
        }
    }
    
    class func callTerminalTypeAPI(_ parameters:[String: Any], header : [String: Any] ,  onCompletion: @escaping ServiceResponse) -> Void
    {
        
        let urlToRequsetLanguage =  ServiceApiNames.initializeBaseURLWithLanguage(providedURL: AppDelegate.BASE_URL, languageType: .english)
        
        let urlToRequset = ServiceApiNames.getCompleteMethodURL(providedURL: urlToRequsetLanguage, method: MethodName.LOCATION ,  nodeId: NodeType.NOTAPPLIED)
        
        APIWrapper.callGet(urlToRequset, param: parameters , header: header as! [String : String]) { (isSucess, responseDictionary, error) in
            
            onCompletion(isSucess, responseDictionary, error)
        }
    }
    
    class func callQueryTypeAPI(_ parameters:[String: Any], header : [String: Any] ,  onCompletion: @escaping ServiceResponse) -> Void
    {
        
        let urlToRequsetLanguage =  ServiceApiNames.initializeBaseURLWithLanguage(providedURL: AppDelegate.BASE_URL, languageType: .english)
        
        let urlToRequset = ServiceApiNames.getCompleteMethodURL(providedURL: urlToRequsetLanguage, method: MethodName.QUERYTYPE ,  nodeId: NodeType.NOTAPPLIED)
        
        APIWrapper.callGet(urlToRequset, param: parameters , header: header as! [String : String]) { (isSucess, responseDictionary, error) in
            
            onCompletion(isSucess, responseDictionary, error)
        }
    }
    
    class func callCategoriesAPI(_ parameters:[String: Any], header : [String: Any] ,  onCompletion: @escaping ServiceResponse) -> Void
    {
        
        let urlToRequsetLanguage =  ServiceApiNames.initializeBaseURLWithLanguage(providedURL: AppDelegate.BASE_URL, languageType: .english)
        
        let urlToRequset = ServiceApiNames.getCompleteMethodURL(providedURL: urlToRequsetLanguage, method: MethodName.SHOPCATEGORIES ,  nodeId: NodeType.NOTAPPLIED)
        
        APIWrapper.callGet(urlToRequset, param: parameters , header: header as! [String : String]) { (isSucess, responseDictionary, error) in
            
            onCompletion(isSucess, responseDictionary, error)
        }
    }
    class func callShopAPI(_ parameters: [String: Any], header : [String: Any] ,idToFetchDetails : String,  onCompletion: @escaping ServiceResponse) -> Void
    {
        
        let urlToRequsetLanguage =  ServiceApiNames.initializeBaseURLWithLanguage(providedURL: AppDelegate.BASE_URL, languageType: .english)
        
        let urlToRequset = ServiceApiNames.getCompleteMethodURL(providedURL: urlToRequsetLanguage, method: MethodName.SHOPNAME ,  nodeId: NodeType.NOTAPPLIED)
        
        let urlWithTid = "\(urlToRequset)" + "\(ServiceApiNames.getNodeURLForDetailView(nodeID: idToFetchDetails))"
        
        APIWrapper.callGet(urlWithTid, param: parameters , header: header as! [String : String]) { (isSucess, responseDictionary, error) in
            
            onCompletion(isSucess, responseDictionary, error)
        }
    }
    class func callMustBuyAPI(_ parameters: [String: Any], header : [String: Any] ,idToFetchDetails : String,  onCompletion: @escaping ServiceResponse) -> Void
    {
        
        let urlToRequsetLanguage =  ServiceApiNames.initializeBaseURLWithLanguage(providedURL: AppDelegate.BASE_URL, languageType: .english)
        
        let urlToRequset = ServiceApiNames.getCompleteMethodURL(providedURL: urlToRequsetLanguage, method: MethodName.MUSTBUY ,  nodeId: NodeType.NOTAPPLIED)
        
        let urlWithTid = "\(urlToRequset)" + "\(ServiceApiNames.getNodeURLForDetailView(nodeID: idToFetchDetails))"
        
        APIWrapper.callGet(urlWithTid, param: parameters , header: header as! [String : String]) { (isSucess, responseDictionary, error) in
            
            onCompletion(isSucess, responseDictionary, error)
        }
    }
    class func callMustBuyDetailsAPI(_ parameters: [String: Any], header : [String: Any] ,idToFetchDetails : String,  onCompletion: @escaping ServiceResponse) -> Void
    {
        
        let urlToRequsetLanguage =  ServiceApiNames.initializeBaseURLWithLanguage(providedURL: AppDelegate.BASE_URL, languageType: .english)
        
        let urlToRequset = ServiceApiNames.getCompleteMethodURL(providedURL: urlToRequsetLanguage, method: MethodName.MUSTBUYDETAILS ,  nodeId: NodeType.NOTAPPLIED)
        
        let urlWithTid = "\(urlToRequset)" + "\(ServiceApiNames.getNodeURLForDetailView(nodeID: idToFetchDetails))"
        
        APIWrapper.callGet(urlWithTid, param: parameters , header: header as! [String : String]) { (isSucess, responseDictionary, error) in
            
            onCompletion(isSucess, responseDictionary, error)
        }
    }
    
    class func callTabularInformationAPI(_ parameters:[String: Any], header : [String: Any] ,URLPath : String,  onCompletion: @escaping ServiceResponse) -> Void
    {
        
        let urlToRequsetLanguage =  ServiceApiNames.initializeBaseURLWithLanguage(providedURL: AppDelegate.BASE_URL, languageType: LocalDB.shared.currentLanguage!)
        
        let urlToRequset = "\(urlToRequsetLanguage)\(URLPath)?_format=json"
        
        APIWrapper.callGet(urlToRequset, param: parameters , header: header as! [String : String]) { (isSucess, responseDictionary, error) in
            
            onCompletion(isSucess, responseDictionary, error)
        }
    }
    
    class func callSubmitFeedbbackAPI(_ parameters: [String: Any], header : [String: Any] ,  onCompletion: @escaping ServiceResponse) -> Void
    {
        
        let urlToRequsetLanguage =  ServiceApiNames.initializeBaseURLWithLanguage(providedURL: AppDelegate.BASE_URL, languageType: .english)
        
        let urlToRequset = ServiceApiNames.getCompleteMethodURL(providedURL: urlToRequsetLanguage, method: MethodName.FEEDBACKFORM ,  nodeId: NodeType.NOTAPPLIED)
        
        
        APIWrapper.callPost(urlToRequset, param: parameters , header: header as! [String : String]) { (isSucess, responseDictionary, error) in
            
            onCompletion(isSucess, responseDictionary, error)
        }
    }
    class func callFlightArrivalDepartAPI(_ parameters:[String: Any], header : [String: Any] , onCompletion: @escaping ServiceResponse) -> Void
    {
        
        let urlToRequsetLanguage =  ServiceApiNames.initializeBaseURLWithLanguage(providedURL: AppDelegate.BASE_URL, languageType: LocalDB.shared.currentLanguage!)
        
        let urlToRequset = ServiceApiNames.getCompleteMethodURL(providedURL: urlToRequsetLanguage, method: MethodName.FLIGHTARRIVAL ,  nodeId: NodeType.NOTAPPLIED)
        
        APIWrapper.callGet(urlToRequset, param: parameters , header: header as! [String : String]) { (isSucess, responseDictionary, error) in
            
            onCompletion(isSucess, responseDictionary, error)

        }
    }
    class func callFlightDepartAPI(_ parameters:[String: Any], header : [String: Any] , onCompletion: @escaping ServiceResponse) -> Void
    {
        
        let urlToRequsetLanguage =  ServiceApiNames.initializeBaseURLWithLanguage(providedURL: AppDelegate.BASE_URL, languageType: LocalDB.shared.currentLanguage!)
        
        let urlToRequset = ServiceApiNames.getCompleteMethodURL(providedURL: urlToRequsetLanguage, method: MethodName.FLIGHTDEPARTURE ,  nodeId: NodeType.NOTAPPLIED)
        
        APIWrapper.callGet(urlToRequset, param: parameters , header: header as! [String : String]) { (isSucess, responseDictionary, error) in
            
            onCompletion(isSucess, responseDictionary, error)
            
        }
    }
}
