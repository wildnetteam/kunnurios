//
//  AppDelegate.swift
//  kannuAirport
//
//  Created by Arpana on 03/05/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit
import SystemConfiguration
import Alamofire

typealias ServiceResponse = (Bool,  Data?, Error?) -> Void
typealias ServiceResponseForArrival = (_ isSuccess: Bool,_ response : [ArrivalDepartModel] , Error?) -> Void

struct APIWrapper {
    
    // MARK: - General API -

    static func isInternetAvailable() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)

        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }

        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    
    
    
    static func callGetWithoutAuthantication(_ router: String, param: [String: Any], header: [String: String], callback:@escaping ServiceResponse)  -> (){
        
        
        let plainString = "\(user):\(password)"
        let plainData = plainString.data(using: .utf8)
        let base64String = plainData?.base64EncodedString(options:NSData.Base64EncodingOptions(rawValue: 0))
        
        let authString = "Basic " + base64String!
        
        // Set in in Authorize header like...
        let headers1: HTTPHeaders = [
            "Authorization": authString,
            ]
        
        if (NetworkReachabilityManager.init()?.isReachable)!{
            
            
            Alamofire.request(router, method: .get, parameters: nil,  encoding: JSONEncoding.default, headers: headers1).validate().validate(contentType: ["application/json","text/html"])
                .responseJSON { response in
                    
                    callback (  response.result.isSuccess , response.data , response.error  )
            }
        }  else{
            print("No interbnet")
            callback ( false,  nil, NSError(domain: AlertMessages.networkError, code: 500, userInfo: [:]))
            
        }
    }
    
    
    static func callGet(_ router: String, param: [String: Any], header: [String: String], callback:@escaping ServiceResponse)  -> (){

        let plainString = "\(user):\(password)"
        let plainData = plainString.data(using: .utf8)
        let base64String = plainData?.base64EncodedString(options:NSData.Base64EncodingOptions(rawValue: 0))
        
        let authString = "Basic " + base64String!
        
        // Set in in Authorize header like...
        let headers1: HTTPHeaders = [
            "Authorization": authString,
           ]
        
        if (NetworkReachabilityManager.init()?.isReachable)!{
            

            Alamofire.request(router, method: .get, parameters: nil,  encoding: JSONEncoding.default, headers: headers1).validate().validate(contentType: ["application/json","text/html"])
                .responseJSON { response in
            
                callback (  response.result.isSuccess , response.data , response.error  )
            }
        }  else{
            print("No interbnet")
            callback ( false,  nil, NSError(domain: AlertMessages.networkError, code: 500, userInfo: [:]))
            
        }
    }
    
  /*  static func callGetAsDict(_ router: String, param: [String: Any], header: [String: String], callback:@escaping (Bool,  AnyObject?, Error?) -> Void)  -> (){
        
        let plainString = "\(user):\(password)"
        let plainData = plainString.data(using: .utf8)
        let base64String = plainData?.base64EncodedString(options:NSData.Base64EncodingOptions(rawValue: 0))
        
        let authString = "Basic " + base64String!
        
        // Set in in Authorize header like...
        let headers1: HTTPHeaders = [
            "Authorization": authString,
            "Accept": "application/json"
        ]
        if (NetworkReachabilityManager.init()?.isReachable)!{
            
            Alamofire.request(router, method: .get, parameters: nil,  encoding: JSONEncoding.default, headers: headers1).validate().responseJSON { response in
                
                
                if((response.result.value) != nil) {
                    let swiftyJsonVar = JSON(response.result.value!)
                    
                    if let resData = swiftyJsonVar["data"].arrayObject {
                        self.arrRes = resData as! [[String:AnyObject]]
                    }
                    
                }
                
                callback (  response.result.isSuccess , response.result as? AnyObject  , response.error  )
            
        }  else{
            print("No interbnet")
            callback ( false,  nil, NSError(domain: AlertMessages.networkError, code: 500, userInfo: [:]))
            
        }
    }*/
    
    static func callPost(_ router: String, param: [String: Any], header: [String: String], callback:@escaping ServiceResponse)  -> (){
        
        let plainString = "\(user):\(password)"
        let plainData = plainString.data(using: .utf8)
        let base64String = plainData?.base64EncodedString(options:NSData.Base64EncodingOptions(rawValue: 0))
        
        let authString = "Basic " + base64String!
        
        // Set in in Authorize header like...
        let headers1: HTTPHeaders = [
            "Authorization": authString,
          //  "Accept": "application/json"
        ]
        
        var  jsonData = NSData()
        
        // var dataString2 :String = ""
        do {
            jsonData = try JSONSerialization.data(withJSONObject: param, options: .prettyPrinted) as NSData
            // you can now cast it with the right type
        } catch {
            print(error.localizedDescription)
        }

        if (NetworkReachabilityManager.init()?.isReachable)!{

        Alamofire.request(router, method: .post, parameters: param, encoding: URLEncoding.default, headers: headers1  as HTTPHeaders).validate().validate(contentType: ["application/json","text/html"]).responseJSON
            {response in
                callback (  response.result.isSuccess , response.data , response.error  )

        }
    }else{
            print("No interbnet")
            callback ( false,  nil, NSError(domain: AlertMessages.networkError, code: 500, userInfo: [:]))
            
        }
    }
    
    static func uploadWithAlamofire(_ router: String, parameters: [String: String], imageData: Data, callback:@escaping (AnyObject?)->()) -> () {
        
        // Begin upload
        Alamofire.upload( multipartFormData: { multipartFormData in
            
            multipartFormData.append(imageData, withName: "file", fileName: "myImage.png", mimeType: "image/png")
            
            // import parameters
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }}
            , to: router, method: .post,
              
              encodingCompletion: { encodingResult in
                
                switch encodingResult {
                    
                case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        callback(response.result.value as AnyObject?)
                    }
                case .failure(let encodingError):
                    
                    callback(nil)
                    print("**ERROR")
                    print(encodingError)
                }
        })
    }
}
