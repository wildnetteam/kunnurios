//
//  ServiceApiName.swift
//  kannuAirport
//
//  Created by Arpana on 04/05/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import Foundation

let imageBaseUrl =  kBOOL_LIVE ? "https://www.kannurairport.aero/" : "http://111.93.53.59/"

enum MethodName {
    
    case SIDEMENUITEMS
    case SIDEMENUSUBITEMS
    case LOGO
    case HEADERBANNER
    case TOPSLIDER
    case INFORMATION
    case TENDERNOTIFICATION
    case CURRENTNEWS
    case PHOTOGALLERY
    case PHOTOGALLERYDETAILS
    case COUNTRY
    case LOCATION
    case QUERYTYPE
    case SHOPCATEGORIES
    case SHOPNAME
    case MUSTBUY
    case MUSTBUYDETAILS
    case FEEDBACKFORM
    case FLIGHTARRIVAL
    case FLIGHTDEPARTURE

}


public enum NodeType :Int
{
    case NOTAPPLIED = -1
    case OVERVIEW = 16
    case CARGO_FACILITIES =  17
    case PARTENERS
    case FLIGHT_ARRIVAL_INFO  = 55
    case FLIGHT_DEPARTURE_INFO = 38
    case ABOUT
    case FLIGHT_PLAANER =  57
    case NEWS =  101 //19
    case TENDER =  102
    case FEEDBACK =  103
    case GRAVEINCE =  104
    case HOME =  191
    case GALLARY = 1036569
    case Other

    
}

public func getURLForNode (nodeType : NodeType) -> String {
    
    switch nodeType {
        
    case .NEWS : return "node/19?_format=json"
    case .CARGO_FACILITIES:  return "node/17?_format=json"
    case .FLIGHT_ARRIVAL_INFO:  return "node/17?_format=json"
    case .FLIGHT_PLAANER: return "node/60?_format=json"
    case .GALLARY: return ""
    default: return ""
        
    }
}


class ServiceApiNames {
    
    let baseURL : String = ""
    
    
    //MARK:- Get the URL of Live/Stagging
    static func initializeBaseURL() -> String {
        
        if kBOOL_LIVE {
            
            //Live URL
           return  "https://kannurairport.aero/"
            
        }
        //Stagging URL
        return "http://111.93.53.59/kialnew/"
        
    }
    
    //MARK:- Get the selected language to append in URL
    static func initializeBaseURLWithLanguage(providedURL : String , languageType : Language) -> String {
        
        switch languageType {
        case .english:
            //English
            return initializeBaseURL()
        case .hindi:
            //Hindi
            return providedURL + "hi/"
        case .malayalam:
            //Malyalam
            return providedURL + "ml/"
        }
    }
    
    //MARK:- Get the method name to append in URL
    static func getCompleteMethodURL(providedURL : String , method: MethodName , nodeId : NodeType) -> String{
        
        switch method {
            
        case .SIDEMENUITEMS:
            return kBOOL_LIVE ? providedURL + "api/menu_items/mobile-navigation-menu?_format=json" : providedURL + "api/menu_items/mobile-navigation?_format=json"
            // case .NEWS: //"api/menu_items/main?_format=json"
            
        //    return providedURL + "currentnews"
            
        case .SIDEMENUSUBITEMS:
            return providedURL + "submenu_items"
            
        case .LOGO:
            return providedURL + "ariline/logo?_format=json"
          
        case  .HEADERBANNER:
             return providedURL + "block/10?_format=json"
            
        case .TOPSLIDER:
                return providedURL + "custom-api/v1/homeslider.json"
        
        case .TENDERNOTIFICATION:
            return providedURL + "tendersnotifications?_format=json"
            
        case .CURRENTNEWS:
            return providedURL + "currentnews?_format=json"
        
        case .PHOTOGALLERY:
            return providedURL + "photo_gallery_cat_api?_format=json"
        
        case .PHOTOGALLERYDETAILS:
            return providedURL + "custom-api/v1/getgallerydetails.json"
        
        case .LOCATION:
            return providedURL + "custom-api/v1/getlocationfeedbackform.json"
        
        case .COUNTRY:
               return providedURL + "custom-api/v1/getallcountry.json"

        case .QUERYTYPE:
            return providedURL + "custom-api/v1/gettypeofqueryfeedbackform.json"

        case .SHOPNAME:
            return providedURL + "category-store-api/"

        case .SHOPCATEGORIES:
            return providedURL + "shop-categories-api?_format=json"
            
        case .MUSTBUY: //http://172.16.16.6/kialnew/store-product-apidetails/2?_format=json
            return providedURL + "store-product-apidetails/"
            
        case .MUSTBUYDETAILS: //http://172.16.16.6/kialnew/product-api-details-custom/2?_format=json
            return providedURL + "product-api-details-custom/"
            
        case .FEEDBACKFORM: //http://111.93.53.59/kialnew/custom-api/v1/submitfeedbackform.json
            return providedURL + "custom-api/v1/submitfeedbackform.json"
            
        case .FLIGHTARRIVAL:
            return providedURL +    "custom-api/v1/getflightinfo.json?type=arrival"// /departure"
            
        case .FLIGHTDEPARTURE :
            return providedURL +    "custom-api/v1/getflightinfo.json?type=departure"

        case .INFORMATION:
            return providedURL +  getURLForNode(nodeType: nodeId)
            
       
        }
    }
    
    static func getNodeURLForDetailView (nodeID : String) -> String {
        
        return "node/\(nodeID)?_format=json"
    }
    
}
